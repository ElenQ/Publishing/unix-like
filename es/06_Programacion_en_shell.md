# Programación en la shell

En en libro *The UNIX Programming Environment* de 1984, los autores Brian
Kernighan y Rob Pike, que trabajaron en el desarrollo de muchas versiones de
Unix, incluyen el próximo fragmento:

> Although most users think of the shell as an interactive command interpreter,
> it is really a programming language in which each statement runs a command.
> Because it must satisfy both the interactive and programming aspects of
> command execution, it is a strange language, shaped as much by history as by
> design.
>
> — Brian Kernighan and Rob Pike, The UNIX Programming Environment,
> Prentice-Hall, 1984

Los autores en este párrafo vienen a decir que, aunque la mayoría de las
personas piensa en la *shell* como un intérprete de comandos, la *shell* es en
realidad un lenguaje de programación en el que cada sentencia ejecuta un
comando. Y añade que es un lenguaje peculiar debido tanto a la historia como
por el propio diseño, ya que debe satisfacer ambos aspectos de la ejecución de
comandos: el interactivo y el programático.

Hasta este punto del documento únicamente se ha trabajado sobre la
interactividad, y se ha presentado la *shell* como un programa que captura los
comandos introducidos a través del teclado. La realidad, como este párrafo
indica, es bien distinta.

El aspecto que falta por trabajar, la programación en la *shell*, es posible
que no tenga el mismo impacto en la vida diaria que el modo interactivo pero
conocerlo es muy interesante ya sea para crear pequeñas herramientas de trabajo
como para, simplemente, enriquecer la relación con el entorno interactivo.

## Conceptos

La *shell* clásica de Unix es un entorno diseñado para trabajar con cadenas de
caracteres (*strings*) y todo gira alrededor de ellas. Esta será en muchas
ocasiones la explicación de muchas de las decisiones de diseño tomadas en la
*shell*, por lo que es extremadamente importante recordarlo.

La orientación a cadenas de caracteres puede manifestarse de diferentes
maneras, pero en el caso de las *shell*s clásicas de Unix se basa en sus modos
de interpolación, que algunos ya han sido estudiados previamente, y en la
captura y el filtrado de texto.

La interpolación se refiere a la capacidad de sustituir ciertas indicaciones,
como identificadores de variables o símbolos, en cadenas de caracteres por su
valor o por el resultado de una ejecución. Esto permite la creación de cadenas
de caracteres tipo plantilla cuyos contenidos se resuelven a casos concretos a
medida que se van evaluando.

La captura y el filtrado de texto se refiere a la capacidad de la *shell* de
ejecutar comandos y recibir su resultado en variables, para un tratamiento
posterior, o redireccionar su salida a tuberías u otros mecanismos de procesado
de texto.

La combinación de ambos puntos, junto a los bloques básicos de la programación
imperativa (bucles y condicionales) completan un lenguaje de programación
poderoso, aunque extraño, con toda la capacidad para ejecutar comandos de
sistema.

Curiosamente, con el tiempo, quien programe a menudo en la *shell* se dará
cuenta de que los dos conceptos resaltados, la interpolación y la captura y
filtrado de texto, son los puntos centrales de la programación en la *shell* de
Unix, hasta el punto que los bucles y los condicionales acaban quedando en un
segundo plano. Esto se debe a que, como se verá a continuación, potencia de la
*shell*, capaz de ejecutar programas y comandos internos muy poderosos, delega
las interacciones de bajo nivel en otras herramientas.


## Programación en la shell

Un programa de la *shell* es un conjunto de comandos agrupados de forma
estructurada en un archivo de texto que puede ejecutarse desde la *shell*.
Estos programas se conocen habitualmente como *shell scripts*, o simplemente
*scripts*, debido a que se comportan como un guión (*script*) que la *shell*
debe interpretar.

El proceso de interpretación puede resumirse como una lectura del archivo
comando por comando que va ejecutando los comandos a medida que los lee. El
mecanismo es el mismo que en el modo interactivo que se ha tratado durante todo
el documento pero, en este caso, la *shell* no esperará por los contenidos
introducidos manualmente, ya que ya dispone de un guión que interpretar.

Como ya se vio previamente, en este proceso de interpretación ocurren
expansiones que alteran el contenido del comando que termina por ejecutarse. En
este apartado se hace especial hincapié en éstas y otras capacidades de la
*shell* que también están presentes en el modo interactivo pero que en el modo
programático cobran especial importancia.

> NOTA: Hasta ahora los ejemplos han incluido el símbolo del *prompt* para
> indicar que se ejecutan en el entorno interactivo. En este apartado no se
> incluirá el *prompt* en el caso de que se trate del contenido de un *script*
> se la *shell* para diferenciarlo del contexto interactivo.

## Sintaxis general

La sintaxis de los programas de la *shell* ya se ha estudiado previamente sólo
que no se ha enfocado de esta forma. Quien haya seguido el documento puede
fácilmente entender qué acciones realiza un *script* sencillo ya que son
simplemente comandos añadidos uno tras otro.

Sólo se debe tener en cuenta que para que la *shell* sepa dónde terminan los
comandos, debe haber algún tipo de separación entre ellos.

La separación más habitual y sencilla es la misma que en el entorno
interactivo: el salto de línea.

La segunda, menos frecuente es el punto y coma, «`;`», que puede usarse para
separar dos comandos que se introduzcan en la misma línea. Lo mismo ocurre en
el modo interactivo, aunque no se haya trabajado[^interactive]:

``` bash
$ sleep 10; echo "Texto a mostrar una vez hayan pasado 10 segundos"
```

[^interactive]: En realidad son el mismo modo y la sintaxis es idéntica en
  ambos.

### Citado

La *shell* es un entorno que trata con cadenas de caracteres pero aplica
diferentes transformaciones sobre ellos de forma perfectamente natural.

La primera es la separación por palabras, cuyo objetivo es el de procesar en
primera instancia las cadenas de caracteres con las que la *shell* tiene que
lidiar. La separación por palabras encuentra caracteres que representan
espacios y separa las cadenas de caracteres en esos puntos. Esto le permite a
la *shell* separar un comando del siguiente, diferenciar argumentos de entrada
y otras muchas cosas más. Como se verá más adelante, se aplica en más contextos
de los que puede parecer a simple vista, pero de momento es suficiente con
comprender el mecanismo.

En ocasiones un comando puede requerir argumentos, como el nombre de un
archivo, que contengan espacios. Para estos casos la *shell* aporta varios
mecanismos siendo el más sencillo el **citado blando** (*soft quoting*) que
usando las comillas «`"`» delimita qué secciones del texto deben interpretarse
como un único elemento.

Otra forma de conseguirlo es mediante el **citado duro** (*hard quoting*) que
usa el apóstrofe «`'`» para delimitar la unidad de texto.

La diferencia entre ambos tipos de citado es que el primero, el blando, sigue
aplicando expansiones e interpolaciones mientras que el segundo, el duro,
siempre se considera de forma literal.

En cualquier caso de citado se pueden incluir saltos de línea o punto y coma
dentro del texto y la *shell* no interpretará que el comando termina ahí sino
que leerá hasta capturar el cierre del citado aplicado.

La tercera forma, pero quizás la peor para bloquear la separación por palabras
es el uso del carácter de escape (ver siguiente apartado), activando el
significado alternativo de los caracteres de espacio y convirtiéndolos así en
literales. La diferencia principal entre el citado y el escape radica en el
acceso al contenido de la cadena de caracteres a delimitar: el carácter de
escape debe introducirse en el elemento a escapar, cosa que no se conoce
siempre (el texto está en una variable, se trata de una sustitución de
comando...).

### Escape

El carácter de escape «`\`» sirve como en otros lenguajes de programación para
activar la interpretación alternativa del próximo carácter, el ejemplo quizás
más frecuente es «`\n`», que representa el salto de línea en lugar del carácter
literal «`n`», aunque hay varios caracteres más que exponen comportamientos
similares pero son los típicos de los lenguajes de programación y no interesa
listar todos.

En la *shell* este mecanismo es importante ya que es un entorno de modo texto
que abusa de las expansiones y las separaciones por palabras. Todos,
los símbolos que la *shell* considera especiales pueden escaparse de modo que
la *shell* los trate como literales y bloquear así su comportamiento especial.

Es decir, el escape sirve tanto para activar el significado especial de un
carácter común así como para activar el significado literal de un carácter
especial.

#### Continuación de línea

Uno de los casos más útiles del escape es la continuación de línea. Hay
comandos que pueden ser demasiado largos para incluirse en una única línea y
mantenerse legibles, por eso la *shell* ignorará los saltos de línea que estén
precedidos por «`\`», de este modo, los comandos pueden extenderse por varias
líneas sin que la *shell* los separe, facilitando así la legibilidad.

Al hacerlo en el entorno interactivo, la *shell* expondrá el segundo *prompt*,
por defecto «`>`», indicando que el comando aún no ha terminado y que
puede seguir introduciéndose, al igual que al introducir cualquier símbolo de
apertura que haya quedado sin cerrar:

``` bash
$ ls /var/log/* --tabsize=5 --recursive --reverse \
> --width=40
```

### Comentarios

Como en otros lenguajes de programación, existe la posibilidad de añadir
comentarios que son ignorados por el intérprete. En este caso, el símbolo de la
almohadilla, «`#`», indica que desde éste hasta el final de la línea todo debe
considerarse un comentario y por tanto debe ser ignorado por el intérprete.

## Ejecución de programas de la shell

El modo más sencillo para ejecutar un programa de la *shell*, un *script*, ya
se conoce: añadirle permiso de ejecución mediante `chmod` y ejecutar el
programa como un comando más. Hay que recordar que si el programa no se
encuentra en el `PATH` habrá que indicar su posición en el sistema de archivos
de forma clara, por ejemplo añadiendo «`./`» por delante de su nombre si el
archivo se encuentra en la carpeta actual.

Otro modo de ejecutar un programa de la *shell* es llamar a la *shell* en modo
no interactivo, esto es, con un archivo como argumento de entrada.

Los dos modos son prácticamente equivalentes, pero no lo son del todo. El
segundo no requiere que el *script* sea ejecutable ya que no se ejecuta como
programa sino que es la *shell* la que se ejecuta,  mientras que en el primer
caso es el *script* el que se lanza y la *shell* se ejecuta por el sistema para
poder interpretar el archivo.

También es posible cargar el contenido del programa a la sesión actual de la
*shell*, lo que ejecuta el programa como si hubiese sido tecleado manualmente
en la sesión actual. Esto tiene una ligera diferencia, y es que las variables
de ese *script* quedarán activas en el entorno actual, ya que es en éste, y no
en uno nuevo, dónde se ha ejecutado (es decir, esta opción no lanza una
*subshell*). Para cargar el programa en el entorno actual puede usarse el
comando interno `source` o el comando interno «`.`», que son equivalentes.

### Entorno de ejecución

El entorno de ejecución es un conjunto de valores que se almacena por cada
*shell*. En este entorno se mantienen los archivos abiertos, el directorio de
trabajo actual (`cd`), la máscara de creación de archivos (`umask`), los
parámetros de la *shell*, las funciones de la *shell* definidas en este
contexto, etc.

Varias estructuras como la llamada de comandos en segundo plano (`&`), las
tuberías sin nombre (`|`), la sustitución de comandos y los comandos entre
paréntesis se ejecutan en entornos derivados del entorno actual conocidos como
*subshell*.

Las *subshell* nacen como una copia de la *shell* actual pero no pueden acceder
al entorno que las llamó. Por tanto, pueden leer parámetros del entorno padre
pero si los reescriben los cambios no se traspasan a la *shell* madre. Esto es
importante ya que permite la comunicación con los subprocesos, pero manteniendo
la seguridad de que un comando asíncrono, como por ejemplo mediante la
ejecución en segundo plano, no altera el entorno actual en un momento
inesperado.

Las utilidades de la *shell* en general (los programas externos) también se
ejecutan en *subshells*, a excepción de los comandos internos (*builtin*) que
están específicamente diseñados para alterar el contexto actual.

El funcionamiento interno de la *shell* (`man 3 fork` y `man 3 exec`) es muy
esclarecedor de cómo funciona este sistema de *subshells*, pero no se
profundizará en esto hasta el próximo capítulo.

### Shebang

Cuando se ejecutan programas en texto, como el caso de los *scripts* de la
*shell*, el sistema no tiene por qué saber qué intérprete debe utilizar para
evaluarlos. La *shebang* es una línea que se escribe al comienzo de los
*scripts* para indicar al sistema qué intérprete debe utilizar. Este concepto
no se limita a la *shell*, sino que se incluye en todos los lenguajes de
programación interpretados. De este modo, el sistema operativo no interpretará
el archivo con un programa erróneo, como por ejemplo usar Python para evaluar
un *script* de Bash.

La *shebang* es un un comentario con el que el *script* debe
empezar[^comment-shebang] y sigue la siguiente sintaxis:

[^comment-shebang]: Ésta es una de las razones por las que la mayor parte de
  los lenguajes de programación interpretados definen «`#`» para comentarios.

``` bash
#!<comando intérprete>
```

Al encontrar la *shebang* el sistema ejecuta el comando indicado en la línea,
sin `#!`, y le inserta el cuerpo del archivo como entrada. A continuación se
muestran varios ejemplos: el primero para Bourne Shell, el segundo para Bash y
el tercero para Python, un lenguaje diferente.

``` bash
#!/bin/sh
echo "hola"
```

``` bash
#!/bin/bash
echo "hola"
```

``` bash
#!/usr/bin/python
print("Hola")
```


En ocasiones, como cuando se distribuye el archivo a otras computadoras, no es
fácil saber dónde se encuentra el intérprete que se desea utilizar así que
añade una capa de abstracción más mediante `env`, siendo este el encargado de
encontrar la localización del intérprete.

``` bash
#!/usr/bin/env python
print("Hola")
```


### Parámetros

Los parámetros de la *shell* son valores a los que se puede acceder usando su
nombre precedido por el símbolo del dólar, «`$`». La *shell* diferencia tres
tipos de parámetros: los parámetros posicionales, los parámetros especiales y
las variables.

En algunos casos los parámetros requieren del uso de las llaves «`{}`»
alrededor del nombre para evitar malas interpretaciones por parte de la
*shell*. El siguiente ejemplo muestra este fenómeno mediante variables de
entorno, un tipo de parámetro, que ya ha sido introducido en capítulos previos:

``` bash
$ export var=valor
$ export variable=valor_de_variable
$ echo ${var}iable
valoriable
$ echo $variable
valor_de_variable
```

#### Parámetros posicionales

Los parámetros posicionales están identificados por números y almacenan los
argumentos de entrada del *script* ordenados del mismo modo que en la llamada
al programa empezando por el número uno.

``` bash
$ echo 'echo $1,$2,$3' > test.sh
$ chmod +x test.sh
$ ./test.sh primer_argumento segundo_argumento tercer_argumento
primer_argumento,segundo_argumento,tercer_argumento
```

Los parámetros posicionales a partir del número nueve, esto es, cuando
necesitan más de un dígito, requieren del uso de las llaves tal y como se
mostró anteriormente.

#### Parámetros especiales

Los parámetros especiales almacenan información adicional y están identificados
por los símbolos: `0*@#$?_!-`.

- `$0`: nombre del *script* actual tal y como se ejecutó.
- `$*` y `$@`: incluyen todos los argumentos de entrada del script
  actual[^all-args].
- `$#`: número de argumentos de entrada.
- `$$`: PID del proceso actual.
- `$?`: código de salida de la ejecución del último programa.
- `$_`: último argumento del último programa.
- `$!`: PID del último proceso ejecutado en segundo plano (*background*).
- `$-`: las banderas (*flags*) activas.

[^all-args]: Se diferencian en el formato: `$*` incluye los argumentos en una
  sola cadena de caracteres mientras que `$@` los incluye en una lista ya
  separada.

#### Variables

Las variables son parámetros con nombres formados por letras, números y el
símbolo de la barra baja, «`_`». Para diferenciarlas de otros parámetros,
siempre deben comenzar con una letra o «`_`».

El uso de las variables es diferente a los otros parámetros porque estás están
diseñadas para ser asignadas por quien está programando y no por la propia
*shell*. De todos modos, la *shell* también incluye algunas variables por
defecto que muestran su configuración.

Las variables se asignan mediante el símbolo de igual, «`=`». Es importante que
no admite espacios alrededor:

``` bash
$ variable=valor
$ echo $variable
valor
```

El ámbito de las variables funciona por proceso, es decir, sólo el proceso
actual puede ver las variables activadas.

``` bash
$ echo 'echo $HELL' > test.sh && chmod +x test.sh
$ HELL="YEAH"
$ ./test.sh              # No accesible

$ HELL=YEAH ./test.sh    # Accesible
                         # Sintaxis especial para introducir la variable en el comando
YEAH
$ export HELL="YEAH"     # Accesible por usar `export`
$ ./test.sh
YEAH
```

Las variables de entorno introducidas en capítulos anteriores mediante el
comando interno `export` tienen la particularidad de que su valor se expone al
entorno completo del sistema. Es por eso que es mucho más útil usar `export` en
la configuración del sistema, porque interesa que todos los programas, no sólo
la *shell* actual, conozcan las variables asignadas.

### Sustitución de comandos

Frecuentemente es interesante obtener el resultado de la ejecución de un
comando en una variable. Como las redirecciones ya se conocen es lógico pensar
que debe haber un modo de redireccionar a una variable, pero no es así: las
redirecciones sólo funcionan con archivos.

Para recoger la salida de un comando en una variable se usa la siguiente
sintaxis:

``` bash
resultado=`comando a ejecutar`
```

En versiones recientes la sintaxis se sustituye por `$()`:

``` bash
resultado=$( comando a ejecutar )
```

En ambos casos, la salida estándar del comando se almacena en la variable con
nombre `resultado`. Si desea capturarse la salida de errores es necesario
redireccionarla a la salida estándar en el propio comando a ejecutar.

La sustitución de comandos ocurre en una *subshell* por lo que es un proceso
que no puede interferir en el programa actual ya sea alterando el contenido de
variables o abriendo y cerrando archivos.

### Control de flujo

El control de flujo es un componente importante ya que permite la ejecución
condicional o repetida de listas de comandos.

Una lista de comandos es cualquier consecución de comandos separados por un
salto de línea, punto y coma, el símbolo et («`&`») o cualquier operador de
control. Dicho de una forma más llana: una lista es un conjunto de comandos.

#### Código de salida

Antes de profundizar en las estructuras u operaciones de control es importante
considerar el mecanismo detrás de estas construcciones. El código de salida,
presentado como el parámetro `$?` es un valor numérico entre `0` y `255`
retornado por los programas (ver `man 3 exit`) para exponer si han terminado
correctamente: un 0 implica que el programa ha terminado correctamente y
cualquier otro valor muestra algún tipo de error.

Este mecanismo es genérico e independiente de la *shell*, pero la *shell* lo
utiliza para gestionar diversas tareas, entre ellas el control de flujo. La
*shell* considera como un valor verdadero un código de salida de `0` y
considera como falso cualquier otro valor.

El valor de retorno de un *script* completo será el retornado por su último
comando, aunque siempre que se desee asignar uno concreto puede usarse el
comando interno `exit`.

Para ciertos casos también es interesante conocer los comandos `true` y `false`
que únicamente asignan `$?` a un valor verdadero (es decir 0) o un valor falso,
respectivamente. Similar a `true`, también existe el comando «`:`», que
representa el comando vacío, y, por consecuencia, siempre termina
correctamente.

#### Operadores de control

Antes de comenzar a estudiar las diversas estructuras de control que la shell
aporta es interesante comenzar por los operadores de control ya que son una
versión reducida del mismo concepto.

Los operadores de control son los siguientes: «`||`» y «`&&`» que representan
el operador lógico OR y AND respectivamente. El operador OR retornará un valor
verdadero cuando uno de los dos lados haya resultado en valor verdadero y falso
en cualquier otro caso, mientras que el operador AND retornará un valor
verdadero cuando ambos lados sean verdaderos y falso en caso contrario.

Ambos operadores funcionan además como operadores de cortocircuito (*short
circuit operator*), lo que significa que evalúan su lado izquierdo en primer
lugar y deciden si es necesario evaluar el otro en función del resultado del
primero. Por ejemplo, un AND cuyo primer valor ya sea falso nunca podrá ser
verdadero independientemente del segundo valor, por lo que no necesita
evaluarlo. Entonces, el operador «`||`» sólo ejecutará el lado derecho si el
lado izquierdo ha terminado con un valor falso mientras que el operador «`&&`»
sólo ejecutará el lado derecho si el lado izquierdo ha terminado con un valor
verdadero.

Estas construcciones son muy interesantes para casos sencillos que requieren
ejecutar un comando sólo en el caso de que el anterior falle (`||`) o se
ejecute de forma correcta (`&&`):

``` bash
$ ls archivo || echo "El archivo no existe"
$ ls archivo && echo "El archivo existe"
```

Por supuesto, para facilitar la gestión del los códigos de salida, la *shell*
aporta también el operador lógico NOT mediante el símbolo de la exclamación
«`!`», que invierte el significado del código de error del comando al que se le
aplique.

``` bash
$ true
$ echo $?
0
$ ! true
$ echo $?
1
```

#### Agrupación de comandos con preferencia

A la hora de aplicar operadores de control, es interesante tener la capacidad
de combinar comandos y operaciones con diferentes niveles de prioridad, como
ocurre en las matemáticas con el uso de paréntesis.

La *shell* aporta esta funcionalidad mediante las paréntesis, que evalúan su
contenido con alta prioridad.

El siguiente ejemplo ejecutará en primer lugar la sección entre paréntesis,
resolverá el código de salida de ésta y después aplicará el siguiente `||`,
todo gracias las prioridades.

``` bash
( rm -r /etc/app/* && echo "config" > /etc/app/conf ) || echo "Config failed"
```

Las agrupación de comandos los trata como si se trataran de uno sólo, con todo
lo que ello conlleva: el código de salida de la agrupación será el de su último
comando (como ocurre en cualquier consecución de comandos), las redirecciones
aplican a la agrupación completa, etc.

La agrupación de comandos tiene un detalle especial: se ejecuta en una
*subshell*, por lo que sus alteraciones de variables, etc. quedan separadas de
la ejecución del *script* actual.

#### Estructuras de control

Las estructuras de control explotan el mismo comportamiento pero, en lugar de
funcionar a nivel de comando, pueden trabajar sobre listas de comandos en su
cuerpo. Las estructuras de control son las mismas que pueden encontrarse en
muchos otros lenguajes de programación y su funcionamiento es prácticamente el
mismo

##### if

La estructura `if` tiene la siguiente forma en su formato más sencillo:

```
if <lista de comandos para la condición>
then
    <lista de comandos>
fi
```

Un ejemplo de su sintaxis podría ser el siguiente:

``` bash
if [ -e "ruta/a/un/archivo" ]; then
    echo "ERROR: El archivo no existe" >&2
    exit 1
fi
```

En esta pieza de código se usa el comando interno `test` mediante su sinónimo
`[` (ver `help test` y `help [`) para comprobar la existencia de un archivo. El
cuerpo del `if` no se ejecutará a menos que el archivo exista.

Al ver `help if` puede obtenerse la sintaxis completa, que incluye la
posibilidad de añadir bloques `elif` para añadir nuevas condiciones y un bloque
`else` para los casos que no se cumplan. Como en cualquier otro lenguaje de
programación, sólo se ejecuta uno de los bloques: el primero en cumplir la
condición.

##### case

El `case` es una estructura muy potente que permite la comprobación de patrones
ya introducidos previamente. La sintaxis más simple es la siguiente:

```
case <palabra> in
    <patrón>) <lista de comandos> ;;
    <patrón>) <lista de comandos> ;; ## opcional
esac
```

El detalle más peculiar del `case` es el uso del doble punto y coma (`;;`) para
delimitar el final de la lista de comandos y el siguiente caso. Es un detalle
necesario para que puedan diferenciarse las secciones correctamente y no está
presente en ninguna otra estructura de la *shell*.

Por lo demás, uno de los casos más útiles para el uso de `case` es el de
analizar cadenas de caracteres mediante patrones. El siguiente ejemplo sólo
retorna un valor verdadero si el argumento de entrada está formado por números:

``` bash
case $1 in
    *[!0-9]*) false;;
    *) true ;;
esac
```

##### while y until

`while` (mientras que) y `until` (hasta que) son dos comandos muy similares
pero cuya lista de condición se evalúa de forma opuesta. Por lo demás, la
sintaxis es la misma excepto por la palabra clave a utilizar y su
funcionamiento es idéntico: ejecutar su cuerpo repetidas veces. Lo único que
cambia es la condición de salida: en el `while` el cuerpo se ejecuta *mientras
que* la condición sea cierta y en `until` *hasta que* lo sea.

La sintaxis es muy sencilla:

```
while <lista comandos de condición>
do
    <lista de comandos>
done
```

##### for

Los bucles `for` son algo más avanzados, ya que permiten iterar sobre una lista
de elementos. Su sintaxis es similar a la de `while` y `until` pero su
condición pasa a ser un bloque de definición de iteración, del siguiente modo:

```
for <variable> in <iterable>
do
    <lista de comandos>
done
```

Un ejemplo sencillo y algo inútil puede ser iterar por varias palabras:

``` bash
for var in Lunes Martes Miércoles Jueves Viernes Sábado Domingo
do
    echo $var  # `var` iterará sobre los días de la semana
done
```

Aunque el ejemplo mostrado es relativamente absurdo, como esta estructura es
muy frecuente, se trabajarán más ejemplos más adelante aportando herramientas
que permiten sacarle todo el partido a esta estructura.

##### break y continue

Los comandos `break` y `continue` afectan a los bucles, tanto `for` como
`while` y `until` para simplificar las condiciones. `break` termina el bucle
actual y `continue` termina la iteración actual y pasa a comprobar y, si es
necesario, ejecutar la siguiente.


### Nota práctica: El primer programa

Suponiendo que el usuario dispone de un directorio `scripts`, que contiene
*scripts* sin permiso de ejecución y con la extensión `.sh`, desea mover los
*scripts* a un directorio llamado `bin`, ubicado en el mismo directorio y que
contiene otros programas, eliminando su extensión y añadiendo permisos de
ejecución.

Esta sería la situación actual simplificada:

``` bash
$ find
./bin
./bin/prog3
./bin/prog1
./bin/prog2
./scripts
./scripts/b.sh
./scripts/c.sh
./scripts/a.sh
```

> NOTA: Al igual que en el ejemplo se espera que no exista colisión entre
> nombres.

Una solución sencilla puede partir del uso de un bucle que itere por todos los
elementos en el directorio `scripts` y vaya realizando las alteraciones
pertinentes. El programa `basename` puede realizar la tarea de
eliminación de la extensión y `chmod` puede encargarse del cambio de permisos:

``` bash
#!/usr/bin/env bash

for FILE in `ls scripts/`; do
    NEW_NAME=bin/`basename $FILE .sh`
    mv $FILE $NEW_NAME
    chmod +x $NEW_NAME
done
```

Hay varios puntos interesantes a notar en el programa. El bucle itera sobre el
resultado de una llamada a `ls`, ejecutada mediante la sustitución de comandos.
El cuerpo del bucle asigna una nueva variable `NEW_NAME` mediante la
concatenación del prefijo `bin/` seguido del resultado de la ejecución de
`basename` sobre el nombre original del archivo, de nuevo mediante una
sustitución de comando. Después se mueve el archivo original a destino y se
cambia el modo del archivo de destino.

Este programa ubicado en la carpeta de trabajo es capaz de realizar el cambio
propuesto pero hay varios puntos que merece la pena señalar.

El primero es qué **ocurre cuando los archivos en el directorio `scripts` tienen
nombres que contienen espacios**. El resultado del comando `ls` se evalúa a una
cadena de caracteres que es después seccionada por palabras para que el bucle
pueda iterar sobre ella, esto hace que los archivos con nombres de varias
palabras sean divididos y el programa falle. Para evitarlo, en lugar de
utilizar `ls` y la sustitución de comandos, es más adecuado utilizar una
expansión ya que las expansiones no aplican separación por palabras.

En segundo lugar, las **llamadas a programas externos afectan negativamente al
rendimiento y es mejor evitarlas al máximo**. Los programas externos como
`basename` o `ls` lanzan un proceso nuevo en una *subshell*, con las
implicaciones de rendimiento que esto implica. En muchas ocasiones es posible
sustituir estas llamadas por capacidades internas de la *shell*.

Añadiendo los cambios indicados el programa es el siguiente:

``` bash
#!/usr/bin/env bash

for FILE in scripts/*; do
    WITHOUT_EXT=${FILE%\.sh}                # Elimina la extensión
    NEW_NAME=bin/${WITHOUT_EXT#scripts/}    # Sustituye el directorio
    mv $FILE $NEW_NAME
    chmod +x $NEW_NAME
done
```

En este caso se han utilizado expansiones de parámetros que aún no se han
estudiado, para así evitar el uso de `basename` y mostrar un adelanto de lo que
se trabajará próximamente.

Sin embargo, **el programa sigue fallando al procesar nombres con múltiples
palabras** pero en este caso se debe a una razón distinta. La expansión usada
en el bucle es la correcta, pero cuando un nombre tiene varias palabras como
por ejemplo «`scripts/dos palabras.sh`» el comando `mv` que se ejecuta tiene la
siguiente forma:

``` bash
mv scripts/dos palabras.sh bin/dos palabras
```

De este modo el comando `mv` no está ejecutándose correctamente, porque la
separación por palabras está enviando cuatro argumentos al programa en lugar de
dos. Para evitar esto, las variables deben citarse correctamente. Aplicada del
siguiente modo:

``` bash
mv "$FILE" "$NEW_NAME"
```

La expansión sería la correcta:

``` bash
mv "scripts/dos palabras.sh" "bin/dos palabras"
```

Los modos de cita, las expansiones y el mecanismo de separación por palabras
son conceptos avanzados del uso de la *shell* que facilitan la interacción con
ésta. Quien no esté familiarizado con estos conceptos tendrá complicaciones
para tanto comprender por qué sus programas fallan como para escribir programas
con un comportamiento predecible. Los siguientes apartados profundizan en estos
conceptos más en detalle y añaden nuevo vocabulario.

Antes de avanzar, ésta es una posible solución correcta al programa propuesto:

``` bash
#!/usr/bin/env bash

for FILE in scripts/*; do
    WITHOUT_EXT=${FILE%\.sh}                # Elimina la extensión
    NEW_NAME=bin/${WITHOUT_EXT#scripts/}    # Sustituye el directorio
    mv "$FILE" "$NEW_NAME"
    chmod +x "$NEW_NAME"
done
```

## Conceptos avanzados

Una vez analizado de forma general en funcionamiento de la *shell* es
interesante adentrarse en los conceptos que se han aplicado en el proceso y
profundizar en cada uno de ellos de forma independiente. De este modo, lo que
se ha recibido como conocimiento intuitivo se afianza definitivamente.

### Argumentos y opciones

Hasta el momento el uso de los argumentos de los diversos programas no se ha
estudiado más allá de explicar que éstos se separan por palabras, mecanismo que
puede controlarse tanto con el citado como con el escape. Y en realidad no hay
mucho más que contar a nivel de funcionamiento interno, pero la práctica
requiere ciertas notas adicionales.

#### Argumentos y separación por palabras

En el programa mostrado en la última práctica se sustituye una llamada al
comando `ls` por una expansión para asegurar que el bucle itere de forma
adecuada y no rompa los nombres de los archivos que contienen espacios.

Esto mismo ocurre con los parámetros `$*` y `$@` que incluyen una lista de los
argumentos de entrada del programa. El primero los incluye sin separar, lo que
no permite una iteración adecuada en un bucle, ya que se aplicaría una
separación por palabras y los argumentos con espacios acabarían rotos. El
segundo, por otro lado, se almacena en una lista ya separada, por lo no hay
lugar a malos entendidos. Este detalle puede probarse con el siguiente
programa:

``` bash
#!/usr/bin/env bash

for A in $*; do
    echo -${A}-
done

for B in "$@"; do
    echo -${B}-
done
```

#### Opciones

Las opciones son un tipo de argumento de entrada que altera el comportamiento
del comando a ejecutar. En el siguiente comando, la opción `-l` indica que el
resultado debe mostrarse en modo lista, es decir, altera el comportamiento del
comando, mientras que el argumento `directorio` indica qué directorio debe
mostrarse dando más detalle al comando:

``` bash
ls -l directorio
```

Generalmente, aunque no sea estándar, los comandos de Unix disponen de dos
formas de indicar opciones: la versión corta y la versión larga. La versión
corta es una de las que más se ha visitado:

``` bash
ls -l   # -l es una opción corta
```

Las opciones cortas además pueden combinarse:

``` bash
# Comandos equivalentes:
ls -l -a -t
ls -lat
```

La versión larga es menos frecuente pero a veces es más representativa:

``` bash
ls --quote-name # --quote-name es una opción larga
```

En caso de que requieran valores, la versión corta los incluye como el
siguiente argumento y la versión larga los incluye usando el símbolo «`=`»:

``` bash
# Comandos equivalentes
ls -w 10
ls --width=10
```

Como bien se ha dicho previamente, las opciones son argumentos de entrada como
cualquier otro por lo que el comando que sigue recibe dos argumentos de
entrada, `-laht` y `directorio`, y es su responsabilidad procesarlos:

``` bash
ls -laht directorio
```

Como se puede observar, al ser responsabilidad del comando el procesar los
argumentos, el uso del guión para indicar opciones cortas o el guión doble para
indicar opciones largas también es una decisión de la implementación del
comando. Hay muchos ejemplos de comandos que se saltan este estándar de facto
(`dd`, `find`, `chmod`...), pero se recomienda seguirlo siempre que sea posible
en los *scripts* y programas que cada cual desarrolle.

Cualquiera que intente crear un *script* o programa que gestione las opciones
de la forma recién descrita y que incluso lance un mensaje de error, como
muchos programas hacen, cuando se introduzcan opciones incompatibles o erróneas
descubrirá que no es tarea sencilla hacerlo. Para facilitarlo, existen tanto el
programa `getopt` como el comando interno `getopts`, compatible con POSIX.

### Redirección avanzada

Aunque las redirecciones ya se han estudiado en bastante detalle, es
interesante comprobar un par de detalles sintácticos para poder aprovecharlas
al máximo.

Las sintaxis de las redirecciones es un poco más general que lo que se
introdujo en el capítulo previo. En realidad siguen la siguiente estructura:

- `número<archivo` , `número>archivo`, `número>|archivo` y `número>>archivo`
  son las redirecciones estudiadas. El número indica el descriptor de archivo a
  utilizar, mientras que el archivo indica el nombre del archivo al o del que
  desea hacerse la redirección. La redirección `>|` sobreescribe el archivo sin
  realizar comprobaciones mientras que `>` comprueba si la *shell* está
  configurada para sobreescribir o para lanzar un error si el archivo ya
  existe.
- `número<>archivo` se introdujo de forma sutil en el capítulo previo, pero no
  llegó a profundizarse. Sirve principalmente para comunicarse con terminales y
  *sockets*. Redirecciona tanto la salida como la entrada al archivo al
  descriptor de archivo indicado por el número.
- `número<<[-]nombre` se trata de un *here document*, que se estudian en el
  siguiente apartado.
- `número<&-` y `número>&-` cierran el descriptor de archivo indicado por el
  número. El primero actúa en descriptores de entrada y el segundo en
  descriptores de salida.
- `número<&palabra`, `número>&palabra` duplican el descriptor de entrada (`<&`)
  o salida (`>&`) con el descriptor que resulta de expandir la palabra, lo que
  hace que escribir o leer de cualquiera de los dos descriptores tenga el mismo
  efecto. Si la palabra expande a «`-`» se activa el caso anterior y el
  descriptor se cierra.

#### Here documents

En ocasiones las redirecciones pueden resultar un poco incómodas de utilizar
cuando el texto que se desea enviar ya se conoce ya que implican añadirlo a un
archivo independiente.

Para casos como éste, las redirecciones de entrada se pueden realizar
directamente desde texto escrito en la *shell* utilizando un *here document*:

``` bash
TESTS=${@}
ssh riscv <<-ENDSSH
    cd lightening
    git fetch --all && git reset --hard origin/main
    if [ -z "${TESTS}" ]; then
        make -C tests test-riscv CC_RISCV="riscv64-linux-gnu-gcc -static"
    else
        make -C tests test-riscv TESTS="${TESTS}" CC_RISCV="riscv64-linux-gnu-gcc -static"
    fi
ENDSSH
```

Los *here documents* como su nombre en inglés indica («documentos aquí») son
literales que pueden insertarse en mitad del script de shell como si se tratara
de archivos que se leen a través de la entrada estándar.

El contenido del *here document* se trata como un string delimitado por
comillas dobles (`"`), por lo que puede aplicar expansiones (ver `TESTS` en el
ejemplo).

La forma básica es usar `<<` seguido de la palabra de finalización, `ENDSSH` en
el ejemplo. Las palabras de finalización más comunes son `END` o `EOF`.

En el caso mostrado se utiliza el símbolo `-` entre el `<<` y la palabra de
finalización para indicar que se desea eliminar la indentación al inicio de
cada línea para facilitar la lectura del código. De no usarlo, el texto
incluiría la indentación, que en este caso particular no tiene ningún efecto en
el resultado.

Como las redirecciones respetan el orden, es posible añadir más después del
*here document* sin que lleve a equivoco. Por ejemplo:

```
$ cat <EOF >salida
> texto
> EOF
$ cat salida
texto
```

#### Nota práctica: Creación de archivos

Es una práctica habitual usar el programa `touch` para crear archivos vacíos,
aunque su funcionalidad es la de actualizar la fecha de edición de los
archivos. Sin embargo, una vez vista la potencia de la *shell*, se puede
esperar que haya algún otro modo de simplificar la creación de archivos que no
requiera ejecutar un programa externo, y así es. Es tan sencillo como lo
siguiente:

``` bash
$ >"nombre de archivo"
```

Este es uno de esos detalles que puede mejorar mucho el rendimiento de los
programas.

### printf

Durante casi todos los ejemplos del documento se utiliza el comando interno
`echo` para visualizar texto en la pantalla, pero quizás no sea el mejor
comando a utilizar si se desean aplicar formatos avanzados.

El comando `echo` varía de una implementación a otra por lo que su salida puede
ser un poco impredecible. En programas cuyo texto de salida sea un simple
mensaje puede no ser problemático, pero en los que requieran una salida más
cuidada se recomienda usar `printf`.

`printf` es un comando mucho más poderoso que permite detallar qué formato se
desea utilizar para la visualización (ancho del texto a usar, número de dígitos
al visualizar números...). De esta manera, los programas pueden asegurar que su
salida siempre se visualizará del mismo modo.

Además de esto, `printf` añade la opción `-v` que permite la escritura de texto
directamente a una variable, de este modo se evitan sustituciones de comandos y
se obtiene una mejora de rendimiento sustancial.

### read

`read` también es un comando interno muy interesante. Su principal función es
la de obtener una línea (una cadena de caracteres terminada en salto de línea)
por la entrada estándar o un archivo y separarla por palabras, asignando cada
palabra a un identificador. Como su código de salida se vuelve falso al
encontrar el final del archivo, es un comando muy útil para aplicar en bucles
`while` y procesar archivos completos línea a línea, obteniendo información de
sus columnas.

Al leer la documentación de `read` (`help read`) se menciona que su sistema de
separación por palabras utiliza `$IFS`.

#### IFS

`IFS`, *internal field separator*, es uno de muchos parámetros de la *shell*.
Este parámetro define cómo debe aplicarse la separación por palabras y cómo
debe funcionar el comando `read`.

Su valor por defecto es `<espacio><tabulación><salto de línea>` lo que
significa que las palabras se separarán con cualquiera de estos tres elementos.
Cambiar el valor de IFS altera cómo deben aplicarse estas separaciones, así que
es fundamental conocer la existencia de este parámetro y tratar de no
sobreescribirlo por accidente. En ocasiones será interesante alterarlo, pero
deberá hacerse con cuidado, retornándolo a su valor original después de su uso,
para evitar alterar el comportamiento de todos los comandos posteriores que
vayan a utilizarse.


### Expansión y procesamiento de línea

Hasta ahora, se han tratado la separación por palabras y las expansiones de
forma independiente y casi mágica pero la realidad es que la *shell* sigue un
orden concreto para aplicarlas que ayuda a comprender el porqué de muchas
advertencias que se han ido introduciendo en este documento.

Cuando ejecuta una línea, ya sea en la interfaz de comandos o en un programa,
la *shell* separa la línea por palabras, allí donde encuentre un espacio sin
citar o escapar. Después trata de analizar cada una de las palabras aplicando
varias expansiones, una detrás de la otra hasta finalmente enviar los
argumentos al comando.

1. Expansión por tilde: descrita en apartados previos, se trata de la
   conversión de la diversas combinaciones usando la tilde, «`~`», a nombres de
   archivo. Si la *shell* no puede realizar la conversión mantiene el símbolo
   literal.
2. Expansión de parámetros: mediante el uso del símbolo del dólar, «`$`», y las
   llaves cuando sea necesario, tal y como ya se ha expresado anteriormente.
3. Expansión aritmética: aunque no se ha introducido, la *shell* es capaz de
   tratar con expresiones aritméticas, para ello deben seguir la siguiente
   sintaxis: `$(( <expresión aritmética> ))`, aunque la sintaxis `$[ <expresión
   aritmética> ]` también está aceptada. No son muy habituales, ya que la
   *shell* suele trabajar con texto.
4. Sustitución de comandos: ya sea usando los *backticks*, « \`», o `$()`, como
   ya se ha introducido previamente.
5. Separación por palabras mediante `$IFS`: de nuevo, la *shell* aplica
   separación por palabras a los resultados de las expansiones previas, debido
   a que puede que éstas sean ahora nuevos comandos o argumentos de ellos.
6. Expansión de nombres de archivo: tal y como se han introducido previamente,
   aunque se detallan más adelante, siempre que la *shell* encuentre alguno de
   los caracteres «`*?[`» sin citar o escapar tratará de aplicar una expansión
   mediante el uso de un patrón sustituyendo el texto por cualquier nombre de
   archivo que se ajuste al patrón. Si no hay nombres que se ajusten al patrón,
   el texto se mantiene literal.

De esta lista de expansiones la primera conclusión que se puede obtener es que
sólo hay un paso que pasa por la separación por palabras, que es la expansión
de nombres de archivo. Esto explica por qué es más interesante usar expansiones
de nombres de archivo en lugar de llamadas a `ls`[^quoted-ls]: al no aplicar
la separación por palabras, la expansión no se ve afectada y los nombres se
gestionan correctamente.

[^quoted-ls]: Aunque `ls` disponga de una opción que cita los nombres
  correctamente.

Puede chocar que la separación por palabras aplique después de evaluar
expansiones pero entenderlo es tan sencillo como evaluar el siguiente ejemplo:

``` bash
$(echo "ls -lat .")
```

Esta sustitución de comando expande a «`ls -lat .`». Lo que no es tan evidente
es que ese comando acaba ejecutándose porque no está siendo capturado como
cadena de caracteres en ninguna parte. A la hora de ejecutar ese comando `ls`,
se requiere de la separación por línea. Es curioso este caso porque deja claro
que todo en la *shell* es una cadena de caracteres, incluso los propios
comandos.

### Patrones

Los patrones son un mecanismo muy potente de la *shell*. Aparecen tanto en la
expansión de nombres de archivo como en las estructuras `case` y permiten
resolver muchos casos de uso complicados.

En su modo más sencillo, la *shell* buscará los caracteres «`?`», «`*`» y «`[`»
y, si los encuentra, evaluará la palabra completa como un patrón. En los
patrones, de modo similar a como ocurre en las expresiones regulares, los
caracteres, menos algunas contadas excepciones, únicamente coincidirán con sí
mismos. Es precisamente en estas excepciones donde radica el interés de los
patrones. Si se desea que las excepciones se evalúen como un literal y sólo
coincidan con sí mismas, basta con usar el carácter de escape, como siempre.
Estos son los caracteres que representan una excepción:

- «`?`»: coincide exactamente con un único carácter cualquiera. En la expansión
  de nombre de archivo, no incluye el separador de archivos `/` o el punto al
  inicio del nombre de un archivo que implicaría que se trate de un archivo
  oculto.

- «`*`» : coincide con cero o más caracteres. Al tratarse de expansiones
  de nombre de archivo también aplican las mismas restricciones que en el caso
  previo, para evitar activar casos indeseados.

- «`[`» : este caso es una apertura de una clase de caracteres, su sintaxis
  completa implica incluir una lista de caracteres o un rango y cerrarla con el
  corchete de cierre, tal que así: `[<lista de caracteres>]`. Este caso
  coincidirá con un único carácter incluido en la lista. Si se desea añadir un
  rango, puede usarse el guión del siguiente modo: `[A-Za-z]`, lo que
  coincidiría con cualquier carácter de la A (mayúscula) a la Z (mayúscula) o
  de la a (minúscula) a la z (minúscula). Si se desea, este bloque también
  puede invertirse mediante el símbolo de la exclamación «`!`» añadido después
  de la apertura, por ejemplo `[!A-Za-z]`, lo que tendría el efecto opuesto,
  coincidiendo con cualquier carácter que no esté en la lista. Además de esto,
  el estándar POSIX define unas clases de carácter que pueden incluirse
  directamente sin necesidad de describirlas al detalle de forma manual tales
  como `alpha`, que deben indicarse entre dos puntos: `[:alpha:]`. Para más
  información leer el manual de la *shell*.


También es posible construir patrones avanzados basados en repeticiones, para
ello se usan las siguientes estructuras:

- `?(<lista de patrones>)`: coincide con ninguna o una aparición de los
  patrones entregados.
- `*(<lista de patrones>)`: coincide con cero o más apariciones de los patrones
  entregados.
- `+(<lista de patrones>)`: coincide con una o más apariciones de los patrones
  entregados.
- `@(<lista de patrones>)`: coincide con uno de los patrones entregados.
- `!(<lista de patrones>)`: coincide con cualquier cosa menos con los patrones
  entregados.

Estas estructuras incluyen listas de patrones en su interior que se definen
como cualquier secuencia de patrones separada por «`|`», para indicar un OR
lógico, o «`&`» para indicar un AND lógico.

Sobre los conceptos recién introducidos pueden construirse configuraciones
avanzadas. Aquí algunos ejemplos:

- `[!a-f]*.c`: coincide con todos los archivos terminados en `.c` que empiecen
  con una letra que no esté en el rango de la `a` a la `f`.

- `test_?([fr]un|log)_file`: coincide con nombres de archivo como:
  `test__file`, `test_fun_file`, `test_run_file` y `test_log_file`.

- `Arch+([0-9])`: coincide con nombres que comiencen por `Arch` y terminen con
  uno o más dígitos.

- `@(install|setup|config).sh`: coincide con `install.sh`, `setup.sh` y
  `config.sh`.

Los patrones son poderosos pero pueden ser lentos, sobre todo si deben
comprobar muchas alternativas. Aunque normalmente la mejor solución es
mantenerse utilizar funcionalidades internas de la *shell*, en este caso, para
casos complejos es interesante plantear el uso de motores de expresiones
regulares tales como los que se pueden encontrar en `awk`, `grep` u otras
herramientas.

> NOTA: como recordatorio, cuando se trata de expansiones por nombre de
> archivo, la *shell* mantiene el texto literal si no es capaz de encontrar
> ningún archivo que cumpla el patrón.

Todo el funcionamiento aquí mostrado normalmente puede modelarse mediante
opciones de la *shell*. El sistema de patrones avanzados puede desactivarse, y
también la expansión de nombres de archivos. Para ello, se recomienda estudiar
las opciones de la *shell* en el manual correspondiente.

### Expansión de parámetros avanzada

Bien estudiados los patrones, es interesante analizar la expansión de
parámetros que aporta la *shell*, que es mucho más avanzada que lo que se ha
propuesto hasta ahora.

Las simples expansiones propuestas al introducir las variables simplemente
sustituyen un nombre de variable por su valor, pero la *shell* es capaz de
utilizar ese momento de la expansión para realizar diversas labores.

Una de las más sencillas es usar un **valor por defecto** para los casos en los
que las variables no estén definidas:

- `${var:-default}`: comprueba si la variable `var` no está definida o si está
  vacía, en caso de estarlo, la expansión resuelve al valor añadido en
  `default`.
- `${var-default}`: similar al caso anterior, analiza únicamente si la variable
  está sin definir, sin comprobar el caso de que esté vacía.

De modo muy similar, las siguientes dos expansiones también trabajan con
valores por defecto, pero en este caso asignan el valor a la variable además de
exponer el valor en la expansión. La sintaxis y el sentido es similar, pero
cambian el guión, «`-`», por el símbolo del igual, «`=`».

- `${var:=default}`: comprueba si la variable `var` no está definida o si está
  vacía, en caso de estarlo, la expansión resuelve al valor añadido en
  `default` y la variable se asigna al valor `default`.
- `${var=default}`: similar al caso anterior, analiza únicamente si la variable
  está sin definir, sin comprobar el caso de que esté vacía.

De modo opuesto, es posible resolver a un valor alternativo en caso de que una
variable esté definida.

- `${var:+alternate}`: comprueba si la variable `var` no está definida o si
  está vacía, en caso de no estarlo, la expansión resuelve al valor añadido en
  `alternate`.
- `${var+alternate}`: como en casos anteriores, este caso sólo comprueba si la
  variable está definida, no comprueba si está llena.

Del mismo modo, también es posible visualizar errores si no está definida una
variable, usando `:?` o `?`, aunque quizás tengan menos interés.

Además este tipo de sustituciones, la *shell* puede alterar el contenido
expuesto aplicando los recién introducidos patrones.

- `${var%PATTERN}`: elimina el patrón `PATTERN` del final de la cadena de
  caracteres `var`, capturando su aparición más corta.
- `${var%%PATTERN}`: igual que la anterior, pero capturando la aparición más
  larga.
- `${var#PATTERN}`: elimina el patrón `PATTERN` del inicio de la cadena de
  caracteres `var`, capturando su aparición más corta.
- `${var##PATTERN}`: igual que la anterior, pero capturando la aparición más
  larga.
- `${var/PATTERN/str}`: sustituye la primera aparición del patrón `PATTERN` de
  la cadena de caracteres `var`, sustituyéndola por la cadena `str`. En caso de
  omitir `/str`, la cadena simplemente de elimina.
- `${var//PATTERN/str}`: similar al caso anterior, pero en esta expansión se
  sustituyen todas las apariciones del patrón en la cadena de caracteres.
- `${var/#PATTERN/str}`: similar a los anteriores pero con el patrón aplicado
  en los primeros caracteres de `var`.
- `${var/%PATTERN/str}`: similar a los anteriores pero con el patrón aplicado a
  los últimos caracteres de `var`.

Las expansiones de parámetros recién introducidas son muy interesantes para
tratar con rutas a archivos, ya que permiten eliminar o alterar directorios y
extensiones con mucha facilidad.

Algunas expansiones adicionales son también interesantes, como `${#var}` que
cuenta la longitud de `var`, o `${var[<expresión aritmética>]}` que sirve para
tratar con conjuntos de valores (*array*). Los *array* son una funcionalidad
avanzada que no es especialmente interesante para los *scripts* que suelen
desarrollarse en el día a día, de todos modos están documentados al detalle en
la *shell* así que quien los necesite siempre puede leer la documentación
correspondiente.

### Funciones

Todo lo aprendido hasta ahora tampoco tiene especial sentido si no se puede
reutilizar el código de forma cómoda. Es por eso que la *shell* aporta
funciones, tal y como otros lenguajes lo hacen, para facilitar la reutilización
del código.

Sí que es cierto que la *shell* es un tanto peculiar en este aspecto, ya que
sus funciones se comportan como un comando cualquiera: exponen un valor
mediante un código de salida, y la forma habitual de que expongan una salida es
mediante la exposición de texto a su salida estándar. Pero antes de estudiar
estas peculiaridades, es mejor ver la sintaxis:

```
nombre_de_función()<comando compuesto>
```

Aunque la siguiente sintaxis también es válida:

```
function nombre_de_función<comando compuesto>
```

En ambos casos el campo indicado mediante `nombre_de_función` se trata del
nombre que se desea añadir y el `<comando compuesto>` es un conjunto de
comandos con una sintaxis particular que se describe a continuación.

#### Comando compuesto

El comando compuesto que puede usarse tiene diferentes formas, la primera puede
ser simplemente los paréntesis, «`()`», que se han usado previamente, que como ya
se ha explicado se ejecutan en una *subshell*.

Para el caso de las funciones, es más interesante usar otro tipo de comando
compuesto, delimitado por las llaves «`{}`», que no se ejecuta en una
*subshell*, lo que permite alterar las variables del *script*.

La *shell* detecta un comando compuesto con la aparición de la llave de
apertura «`{`», después lee el contenido hasta encontrarse con una llave de
cierre «`}`». Para que el comando compuesto sea procesado correctamente, la
llave de cierre debe estar separada del último comando de la lista interior
mediante un delimitador de comando, el salto de línea o el punto y coma, del
siguiente modo:

```
{ comando ; }
{ comando <salto de línea> }
```


El comando compuesto es un mecanismo independiente de las funciones, que
también es interesante para los casos en los que se quieren redireccionar las
salidas de varios comandos en conjunto, como puede verse en el siguiente
ejemplo:

``` bash
$ { ls ; echo "TERMINADO" ; } > salida
```

#### Uso de funciones

El uso de las funciones es un tanto particular, ya que como se ha dicho se
comportan como comandos cualquiera. Por ejemplo:

``` bash
saludo(){
    printf "El sistema de procesamiento avanzado te da la bienvenida, \
introduce tu nombre para continuar\n> "
    read NAME
    printf "Me alegro de conocerte, $NAME\n"
}

saludo
```

Como puede apreciarse en el ejemplo, no hay modo de diferenciar un comando
cualquiera de una función como `saludo` en el momento de la ejecución.

##### Argumentos de entrada

Del mismo modo, a una función se le pueden enviar cualquier cantidad de
argumentos, como si de cualquier otro comando se tratara.

Los argumentos de entrada se reciben como si la función se tratara de un
*script* completo, en los parámetros especiales numerados y en `$*` y `$@` como
ya se explicó previamente:

``` bash
saludo(){
    printf "El sistema de procesamiento avanzado te da la bienvenida, $1"
}

saludo Ekaitz
```

Si se desea iterar por los argumentos de entrada tanto de una función como de
un *script* puede ser interesante el comando interno `shift` que rota los
argumentos de la posición N a la posición N-1, haciendo que el primero de ellos
se descarte.

###### Retorno de valores

El retorno de valores es un tema complicado, ya que no hay mecanismo de retorno
como en otros lenguajes. Como ya se ha dicho, la *shell* trata las funciones
como *scripts* y éstos no tienen modo de devolver valores, más allá del simple
código de salida que pueden lanzar mediante `exit`.

En las funciones el método del **código de salida** también está disponible,
pero para ello necesita usar un comando diferente, ya que `exit` termina la
ejecución del programa actual, y se necesita sólo terminar con la función. Para
esto existe el comando `return`. Que es prácticamente idéntico a `exit`, pero
con esa particularidad. Ambos retornarán un valor numérico entre 0 y 255 que se
almacena en el parámetro especial `$?` justo después de terminar la ejecución
del comando.

Esto es útil para comprobaciones de verdad y para comprobar si ha habido un
error en el programa. Pero más allá de eso tiene una utilidad muy reducida.
Para poder retornar valores arbitrarios es necesario usar algún mecanismo
adicional.

El más obvio es capturar la salida de las funciones mediante una **sustitución
de comando**, ya que se comportan como cualquier comando. Éste será en general
el mecanismo que más se use, aunque pueda ser incómodo con alguna tarea.

``` bash
GREETING=$( saludo Ekaitz )
```

La última opción es **exponer variables** al exterior y avisar a quien use la
función que la salida de ésta debe obtenerse de esa variable. Es peligroso este
mecanismo ya que si no se eligen los nombres cuidadosamente, la variable puede
estar asignada previamente por otras partes del programa. Esta es, a pesar de
este problema, la única solución en las ocasiones que la captura de la salida
no es suficiente.


#### Variables y ámbito

Las variables de la *shell* son globales, lo que implica que las funciones
tienen acceso a su contenido y que las variables definidas de la forma habitual
en las funciones también pueden verse desde el exterior (a no ser que el cuerpo
de la función esté delimitado por paréntesis, lo que lo ejecutaría en una
*subshell*).

Para reducir el riesgo de colisiones, la *shell* introdujo el comando `local`
que crea variables locales que sólo pueden ser accedidas desde la función
actual.

En general, todas las variables generadas en una función, a no ser que quieran
exponerse, deberán declararse como locales usando `local`.

#### Funciones y aliases

Visto que las funciones se comportan como un comando es fácil pensar que pueden
sustituir perfectamente a un `alias` y así es. Las funciones son de hecho
sugeridas por muchos manuales como una alternativa a los alias ya que no hay
nada que un alias pueda hacer que una función no sea capaz. En realidad son más
poderosas que un alias en todos los aspectos y cuando un alias se queda corto,
usar una función es una de las soluciones más evidentes (otra solución sería
crear un programa completo).

### Compatibilidades

Cuando se escriben herramientas en *shell* hay que ser consciente que cada
*shell* es diferente. Muchas *shells* tipo Unix añaden funcionalidades
adicionales sobre el estándar POSIX e incluso algunas no lo siguen. En este
documento se ha tratado de contar únicamente las partes compatibles con el
estándar o al menos con las *shells* más conocidas, pero no siempre es fácil
saber los detalles concretos del estándar o de todas las *shells* para las que
se desea que el programa sea compatible.

Si se desean escribir programas portables hay varias opciones que se deben
barajar. La primera es limitar la cantidad de *shells* para las que el programa
es válido, por ejemplo añadiendo una *shell* concreta en la *shebang* del
programa. La segunda opción es abrazar el estándar POSIX y advertir a quien use
el programa que éste sólo funcionará en las *shells* que lo cumplan. Otra
opción, pero quizás la más complicada, es usar sólo la parte del estándar POSIX
que es común a la mayor cantidad de *shells*.

Existen herramientas como [shellcheck][shellcheck] que pueden facilitar la
tarea, sobre todo en el segundo y el tercer caso. Su funcionalidad es sencilla,
son herramientas de análisis estático del código que comprueban la
compatibilidad de éste con el estándar o con las *shells* que conocen. De este
modo los programas pueden comprobarse antes de enviarlos a producción.

[shellcheck]: https://www.shellcheck.net/
