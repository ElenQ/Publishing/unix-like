# Programar en Unix

Desde la perspectiva de quien desarrolla aplicaciones pueden afianzarse muchos
de los conocimientos introducidos en apartados previos. En este apartado se
trabaja en esa dirección, discutiendo las diferentes interfaces del sistema y
cómo trabajar con él, para así complementar el enfoque previo.

Muchas herramientas introducidas hasta el momento son meras versiones
ejecutables de utilidades que el sistema aporta a modo de biblioteca o
accesibles a través de una llamada al sistema. Tanto es así que el propio
manual diferencia comandos en la sección `1` de las llamadas a sistema `2`
debido a que muchos términos de búsqueda se llaman de la misma manera a pesar
de representar cosas diferentes, esto es: la llamada al sistema y el programa
que a través de la llamada al sistema realiza la misma acción. Un caso de los
muchos que hay es `chmod`, nombre expuesto como llamada al sistema y,
documentado en `man 2 chmod`, y como ejecutable, documentado en `man 1 chmod`.

## El lenguaje C

El lenguaje C fue inventado en el mismo lugar y por algunos de los inventores
de Unix, es por eso que tienen una relación especial. La documentación del
sistema que puede accederse mediante las páginas `man` se muestran en el
lenguaje C, ya que es el lenguaje en el que Unix se desarrolla desde una de las
primeras versiones, porque tanto los nuevos sistemas tipo Unix como Linux o
Minix también están desarrollados en este lenguaje y, sobre todo, porque el
estándar POSIX se pensó con C en mente e incluye una biblioteca estándar en C
(`unistd.h`).

Esto no significa que la única forma de interactuar con un sistema Unix sea
usando el lenguaje C, pero la interacción con el sistema suele realizarse
mediante C o algún mecanismo de interacción con este lenguaje como una FFI
(*foreign function interface*) independientemente del lenguaje en el que uno se
encuentre.

Esto se debe en parte a que C es un lenguaje con una gran capacidad para
representar acciones de bajo nivel, que es justo el nivel en el que las
operaciones que se exponen en este apartado se encuentran. Cuando se trabaja
desde otros lenguajes, como la propia *shell*, normalmente estas operaciones de
bajo nivel se abstraen en interfaces más amigables, pero siempre es interesante
conocer lo que ocurre debajo de las capas de abstracción que los entornos de
trabajo aportan.

Este apartado no pretende profundizar en el desarrollo en este lenguaje, sino
que pretende servir de introducción a cómo los programas interactúan con el
sistema operativo en los sistemas similares a Unix. No se tratarán ejemplos de
código que requieran un conocimiento ni siquiera superficial del lenguaje con
el objetivo de no tener que afrontar los detalles que hacen de C un lenguaje
tan potente y difícil de comprender, así que quien no conozca el lenguaje puede
perfectamente seguir este apartado.

> NOTA: quien desee ejecutar los ejemplos de código mostrados durante el
> apartado y realizar pruebas, puede probar a utilizar el compilador de C del
> proyecto GNU, conocido como GCC. `man gcc` es una página larguísima que puede
> resultar abrumadora. Para los ejemplos mostrados por este documento basta con
> indicar un archivo de entrada e indicar qué archivo de salida se desea con la
> opción `-o`, por ejemplo: «`gcc tuberias.c -o tuberias`» compila el archivo
> de C `tuberias.c` al programa `tuberias` que ya puede ser ejecutado como
> cualquier otro.

## Visión general de un programa

Cuando se ejecuta un programa en Unix se crea un proceso para él. Un proceso es
un concepto técnico para referirse a un programa en ejecución. Como bien se ha
explicado previamente, cada proceso tiene asignado su identificador llamado
PID que sirve para hacer referencia a este programa en ejecución, tanto desde
otros programas como internamente en el sistema.

El núcleo dispone de varios componentes que se encargan de orquestar la
ejecución de los procesos. Su tarea se resume en dos conceptos: planificación
(*scheduling*) y lanzamiento (*dispatching*). En Unix, un sistema multitarea,
los procesos se ejecutan a la vez, compartiendo tiempo de ejecución. El sistema
de planificación decide cuánto tiempo debe ejecutarse cada proceso antes de
pausarlo y ejecutar otro, con el fin de conseguir que todas las tareas avanzan
sin estancarse. El sistema de lanzamiento es, por otro lado, el encargado de
pausar y reanudar los procesos en el momento que el planificador decida,
guardando su estado al pausarlos y recuperándolo cuando los reanude.

Los procesos se ejecutan de forma aislada. Cada uno de ellos dispone de su
memoria, y no tiene acceso a la de los demás. La memoria de cada proceso se
distribuye en diferentes secciones: el código del programa en ejecución y los
datos estáticos ocupan el principio de la memoria y al final de ella se
encuentra la pila (*stack*). El espacio vacío que queda entre los bloques
ubicados al principio y la pila se aprovecha cuando se necesita cualquiera de
los dos lados crezca, ya sea porque la pila necesita crecer o porque el
programa necesita memoria dinámica.

La memoria dinámica se obtiene aumentando el tamaño asignado para el programa y
los datos. Para esto, el programa debe avisar al núcleo de que necesita más
espacio de almacenamiento. Si el núcleo lo acepta, se reorganiza el espacio en
la memoria, aumentando el espacio disponible para el programa. A este mecanismo
se le conoce como alquiler de memoria (*memory allocation*).

La gestión de memoria es extremadamente importante para conocer el
funcionamiento de los sistemas operativos y aporta muchos puntos interesantes a
analizar.

El primero de estos puntos es el por qué de esta distribución. Realmente la
máquina dispone de una memoria limitada, con un sistema de direccionamiento
único. Todos los programas en ejecución en la máquina realmente usan esta
memoria física, pero ellos nos son conscientes de ello ya que, mediante la
cooperación de un componente hardware conocido como MMU (*memory management
unit*) y el núcleo, se les hace creer a los programas que ellos son el único
programa en ejecución en el dispositivo y que toda la memoria, incluso más que
la memoria física de la máquina, está disponible para ellos. Esto se realiza
mediante traducciones de direcciones virtuales a físicas y varios mecanismos
más que no es interesante estudiar en este punto. Lo que es necesario recordar
es que los procesos se sienten únicos en la máquina.

Esto explica por qué deben pedir acceso a nuevos bloques de memoria en lugar de
usarla directamente. Al tratar de usar bloques de los que no se ha avisado al
núcleo, el sistema de traducción de memoria no está preparado y el núcleo debe
capturar esto como un fallo de uso de memoria.

El segundo punto interesante está relacionado con la interacción entre
programas. Si los programas están aislados, no existe modo de que puedan
colaborar entre ellos. Por suerte los sistemas tipo Unix aportan un conjunto de
herramientas, algunas ya visitadas, para la comunicación entre procesos
(*IPC*). La más obvia, es el sistema de archivos, aunque hay otras como la
memoria compartida (*shared memory*), que se trata de un tipo de alquiler de
memoria especial que permite que varios procesos tengan acceso a la misma
memoria física.

Evidentemente, estos mecanismos de *IPC* requieren de algún tipo de control ya
que si dos procesos de forma paralela escriben en una sección de memoria o
disco duro, los datos pueden corromperse.

Esta visión general puede ser un tanto técnica pero es primordial para tratar
las pocas funcionalidades del sistema que se desean tratar en este apartado.
Conocerlas puede marcar un antes y un después en el uso de Unix ya que, a pesar
de ser conceptos de programación avanzados, afectan en gran medida a las
decisiones de diseño de Unix y a cómo las aplicaciones trabajan tanto por
separado como en colaboración.

## Interactuar con el entorno

Como ya se ha mostrado en el capítulo previo sobre la *shell*, los programas
tienen la necesidad de interactuar con el entorno en el que trabajan. Fuera del
contexto de programación en *shell* el caso es exactamente el mismo y conceptos
similares aplican, pero a un nivel más bajo.

### Argumentos de entrada

Cualquier programa en Unix puede recibir argumentos de entrada. La pequeña
diferencia es que la *shell* preprocesa los argumentos y los asigna en unos
parámetros particulares. En un programa cualquiera Unix asignará los argumentos
de entrada en las primeras posiciones de la pila (*stack*), siendo el primer
valor la cantidad de argumentos recibidos y los siguientes los propios
argumentos, en orden de inclusión. El valor de los argumentos se indica en
texto, ya que es como se introducen, si se desean traducir a valores numéricos
es el programa el encargado de hacerlo.

Al igual que con la *shell* los diferentes lenguajes de programación
preprocesan estos argumentos y los entregan de formas más accesibles.

En el caso del lenguaje C se reciben del siguiente modo en la función
principal:

``` c
int main (int argc, char * argv[]){
    //    ^^^^^^^^                  Número de argumentos
    //              ^^^^^^^^^^^^^   Valor de los argumentos
}
```

#### Procesado de argumentos de entrada

Como ya se ha visto anteriormente, la gestión de los argumentos de entrada y su
procesado está completamente en manos del programa. Es por eso que la
biblioteca estándar de C de POSIX, `unistd.h`, incluye un módulo concreto para
esta tarea.  Ver `man 3 getopt`.

Implementaciones como GNU disponen de extensiones adicionales al funcionamiento
de `getopt` en su propia sección de la biblioteca estándar de C pero no son
compatibles con POSIX.

### Código de salida

Del lado opuesto a la captura de argumentos es el código de salida, el
mecanismo ya estudiado para retornar valores. Realmente este proceso se realiza
mediante una llamada al sistema, un mecanismo que se presenta más adelante en
este apartado, haciendo uso de la llamada `exit` (ver `man 2 exit`).

Sin embargo, igual que en el caso de los argumentos de entrada, el lenguaje C
está preparado para entregar una interfaz más cómoda mediante el valor de
retorno de la función `main`:

``` c
int main (int argc, char * argv[]){
    return 0; // Activa el código de salida al valor 0
}
```

De todos modos, también puede aplicarse la función `exit` u otras similares
para otros casos del programa más particulares.

Los códigos de salida son muy importantes la que la *shell* y el propio sistema
dan por hecho de que los códigos de salida siguen la interfaz habitual: `0`
para casos de ejecución correcta y cualquier otro valor para el caso en el que
haya habido un error. Esto es importante porque si deseamos utilizar el
programa desde la *shell*, el valor `$?` estará asignado al código de salida
del programa y las operaciones como `&&` y `||` operarán sobre ese valor.

Por otro lado, el valor de retorno debe estar entre `0` y `255` a pesar de
estar normalmente indicado por un valor `int` en C.

Sólo deberían usarse valores entre `127` y `0` porque sólo preservan los bits
menores del valor. Los valores más altos tienen un significado especial, si un
programa termina debido a una señal, el código de salida representa el valor
máximo, `128`, más el número de la señal que terminó el proceso.

``` bash
$ sleep 1000
^C
$ echo $?  # Se ha terminado mediante una señal al enviar Ctrl-C en la shell
143
```

### Variables de entorno

Los programas también tienen acceso a variables de entorno, igual que la propia
*shell*. Sin embargo, en este nivel, mucho más bajo, el acceso es un tanto más
arcaico. Desde C, se exponen estas variables de entorno en la variable externa
`environ`:

``` bash
extern char ** environ;
```

El problema es la capacidad de acceder a estos valores, debido a que están en
una estructura un tanto incómoda. Para evitar acceder de forma cruda, POSIX
aporta funciones adicionales como `getenv`, `setenv` y `unsetenv`. Sus
respectivas páginas del manual explican cómo usarlas en detalle.

Más allá de cómo deben usarse estas variables desde los programas de los que
cada persona desarrolle, este mecanismo se explota, como ya se ha visto en
otros casos, por todo el sistema. La documentación en `man environ` muestra un
pequeño resumen de cómo los programas básicos de Unix lo usan.

### Entrada y salida estándar

Los programas en Unix comienzan con los tres descriptores de archivo, entrada
estándar (`stdin`), salida estándar (`stdout`) y salida de errores (`stderr`)
abiertos. Como ya se conoce de capítulos previos, estos descriptores
interactúan con la terminal, a no ser que el proceso padre haya realizado
alguna redirección con ellos.

El núcleo gestiona estos descriptores de archivo de forma cruda, es decir
utilizando únicamente un número para referirse a ellos. La cabecera de
compatibilidad con POSIX incluye estos descriptores de archivo en las variables
`STDIN_FILENO`, `STDOUT_FILENO`, y `STDERR_FILENO` de modo que puedan usarse de
forma directa con llamadas al sistema de bajo nivel.

Para facilitar la tarea, la biblioteca estándar del lenguaje C, también parte
del estándar POSIX, aporta una capa adicional de simplificación, permitiendo
acceder a los descriptores de archivo mediante una interfaz más sencilla:

``` c
#include <stdio.h>

extern FILE *stdin;
extern FILE *stdout;
extern FILE *stderr;
```

Estos identificadores hacen referencia a los descriptores de archivo recién
descritos pero añaden una pequeña capa de abstracción para facilitar, por un
lado, la portabilidad a otros sistemas y, por otro, para aportar una
funcionalidad más avanzada, con *buffering*, y códigos de error más manejables,
entre otras cosas.

Más adelante se analizan las llamadas al sistema y su interfaz, de momento es
interesante conocer que la biblioteca estándar de C llamará internamente al
sistema, pero también es posible realizar las llamadas al sistema de forma
manual, aunque menos práctico. Para observar las diferencias es interesante
comparar `man 2 open`, la llamada al sistema para abrir archivos, con `man 3
fopen`, la implementación de la biblioteca estándar de C para abrir archivos.

### Archivos temporales

Los archivos temporales son también parte importante del entorno, ya que
permiten muchas acciones interesantes. Por ejemplo, interactuar con otros
programas de forma muy sencilla.

Como ya se ha estudiado previamente, el directorio `/tmp` almacena los archivos
temporales, así que podría entenderse que usar un archivo temporal es tan
sencillo como añadir un nuevo archivo en ese directorio.

La realidad es que es así de sencillo, pero hasta cierto punto. Cada aplicación
que se ejecuta en un sistema multitarea debe diseñarse con la idea de que
muchas instancias de ella pueden estar ejecutándose al mismo tiempo. Si se crea
un archivo temporal con un nombre fijo, las múltiples instancias de la
aplicación lo compartirán, o incluso puede que colisione con otras aplicaciones
que decidieron llamar del mismo modo a sus archivos temporales.

POSIX aporta un grupo de funciones para crear archivos temporales que no
colisionarán: `mktemp`, `tmpfile`, etc. Siempre que se necesite el acceso a un
archivo temporal es interesante usarlas.

Por otro lado, estas funcionalidades también están disponibles para programas
escritos en *shell* mediante el programa `mktemp`.

## Modos de ejecución

Los sistemas operativos, tal y como se describieron en capítulos previos, son
un programa que se ejecuta en una máquina y se encargan de gestionar sus
recursos. En el caso de Unix, como en otros sistemas operativos complejos, el
núcleo es un apartado privilegiado, el único capaz de gestionar la máquina.

En Unix existe una separación de dos modos de ejecución. Por un lado, el núcleo
se ejecuta con total acceso sobre la máquina en lo que se conoce como *Kernel
Mode* (modo núcleo). Por otro, el *User Mode* (modo usuario) es el modo en el
que el resto de programas se ejecutan.

En el *Kernel Mode* el código en ejecución, el núcleo, tiene acceso total a la
máquina: puede utilizar cualquier dirección de memoria válida, tiene acceso
total al disco duro, puede ejecutar cualquier instrucción, etc. Un fallo en
este modo puede ser catastrófico ya que no existe ningún mecanismo para lidiar
con él.

El *User Mode*, donde se ejecutan la mayoría de los programas, es todo lo
contrario. En este modo, los programas no tienen modo de acceder a la memoria
real, tal y como se ha descrito al comienzo de este apartado. Tampoco pueden
ejecutar cualquier instrucción, algunas quedan reservadas para el modo núcleo.
Como los programas en este modo no tienen acceso a la máquina, deben realizar
peticiones al *Kernel Mode* para conseguirlo.

Hay varios modos de que el modo de ejecución pase del modo usuario al modo
núcleo:

- Interrupciones de software: casos de fallos de ejecución como divisiones
  entre cero, acceso de páginas de memoria incorrectas, etc. Estos errores
  cuando ocurren en código en *User Mode* hacen que el núcleo tome el control
  para gestionar la interrupción.

- Interrupciones de hardware: requieren la atención del procesador llamando
  directamente a funciones del núcleo. Las interrupciones de teclado o acceso a
  disco son ejemplos excelentes de esto: cuando ocurren, el núcleo debe
  procesarlas al momento por lo que se salta al *Kernel Mode*.

- Señales: se gestionan por el núcleo, donde se decide qué hacer con ellas. Ya
  sea llamar a su gestor correspondiente del programa al que hacen referencia o
  terminar el programa.

- Llamadas al sistema: son el mecanismo por el que los programas piden recursos
  al sistema. Al hacerlo, el núcleo es el encargado de responder a la petición,
  ya sea tras capturar el recurso para el programa o tras denegarlo. En
  cualquier caso, un programa cualquiera en el modo usuario pasa el control al
  modo núcleo al realizar una llamada al sistema.

- Instrucciones especiales: algunas instrucciones adicionales pueden ceder el
  control al núcleo. Se trata del mismo caso que las llamadas a sistema, ya que
  estas se realizan mediante una instrucción especial, pero también incluye
  otros casos como los *breakpoints*.

Estas transiciones entre uno y otro modo son conceptos avanzados de diseño e
implementación de sistemas operativos que se escapan a los objetivos de esta
explicación, pero es fundamental comprender que existe este intercambio de
control para entender qué tipo de acciones realizan los programas en una
computadora y las capacidades que tienen.

En resumen, y desde la perspectiva del desarrollo de aplicaciones, los
programas se ejecutan en un contexto aislado que no tiene capacidad para
realizar casi ninguna acción que pueda afectar al exterior. En la mayoría de
los casos, si el programa necesita de cualquier recurso del sistema debe
pedírselo al núcleo mediante una llamada al sistema y es éste el que tiene la
capacidad de hacer los cambios necesarios.

Por otro lado, la diferencia entre los dos modos de ejecución ya se ha visitado
de forma lateral en el apartado sobre el sistema de arranque. En este apartado
se explicó que el núcleo era el primero en arrancar y que después era el
programa *init* el encargado de ejecutarse y preparar el entorno de usuario.
Este proceso está muy ligado a lo que se acaba de mostrar aquí, pero ocurre
desde la perspectiva opuesta, siendo el modo núcleo el que despierta el modo
usuario. Tal y como se mencionó, una vez el modo usuario está preparado el
núcleo pasa a un segundo plano y espera a las peticiones o interrupciones que
puedan requerir de su atención, llegando, justamente, a la situación que recién
se acaba de explicar.


## Llamadas al sistema

Tal y como se acaba de proponer, las llamadas al sistema son el modo en el que
los programas acceden a los recursos. Éstas se exponen a través de
instrucciones especiales del procesador que provocan que el núcleo tome el
control. Las llamadas al sistema conforman una ABI (*application binary
interface*) donde cada llamada al sistema tiene asignado un número, al activar
la instrucción especial el núcleo comprueba el número de la llamada al sistema
y sus argumentos y la ejecuta. Desde el lenguaje C se expone esta ABI como un
conjunto de funciones predefinidas en una API mucho más sencilla, que es la que
se tratará aquí, pero es importante recordar que siempre se podrá realizar una
llamada al sistema de forma directa a nivel de máquina.

> NOTA: Las llamadas al sistema se recopilan en su propia sección del manual,
> bajo el número `2`.

### Nota práctica: llamadas al sistema en ensamblador RISC-V y C

Antes de saltar a ver ejemplos y a usar las llamadas al sistema de forma más
natural, es interesante entender lo que se acaba de exponer. En la arquitectura
RISC-V, la instrucción `ecall` es la encargada de lanzar una llamada al sistema
y los registros `a0`, `a1`, `a2`, etc. son los encargados de gestionar los
argumentos de de la llamada, siendo `a7` el número de la llamada que se desea
realizar.

El siguiente ejemplo realiza una llamada al sistema a `read`, cuyo número es el
`63`, para leer un byte desde el descriptor de archivo `0`, `stdin`, a la
posición de memoria indicada por la etiqueta `buffer`.

``` asm
# Leer un byte desde stdin
li a7, 63             # sys_read
li a0, 0              # File descriptor (stdin)
la a1, buffer         # Buffer
li a2, 1              # Tamaño que se desea leer
ecall
```

Cada arquitectura tiene una forma de hacerlo. En `man syscall`, una función de
llamada al sistema indirecta, se muestra cómo funcionan las llamadas al sistema
en cada arquitectura de forma muy general.

El mismo ejemplo usando C se simplifica bastante:

``` c
#include <unistd.h>     // Incluye la cabecera de POSIX

read(0, buffer, 1);
```

En ninguno de los dos ejemplos se ha tratado el valor de retorno, que es muy
importante. En ensamblador, el valor de retorno se recoge en los registros
habilitados para esta tarea, en este caso `a0`. En C se recupera en el valor de
retorno de la función. Este valor de retorno es importante ya que indicará cómo
ha funcionado la llamada y si su efecto es el deseado. En el caso de `read` el
valor de retorno es el número de bytes del archivo que se han podido leer.

### Nota práctica: visualización de llamadas al sistema

Para comprender la importancia de las llamadas al sistema es interesante
analizar su uso en programas que ya se conocen. El programa `strace` visualiza
las llamadas al sistema realizadas por un programa que se le envíe como
argumento.

``` bash
$ strace ls
```

Ejecutar `strace` muestra un resultado sorprendentemente largo donde todas las
visualizaciones a pantalla, las lecturas de archivos, etc. se muestran al
detalle. Esto es lógico porque, como ya se ha mencionado, los programas no
tienen acceso directo al sistema y todo debe pasar por el núcleo. Por ejemplo,
una simple visualización en la pantalla de la terminal es en realidad una
escritura en el archivo `stdout`, lo que implica la llamada al sistema `write`.

Independientemente del lenguaje de programación en el que se trabaje, al final
todas las operaciones que requieran acceso a recursos acaban realizando
llamadas al sistema, es por eso que es interesante conocerlas.

### Errores en las llamadas a sistema

Las llamadas a sistema pueden fallar de infinidad de maneras y es
responsabilidad del programa gestionar esos posibles errores. Los fallos pueden
ocurrir por falta de permisos, como al intentar escribir un archivo de sólo
lectura; por falta de recursos, como que no haya más espacio disponible en
memoria; por un error en los argumentos de entrada, como que varios argumentos
no sean compatibles; etc.

El primer punto a controlar al realizar una llamada al sistema es si su valor
de retorno es el correcto. En el caso de `read` (ver `man 2 read`), el valor de
retorno es el número de bytes leídos pero retorna el valor `-1` en caso de que
haya ocurrido un error durante la lectura.

Sin embargo, un simple valor negativo como `-1` no es suficiente para saber qué
ha podido ir mal, por lo que el sistema aporta una interfaz de errores
adicional conocida como `errno`.

La mayor parte de llamadas al sistema, e incluso muchas llamadas a bibliotecas,
activarán `errno` en casos de errores, asignándole un valor que describa la
razón del error. La cabecera `errno.h` aporta una interfaz de gestión de la
variable `errno` y la página del manual (`man 3 errno`) aporta una detallada
explicación del funcionamiento de este mecanismo de errores.

### Nota práctica: llamadas al sistema y biblioteca estándar

La biblioteca estándar del lenguaje C está diseñada para ser más amigable que
las llamadas al sistema directas pero también está diseñada pensando en una
portabilidad entre sistemas operativos. El mismo código C, si no hace uso de
características concretas de los entornos como son las llamadas al sistema,
debería ser capaz de compilar y funcionar correctamente en diferentes sistemas
operativos, incluso en aquellos que no cumplan el estándar POSIX, sin requerir
ningún cambio.

En general, no existe ninguna razón para utilizar llamadas al sistema de forma
directa puesto que no son portables ni aportan una interfaz especialmente
cómoda. La biblioteca estándar del lenguaje de programación con el que se
trabaje será quien se encargue de ejecutar las llamadas al sistema
necesarias internamente, permitiendo así una portabilidad sencilla a otros
sistemas operativos.

En algunos casos muy particulares sí que resulta interesante trabajar
directamente en llamadas al sistema (por ejemplo, cuando se desarrolla una
biblioteca estándar para un lenguaje) pero siempre debe tenerse en cuenta que
estas llamadas al sistema no son portables. Si se desea dar soporte a otras
plataformas, deberá incluirse un mecanismo que seleccione las llamadas al
sistema de la plataforma objetivo en cada caso.

Por todo esto, en lugar de tratar en detalle todas las llamadas al sistema
disponibles, que de igual modo pueden listarse en `man 2 syscalls`, este
capítulo trata de mostrar las capacidades generales del sistema, puesto que son
estas últimas las que realmente delimitan lo que se puede o no se puede hacer
con éste y tienen bastante impacto en el entendimiento general del mismo a
nivel de administración y uso diario.


## Bibliotecas

Las bibliotecas son un concepto muy importante en Unix ya que permiten
reutilizar código con sencillez. Los sistemas Unix soportan dos tipos de
bibliotecas: las estáticas y las dinámicas.

Las **bibliotecas estáticas** (*static library*), también conocidas como
*archives*, son archivos compilados que almacenan un conjunto de
funcionalidades de modo que después pueden incluirse en nuevos programas a la
hora de generarlos. Suelen nombrarse con la extensión `.a`, de *archive*.

Las **bibliotecas dinámicas** (*dynamic library* o *shared object*) son un tipo
de biblioteca especial que los programas incluyen en el momento de ejecución.
Estas bibliotecas se cargan mediante un mecanismo del sistema operativo
conocido como *enlazador dinámico* (*dynamic linker*). Estas bibliotecas se
cargan en vivo, mediante un alquiler de memoria, extendiendo el espacio del
programa, en el momento en el que se necesiten. Estas bibliotecas suelen
nombrarse con la extensión `.so`, de *shared object*.

Desde la perspectiva de quien desarrolla programas para Unix ambos tipos de
bibliotecas son fundamentales, pero para quien desea mantener sistemas el
segundo tipo es mucho más importante. Las bibliotecas estáticas se incluyen en
el momento de creación del programa por lo que los programas ya llegan con
ellas incluidas y no suponen ningún problema. Las bibliotecas dinámicas, sin
embargo, deben encontrarse en momento de ejecución y para ello el sistema debe
estar bien configurado y las bibliotecas deben estar instaladas en el sistema,
normalmente en `/lib` y `/usr/lib`, pero en función de la configuración del
sistema podrán incluirse en otros directorios.

En general, el gestor de paquetes se encarga de descargar las bibliotecas
necesarias pero no siempre es así. Algunos gestores de paquetes optan por una
estrategia conservadora y sólo instalan las bibliotecas mínimas para el
funcionamiento del programa, dejando algunas funcionalidades completamente
inhabilitadas. Quien gestione el sistema debe ser consciente de esto y añadir
las bibliotecas adicionales necesarias para habilitar las funcionales que
necesite.

La documentación en `man 8 ld.so` describe en detalle cómo funciona el
enlazador dinámico incluyendo muchos puntos importantes como la búsqueda de las
bibliotecas en el sistema, asociado a variables de entorno como
`LD_LIBRARY_PATH`, entre otras.

### Nota práctica: visualización de bibliotecas dinámicas

El programa `ldd` es capaz de visualizar las bibliotecas dinámicas de las que
depende un programa ejecutable:

``` bash
$ ldd /bin/ls
   linux-vdso.so.1 (0x00007ffcc3563000)
   libselinux.so.1 => /lib64/libselinux.so.1 (0x00007f87e5459000)
   libcap.so.2 => /lib64/libcap.so.2 (0x00007f87e5254000)
   libc.so.6 => /lib64/libc.so.6 (0x00007f87e4e92000)
   libpcre.so.1 => /lib64/libpcre.so.1 (0x00007f87e4c22000)
   libdl.so.2 => /lib64/libdl.so.2 (0x00007f87e4a1e000)
   /lib64/ld-linux-x86-64.so.2 (0x00005574bf12e000)
   libattr.so.1 => /lib64/libattr.so.1 (0x00007f87e4817000)
   libpthread.so.0 => /lib64/libpthread.so.0 (0x00007f87e45fa000)
```

Cuando se desarrollan aplicaciones o se preparan en paquetes, `ldd` puede
servir como herramienta para descubrir posibles errores.

### Carga manual de bibliotecas dinámicas

Además del sistema de carga habitual, que carga las bibliotecas según se van
necesitando, el enlazador dinámico se muestra también como una biblioteca de
modo que puede utilizarse para cargar bibliotecas dinámicas desde los
programas, de forma manual.

Mediante este sistema, los programas pueden cargar módulos externos sin
necesidad de enlazarlos originalmente en el programa, haciéndolos completamente
opcionales. Para esto el estándar POSIX aporta funciones de la familia de
`dlopen`, que usan el enlazador dinámico de forma manual. Para más información,
ver `man dlopen`.


## Procesos

Como ya se ha hablado de procesos durante muchos apartados del documento ya se
conocen muchos conceptos relacionados con ellos. La interfaz de llamadas al
sistema como `getpid` son capaces de obtener el PID del proceso en marcha, o
conceptos similares, pero este apartado se centrará en la creación de nuevos
procesos y su control.

La forma más fácil, pero al mismo tiempo la más insegura, de crear un nuevo
proceso es la llamada al sistema `system`. Ésta se encarga de hacer todo lo
necesario para ejecutar un comando de la *shell*. Sin embargo, esto no es lo
que se suele necesitar a la hora de lanzar un nuevo proceso, sobre todo cuando
se desea que el nuevo proceso interactúe con el proceso actual.

Para estos casos es necesario reducir el nivel de abstracción y entender cómo
funciona el mecanismo interno de creación de nuevos procesos.

### Creación de nuevos procesos

La llamada al sistema `fork` es el mecanismo principal de creación de nuevos
procesos. La propia llamada `system` recién descrita usa este mecanismo de
forma interna.

`fork` crea un nuevo proceso copiando el proceso actual al detalle. La única
diferencia entre el nuevo proceso y quien llamó a `fork` es el identificador,
PID, y su posición en la jerarquía de procesos: el proceso que realiza la
llamada es el proceso padre (*parent process*) y el nuevo proceso es el proceso
hijo (*child process*).

Al realizar la llamada a `fork` ambos procesos continúan su ejecución desde la
posición en la que están, justo tras la llamada a la función y su estado y
código es completamente indistinguible hasta el punto que, si no se realiza
ningún procesamiento adicional, cada proceso no sabe si se trata del padre o
del hijo, ya que son idénticos[^fork-copy].

[^fork-copy]: No es del todo cierto, hay varios puntos concretos que no se
  copian del proceso padre al hijo, para obtener una lista detallada ver `man 2
  fork`.

Para diferenciar un proceso del otro debe leerse el código de retorno de la
llamada al sistema `fork`. La llamada retorna el PID del proceso hijo en el
proceso padre y `0` en el proceso hijo. Si ocurriera un error el proceso hijo
no se genera, se retorna `-1` en el padre y se activa `errno`.

#### Nota práctica: uso de la llamada fork

El siguiente ejemplo muestra cómo usar `fork`. Para ejemplificar que uno y otro
proceso entran en los bloques se muestran las funciones `getpid` y `getppid`,
que obtienen el PID del proceso actual y el PID del proceso padre,
respectivamente. De este modo puede verse como el proceso hijo visualiza el PID
del proceso padre, obtenido desde `getppid`, mientras que el padre obtiene el
PID del proceso hijo desde `fork` y muestra el suyo propio mediante `getpid`.

``` c
#include<unistd.h>
#include<stdio.h>

int main(int argc, char* argv[]){
    pid_t pid = fork();
    if(pid == -1){
        // Ha ocurrido un error
        exit(1);
    }

    if(pid == 0){
        // Este bloque lo ejecuta el proceso hijo
        printf("PID padre: %d\n", getppid());
    } else {
        // Este bloque lo ejecuta el proceso padre
        printf("PID hijo: %d \tPID padre: %d\n", pid, getpid());
    }
}
```

Esta gestión de los identificadores de procesos es importante ya que en muchas
ocasiones el proceso padre debe orquestar varios procesos hijos y/o enviarles
información y señales. Si no se almacenan los identificadores de proceso de
forma correcta, esta comunicación no puede darse.

### Terminación de procesos

Al igual que el control de los identificadores de proceso es importante para
asegurar que la comunicación entre ambos procesos es posible, también es
interesante poder capturar el momento de terminación de un proceso hijo.

Como bien se conoce de capítulos previos, los procesos en Unix dependen de que
su padre los libere una vez han terminado. Mientras que esto no se dé se
considera que los procesos son *zombies* y su identificador no se libera de la
tabla de procesos. Este mecanismo, ya introducido en el capítulo sobre arranque
y servicios, no muestra la razón real de por qué los procesos *zombie* deben
existir.

Cuando un proceso termina, en ocasiones necesita hacerle saber al proceso padre
en qué condiciones terminó. Si el proceso se eliminase completamente al
terminar, no habría modo de que hiciese llegar al padre la información de cómo
terminó.

Un ejemplo sencillo de esto es el código de retorno en la *shell*, indicado por
`$?`, que guarda el valor de salida del último comando. Cuando la *shell*
ejecuta un programa externo, como `ls`, éste se lanza en un proceso
independiente. Si el proceso terminara de forma completamente independiente del
proceso padre, la *shell*, no habría forma de recuperar el código de salida del
programa.

El mecanismo de los procesos *zombie* sirve para superar esta limitación. El
proceso hijo una vez terminado se mantiene en un estado mínimo, que mantiene su
PID y su código de salida para que el proceso padre pueda capturarlo. Una vez
el código de salida se captura, el proceso ha terminado su propósito y puede
eliminarse por completo.

Como recordatorio de este sistema se puede pensar en que los procesos *zombie*,
aunque ya estén muertos aún les queda un propósito por realizar, por lo que se
aferran a la vida hasta cumplirlo. Una vez está hecho, pueden descansar.

Las llamadas al sistema tipo `wait` (esperar en inglés) son las que implementan
esta funcionalidad de recuperación del código de salida de los procesos hijo.
La documentación disponible en `man 2 wait` muestra en detalle su
funcionamiento y varios ejemplos de uso, pero pueden simplemente resumirse en
que el proceso padre que efectúe la llamada `wait` se pausará hasta que uno de
sus hijos termine y capturará su código de salida cuando esto ocurra. Si el
proceso hijo ha terminado antes de que el proceso padre llame a `wait`, el
proceso hijo estará en estado *zombie* hasta que se efectúe la llamada, la cual
retornará automáticamente, sin esperas.

Otras versiones de la llamada `wait` añaden otras funcionalidades adicionales.
Por ejemplo `waitpid` permite esperar por un proceso hijo en particular,
identificado por su PID.


### Nota práctica: Minishell I: fork, exec y wait

Un uso habitual de `fork` es el de llamar a continuación a una llamada al
sistema de la familia `exec`. Las llamadas al sistema de esta familia
sustituyen el código del proceso actual por el programa que se les indique. De
esta manera es posible crear algo que se ha visitado una y otra vez durante
todo el documento: una *shell*.

> NOTA: Se las trata como una familia porque son un conjunto de llamadas al
> sistema con diferentes características. La documentación en `man 2 exec`
> define las diferentes particularidades de cada una en función de las letras
> que conforman sus nombres. Por ejemplo, `execve` incluye la particularidad
> `v`, que indica que sus argumentos se entregan en un array, y la
> particularidad `e`, que indica que puede recibir un entorno (*environment*)
> de ejecución.

Las *shells*, resumiendo mucho, son un proceso principal que captura el texto
enviado por quien la usa y lo procesa lo suficiente como para saber qué
comandos debe ejecutar. Cuando se trata de comandos internos, es la propia
*shell* quien los resuelve con su propia implementación, pero cuando se trata
de comandos externos la *shell* ejecuta una llamada a `fork` y, mediante `exec`
sustituye el código del proceso hijo por el código del programa que se desea
ejecutar. Esta pareja de llamadas al sistema es lo que conforma una *subshell*.
Cuando el proceso hijo termina, el control vuelve a pasar al proceso padre
usando `wait`, que vuelve al bucle de espera, mostrando el *prompt*.

Evidentemente, la cantidad de acciones que la *shell* efectúa sobre el texto,
como las expansiones y la asignación de variables, requieren una implementación
bastante larga y compleja, pero el mecanismo de ejecución de programas es tan
simple como se acaba de proponer.

Como ejercicio para quienes estén más versados en el lenguaje C y deseen
inspeccionar la interfaz con el sistema, se propone la programación de una
pequeña *shell* que pueda recibir comandos y ejecutarlos. Más adelante, se irán
incorporando nuevos conceptos a esta *shell*, a medida que se vayan estudiando.


### Señales

Las señales, como ya se introdujo anteriormente, son un mecanismo de gestión de
procesos que funciona a modo de mensaje. Las señales son asíncronas: cuando un
proceso recibe una señal la gestiona automáticamente, abandonando lo que sea
que esté realizando en ese momento.

Cada señal se identifica con un número, pero normalmente se utilizan nombres
más representativos de su acción. `man 7 signal` describe las señales con
bastante nivel de detalle y muestra sus nombres y su número correspondiente.

Cuando un proceso recibe una señal puede realizar diferentes acciones en
función de la señal de la que se trate. En la mayor parte de los casos el
programa puede definir un comportamiento para la señal que se denomina *signal
handler* (gestor de señal). Cuando se recibe la señal, el *signal handler* se
ejecuta automáticamente, pausando la ejecución del programa, y cuando el
*signal handler* termina, la ejecución del programa continúa donde se había
pausado.

Hasta ahora se han analizado casos en los que las señales se envían desde otros
programas mediante el uso de comandos de la familia `kill`, o el uso de
combinaciones de teclas en la terminal de comandos, pero el propio núcleo
también enviará señales a los programas si se dan ciertas circunstancias. Por
ejemplo, el núcleo enviará `SIGSEGV` si el programa incurre en una violación de
segmento de memoria. Este tipo de señales enviadas por el núcleo suelen tener
un comportamiento por defecto bastante agresivo porque son indicativas de un
fallo, normalmente terminarán la ejecución del programa y crearán una copia de
su memoria en un archivo, lo que se conoce como un *core file*, para que pueda
analizarse en busca de errores.

#### Captura de señales

Las señales enviadas por el núcleo y las enviadas por otros procesos se
gestionan del mismo modo desde la perspectiva del programa receptor. La
cabecera `signal.h` aporta la interfaz necesaria para el control de señales
desde un programa. La llamada al sistema `sigaction` es la mejor manera de
registrar un *signal handler* en un programa ya que la llamada `signal` ha
sufrido variaciones de una implementación de Unix a otra y no es universal.

El siguiente ejemplo mínimo muestra un programa que espera a la señal `SIGUSR1`
y al recibirla termina con un código de salida de `0`:

``` c
#include<signal.h>
#include<unistd.h>
#include<stdlib.h>

void handler (int signal_number)
{
    // Gestionar la señal
    exit(0);
}
int main (int argc, char* argv[])
{
    // Prepara el signal handler
    struct sigaction sa = {};
    sa.sa_handler = &handler;
    sigaction (SIGUSR1, &sa, 0);

    // Espera
    sleep(10000);
}
```

#### Envío de señales

El sistema de envío de señales es tan sencillo como desde la terminal y usa la
misma nomenclatura. La llamada al sistema `kill` (ver `man 2 kill`) enviará la
señal indicada al proceso deseado, identificado por su PID.

#### Reentrancia

Las señales son asíncronas por lo que pueden aparecer en cualquier momento de
la ejecución de un programa, incluso a mitad de gestión de una señal anterior,
es por eso que las funciones gestoras de las señales, como `handler` en el
ejemplo previo, deben desarrollarse cuidadosamente.

Deben evitarse las llamadas de entrada y salida, como escribir o leer de
archivos, las llamadas que capturen recursos y todo tipo de acciones de este
tipo ya que si la señal llega de nuevo cuando la señal previa aún no se ha
resuelto las operaciones pueden colapsar, en el caso de las operaciones de
lectura y escritura corrompiendo los datos y en el caso de las obtenciones de
recursos adquiriéndolos por duplicado, dando lugar a *deadlocks* u otro tipo de
situaciones indeseables.

En informática se conoce un término para que es justo lo que se necesita para
que los *signal handlers* sean seguros: la reentrancia (*reentrancy*). La
reentrancia es la capacidad para una rutina de poder pausar su ejecución para
poder ejecutarla de nuevo. Para que una rutina sea reentrante deben darse
varias características que la propia definición de reentrancia describe. Quien
desee gestionar señales deberá familiarizarse con este concepto e implementarlo
cuidadosamente para que sus programas sean seguros.

### Nota práctica: Limpieza asíncrona de procesos hijo

El sistema de señales también facilita la interacción entre procesos padre e
hijo. Cuando un proceso hijo termina, el núcleo automáticamente envía la señal
`SIGCHLD` al proceso padre, de modo que éste puede capturarla y lanzar el
`wait` correspondiente de forma asíncrona.

Este sistema es muy cómodo y útil ya que no requiere bloquear el proceso
principal en una llamada a `wait`, permitiendo así que el proceso padre siga
ejecutando órdenes.

### Nota práctica: Minishell II: Gestión de procesos

Las *shell*, como se vio en el capítulo previo, son capaces de pausar procesos,
continuarlos, mandarlos al segundo plano, etc. Parte de este control es posible
implementarlo mediante la llamada al sistema `kill`, pero en realidad requiere
de algunos conceptos avanzados que no se han visitado.

En sistemas compatibles con POSIX, los procesos, además de tener sus propios
identificadores, también se identifican por grupos (*process group*) mediante
un identificador de grupo. Estos identificadores de grupo pueden utilizarse
como receptores de señales, haciendo que todos los procesos del grupo reciban
la señal. Esta funcionalidad es interesante para varios puntos, por ejemplo,
con los grupos bien asignados es posible terminar todos los procesos en marcha
mediante `Ctrl+C`. Para aprender más sobre los grupos y cómo usarlos ver `man
setpgid`.

Además de los grupos, existe el concepto de sesión (*session*), que se trata de
un conjunto de grupos. Ver `man setsid`.

A pesar de ser conceptos nuevos no son especialmente complicados si las bases
son sólidas así que se anima a quien tenga interés a intentar aprender más
sobre estas funcionalidades y tratar de mejorar el diseño de la *shell* de la
nota práctica anterior[^job-control-shell].

[^job-control-shell]: El manual de la biblioteca estándar de C del proyecto GNU
  muestra cómo crear una *shell* con capacidad para controlar procesos que
  puede servir de referencia a quien quiera intentarlo:  
  <https://www.gnu.org/software/libc/manual/html_node/Implementing-a-Shell.html>


## Hilos

Los hilos (*thread*) son un sistema similar a los procesos a nivel práctico. Su
objetivo es el de aportar a un programa la posibilidad de realizar varias
tareas a la vez, pero de forma más ligera que los procesos.

A diferencia de los procesos, los hilos no crean una copia completa del
proceso que los genera, sino que comparten recursos con el proceso que los
creó, de modo que sobrecargan mucho menos el sistema. Sin embargo, el hecho de
que compartan los recursos también es preocupante, ya que si alteran la
memoria, el proceso padre sufrirá los cambios, por lo que se requiere un
tratamiento especial de control de acceso a la memoria que en los procesos no
se requiere.

POSIX define una interfaz para la creación y gestión de hilos conocida como
`pthread`, que se encuentra en su propia cabecera, `pthread.h`, y en su propia
biblioteca, `libpthread`, independiente de la biblioteca estándar de C. Su
documentación general puede encontrarse en `man 7 pthreads`, donde se enlaza a
las funciones necesarias para controlar los hilos[^pthread-impl].

[^pthread-impl]: Evidentemente, estos hilos requieren de un soporte interno
  aportado por el núcleo. En Linux, por ejemplo, se crean mediante la llamada
  al sistema `clone`, similar a `fork` pero con capacidad para crear hilos, y
  se gestionan de forma similar a un proceso. Es posible enviar señales a los
  hilos usando `tkill` en lugar de `kill`, pero toda esta interfaz es
  dependiente de la implementación del núcleo y no siempre está disponible en
  la biblioteca estándar de C.

> NOTA: como los hilos POSIX se encuentran en una biblioteca independiente, se
> encuentran en la sección `3` del manual.

El uso de los hilos POSIX es bastante sencillo una vez se conocen otros
mecanismos como los procesos. Cada hilo dispone de un identificador que puede
usarse para referirse a él, como ocurre con los procesos. Una diferencia
interesante es que en los hilos POSIX se envía una función al hilo en el
momento de su creación que será la que el hilo ejecutará. Resumiendo estos
puntos en un ejemplo sencillo que muestra cómo tanto el hilo como el proceso
principal se ejecutan en paralelo, uno mostrando la letra `H` en pantalla y el
otro mostrando la letra `P`:


```c
#include <stdio.h>
#include <pthread.h>

void *perform_work(void *arguments){
    while(1) fputc('H', stderr);
}

int main(void) {
  pthread_t thread;
  pthread_create(&thread, NULL, perform_work, NULL);
  while(1) fputc('P', stderr);
  pthread_join(thread, NULL);
}
```

> NOTA: Para compilar el ejemplo es necesario incluir la librería `libpthread`,
> en GCC puede hacerse añadiendo `-lpthread`.

### Envío de datos a los hilos

Se observa en el ejemplo que la función `perform_work` recibe unos argumentos
que no llegan a usarse. El último argumento de entrada de `pthread_create`,
asignado al valor `NULL` en el ejemplo, es donde pueden enviarse los argumentos
a la función que se ejecutará en el hilo y se recibirán como un puntero vacío
genérico que puede después reconvertirse al tipo que se envió.

### Finalización de los hilos

La función `pthread_join` que en el ejemplo no tiene especial interés es
fundamental cuando se envían argumentos a los hilos. Los hilos comparten la
memoria del proceso por lo que los datos que se les envíen están alojados en la
memoria del proceso principal. Si no se espera por la finalización de los hilos
y éstos han recibido argumentos, si el programa principal termina antes de que
los hilos lo hagan, la memoria del programa se limpia y el hilo pierde sus
argumentos, pudiendo dar a lugar a errores de acceso a memoria. Mediante
`pthread_join` se espera a la finalización de los hilos para asegurar que
siempre tienen sus argumentos disponibles.

Por otro lado `pthread_join` también puede capturar valores de retorno de los
hilos mediante su segundo argumento de entrada asignándolos a la variable que
se le indique.

### Mutexes y semáforos en hilos

Como la memoria de los hilos se comparte, es bastante fácil corromperla si no
se trata con cuidado.

Cualquier escritura en un espacio de memoria compartida es suficientemente
peligroso como para requerir de una gestión adicional ya que si un hilo se
encuentra escribiendo mientras el otro lee, es posible que los datos leídos no
estén escritos por completo y sean incoherentes en el momento de la lectura.
Con la escritura ocurre lo mismo.

Existen muchas herramientas para evitar estos problemas, siendo una de las más
sencillas el uso de un *mutex*. Los *mutex* se comportan como un cerrojo que
sólo un hilo puede cerrar (*lock*) cada vez. Cuando el hilo termina de trabajar
en la sección compartida puede liberar (*unlock*) el cerrojo y dejarlo así
disponible. Cuando un hilo trata de cerrar un *mutex* ya cerrado se bloquea
hasta que éste esté disponible y así nunca coinciden trabajando en un área
peligrosa. Por supuesto, la acción de cierre del *mutex* es atómica, por lo que
no puede ocurrir que varios hilos cierren el *mutex* al mismo tiempo y todo se
bloquee.

Las interfaz de hilos POSIX incluye funciones de gestión de *mutex* así que no
es necesario incluir nada más para tratar con ellos.

En casos más avanzados en los que varios hilos deben interactuar, es en
ocasiones interesante utilizar semáforos en lugar de *mutexes*. Los semáforos
son una versión avanzada de los *mutex*, que en lugar de tener dos estados,
abierto y cerrado, son capaces de contar. Su uso es más complicado que el de
los *mutex* pero aportan una funcionalidad poderosa que quien haga un uso
exhaustivo de los hilos debe conocer. Para saber más, ver `man 7 sem_overview`.


### Conceptos avanzados

Aunque su explicación escapa a lo que se pretende transmitir en este documento,
los hilos POSIX agrupan muchos conceptos adicionales que es interesante
estudiar si se necesita de sus capacidades. La cancelación de los hilos, los
datos propios de cada hilo e incluso una gestión avanzada de los
identificadores de hilo son algunos de estos conceptos.

Con la información transmitida hasta el momento es suficiente para conocer el
interés de los hilos y cómo usarlos, pero si se desea programar alguna
aplicación que dependa mucho de este mecanismo se sugiere leer en detalle la
documentación y conocer muy bien lo que se hace, ya que es muy fácil
equivocarse en el desarrollo de programas multihilo y crear aplicaciones que
fallan de forma inesperada y difícil de analizar.


## Comunicación entre procesos

La comunicación entre procesos (*IPC*, de *Inter-Process Communication*) es muy
importante en los sistemas multitarea como Unix. No todos los tipos de
comunicación entre procesos soportados por el sistema son interesantes para
quien administra la máquina pero casi todos lo son para quien desea programar
aplicaciones para éste.

Aunque en la mayor parte del documento se ha realizado un esfuerzo especial en
hacer referencia a interfaces estandarizadas en POSIX, lamentablemente los
sistemas de comunicación entre procesos tienen una herencia muy fuerte de
System V Unix y la interfaz POSIX no está tan ampliamente disponible.

> NOTA: La interfaz de IPC basada en POSIX suele encontrarse en la biblioteca
> estándar de POSIX para sistemas operativos de tiempo real llamada `librt`. No
> se trata de llamadas al sistema en este caso.

La interfaz estilo System V para IPC puede incluirse en los programas de C
usando las cabeceras `sys/*`, como `sys/shm.h` para la memoria compartida o
`sys/sem.h` para los semáforos.

### Memoria compartida

La memoria compartida (*shared memory*) es quizás el sistema más sencillo de
entender. Su funcionamiento es similar al de cualquier alquiler de memoria
clásico, pero la memoria alquilada queda disponible para varios procesos de
modo que pueden usarla para comunicarse. Este método es también el más rápido
puesto que acceder a memoria compartida es un simple acceso a memoria.

El inconveniente principal de la memoria compartida es que no dispone de ningún
método de control de accesos automático por lo que, igual que en el caso de los
hilos donde se comparte la memoria, el acceso debe controlarse mediante
semáforos o técnicas similares.

La documentación en `man shm_overview` muestra una descripción general del
comportamiento de la memoria compartida estilo POSIX. Para la memoria
compartida estilo System V, ver `man 2 shmget`.

### Semáforos

Los semáforos son el mismo mecanismo presentado para la gestión de hilos, pero
son también capaces de controlar acceso en procesos, que, por ejemplo,
comparten memoria.

Al igual que la memoria compartida existen dos estilos para la gestión de
semáforos: el aportado por POSIX en la cabecera `semaphore.h` y el aportado por
System V en la cabecera `sys/sem.h`. El estilo System V es más antiguo y menos
amigable, pero en algunos casos puede que sea el único disponible.

La documentación en `man sem_overview` describe de modo general el
funcionamiento de los semáforos y compara las interfaces tipo POSIX y System V.


### Tuberías

En secciones anteriores ya se ha hablado de las tuberías así que no debe
sorprender que el sistema operativo exponga una interfaz para su creación y
manejo.

La interfaz para crear tuberías es especialmente sencilla en comparación con el
resto que se acaba de presentar. La llamada a sistema `pipe` construye una
nueva tubería y hace saber al programa que la llame en qué descriptores de
archivo queda disponible, uno para lectura y otro para escritura. A partir de
aquí, cada proceso puede escribir y leer de forma estándar (`read` y `write`,
tal y como se haría en un archivo cualquiera) y el núcleo se encargará de
pausar y arrancar los procesos para que puedan comunicarse correctamente.

La interfaz de las tuberías está contemplada en el conjunto de llamadas al
sistema de POSIX y está disponible bajo `unistd`, por lo que no es necesario
incluir bibliotecas para su uso.

#### Nota práctica: Minishell III: Tuberías y redirección

Aunque se conozca la llamada al sistema capaz de crear tuberías, no es sencillo
deducir cómo aplicar este conocimiento a la *shell* que ya se viene presentando
desde el inicio del capítulo.

El siguiente ejemplo muestra el uso de las llamadas al sistema `dup` junto con
las llamadas al sistema para cerrar archivos, `close`, para mostrar cómo se
explota la creación de una tubería que aplica una redirección de la salida
estándar de un proceso y de la entrada estándar de otro:

``` c
#include<unistd.h>
#include<stdio.h>

int main(int argc, char* argv[]){
    // Creación de la tubería
    int fds[2];
    pipe(fds);

    // Creación de procesos
    int pid = fork();
    if(pid != 0){
        // Proceso padre

        // Redirección de salida estándar
        close(STDOUT_FILENO);                // Cierra STDOUT
        dup(fds[1]);                         // Asigna la tubería a STDOUT

        // Escritura a STDOUT
        // Si se tratara de una shell este punto llamaría a un comando.
        write(STDOUT_FILENO, "hola", 5);
    } else {
        // Proceso hijo

        // Redirección de entrada estándar
        close(STDIN_FILENO);                 // Cierra STDIN
        dup(fds[0]);                         // Asigna la tubería a STDIN

        // Lectura de STDIN
        // Si se tratara de una shell este punto llamaría a un comando.
        char buffer[5];
        read(STDIN_FILENO, &buffer, 5);

        // Muestra en pantalla el mensaje recibido por la tubería
        printf("He recibido> %s\n", buffer); 
    }
    return 0;
}
```

Este ejemplo sencillo muestra dos conceptos al mismo tiempo.

Las tuberías sólo ocupan un las dos primeras líneas de la función `main`. Usar
las tuberías pueden usarse directamente desde ahí, escribiendo al descriptor de
archivo correspondiente, uno como escritura y otro como lectura.

El resto del ejemplo muestra el mecanismo de redirección y su funcionamiento,
pero se debe una explicación para poder comprenderlo correctamente. La llamada
al sistema `dup` explota el funcionamiento de los descriptores de archivos,
duplicando (de ahí su nombre) un descriptor de archivo sobre otro de modo que
son idénticos. Esta duplicación no ocurre de cualquier manera, sino que se
duplican sobre el menor descriptor de archivo disponible tal y como la llamada
de apertura de archivo (ver `man 2 open`) hace.

El hecho de que el descriptor de archivo abierto sea siempre el menor
disponible es lo que habilita la redirección de entrada y salida estándar, que
están asignadas a los descriptores de archivo más bajos (`0` y `1`). Al
reabrir o duplicar un descriptor de archivo se reutilizan descriptores previos,
lo que permite que aunque el programa crea estar escribiendo en `stdout` y
leyendo de `stdin` por tratar con los descriptores `0` y `1`, está realmente
tratando con la tubería, cuyo acceso se habilitó en esos descriptores.

A la hora de extrapolar este conocimiento a la *minishell* que se lleva
trabajando desde el inicio del capítulo, es necesario sustituir las llamadas de
lectura y escritura por una llamada a `exec` que sustituya el proceso actual
por el programa que la *minishell* esté ejecutando en ese momento, pero el
resultado será el mismo. El programa introducido por `exec` en el proceso hijo
hará uso de la entrada estándar en el descriptor de archivo `0` que la
*minishell* ha sustituido por la tubería sin notar la diferencia. Lo mismo
ocurrirá con la salida estándar del proceso padre, ahora asignada a la tubería.

Del mismo modo que se ha usado la tubería en este ejemplo, se puede emplear la
misma técnica para aplicar redirecciones a archivo. Por supuesto, sustituyendo
la llamada a `pipe` por una apertura a un archivo usando `open`.

#### Tuberías con nombre

Las tuberías con nombre, o *FIFO*, ya se introdujeron en el apartado sobre el
sistema de archivos. Su única diferencia con las tuberías sin nombre radica en
que las tuberías con nombre están asociadas a un archivo.

En la *shell* las tuberías con nombre se crean mediante el comando `mknod`,
como ya se estudió, que internamente ejecuta la llamada al sistema del mismo
nombre, `mknod`. Al igual que desde la *shell*, las tuberías con nombre
funcionan exactamente como un archivo una vez creadas, así que tras crearlas
con la llamada a `mknod` simplemente se abren, cierran, leen y escriben como un
archivo cualquiera, por supuesto, con el núcleo gestionando los accesos para
llegar a una sincronización de forma automática.

### Sockets

Los *sockets*, a diferencia de las tuberías, son un mecanismo de comunicación
bidireccional que incluye la capacidad de comunicarse a través de la red.

Quien ya conozca la programación en entornos de red, ya conocerá el concepto
*socket* y cómo se gestionan, debido a que son un concepto que se ha
extrapolado a otros sistemas operativos debido a la influencia de Unix en el
diseño de Internet y las redes TCP/IP.

Los *sockets* están diseñados para trabajar en dos modos, modo *conexión* y
modo *datagrama*, que replican las características de los protocolos de
Internet TCP y UDP, respectivamente. El primero asegura una comunicación
correcta y ordenada entre los dos lados, mientras que el segundo no garantiza
una comunicación correcta y se basa en el envío de mensajes independientes.

Los *sockets* son probablemente el sistema de comunicación más completo y
complejo mostrado hasta ahora. La página del manual `man 7 socket` muestra una
descripción general del sistema de *sockets* y aporta el punto de partida para
el desarrollo de aplicaciones que hagan uso de ellos.

Como ya se analizó al estudiar el sistema de archivos, los *sockets* también
pueden funcionar en la misma máquina, sin necesidad de salir al exterior,
mediante los *sockets* de dominio Unix. Para saber más sobre ellos y aprender a
gestionarlos ver `man 7 unix`.


### Otros

Los conceptos de IPC introducidos aquí no son los únicos disponibles. Unix
también proporciona otros sistemas como las colas de mensajes (ver
`mq_overview`), la posibilidad de compartir memoria a través de `mmap`, etc.

Se ha decidido mencionar algo más en detalle los otros sistemas debido a que
son más usados y están más relacionados con los otros puntos tratados en el
documento.
