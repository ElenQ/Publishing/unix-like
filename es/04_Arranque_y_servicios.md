# Servicios e inicialización del sistema

Los sistemas Unix, como sistemas multitarea que son, suelen ejecutar varias
tareas al mismo tiempo, muchas de ellas en un segundo plano. Estas tareas en
segundo plano, conocidas como servicios (*service*) o *daemon*, simplemente se
mantienen latentes, esperando a ciertos eventos para despertarse y realizar sus
acciones, tan variadas como realizar operaciones de mantenimiento en el
sistema, responder a peticiones de clientes o aceptar conexiones de red.

Aunque quizás no en la misma medida que el sistema de archivos, los servicios
son muy importantes en Unix, ya que el propio sistema es muy usado en el
entorno del servidor, donde todo lo que la máquina hace es lanzar varios
servicios y esperar a que lleguen peticiones.

En realidad los servicios en Unix tienen más incidencia en el sistema de la que
quizás pueda parecer, puesto que son los encargados de muchas tareas
primordiales. Algunas ya se han mencionado, como `udev`, y otras se verán a lo
largo de este capítulo.

## Procesos

Como ya se explicó anteriormente, todos los procesos corriendo en una máquina
Unix tienen un identificador asociado conocido como PID y todos siguen una
jerarquía, donde absolutamente todos los procesos de la máquina tienen un
proceso padre asociado, por ejemplo, al ejecutar un programa en la *shell*,
ésta genera un proceso nuevo y es ella la que se considerará el proceso padre
del programa recién lanzado.

El proceso padre es un mecanismo necesario para limpiar el identificador del
proceso de la tabla de procesos activos. Cuando un proceso hijo termina, es
responsabilidad del proceso padre capturar ese evento y así liberar el PID para
que otro proceso pueda usarlo.

Pero cualquier proceso puede lanzar otros procesos[^shell-pid], por lo que la
jerarquía es profunda, mostrando un árbol genealógico completo, lo que tiene
ciertas implicaciones en el sistema.

[^shell-pid]: El ejemplo más evidente es la *shell*, en ella pueden lanzarse
  nuevas *shells*.

### Procesos zombie

Durante el lapso de tiempo que ocurre mientras que el proceso hijo termina y el
padre lo limpia, se dice que el proceso hijo es un proceso *zombie*, ya que
está «muerto en vida». Lo único que conserva es su PID y todos los demás
recursos asociados a él han sido eliminados por el núcleo.

Si un proceso padre falla a la hora de liberar el PID del proceso hijo (ver
`man wait`), el proceso hijo es posible que se mantenga en estado *zombie* de
forma indefinida. Si esto ocurre en demasiadas ocasiones, se agotarán los PID
de la máquina y puede que no pueda lanzar más procesos.

### Procesos huérfanos

En algunos casos, un proceso puede seguir su ejecución incluso cuando su padre
haya terminado la suya, ya sea por un fallo o porque haya sido terminado por el
usuario. En estas situaciones se dice que el proceso hijo se ha quedado
huérfano (*orphan*) y alguien debe hacerse cargo de él para limpiar su PID
cuando el proceso termine.

### Proceso init

Para que el mecanismo de los procesos sea posible a nivel de sistema, es
necesario un proceso especial, especial porque, por un lado, no tendrá padre,
y, por otro, de alguna forma u otra será el padre (abuelo, bisabuelo o
tatarabuelo) de todos los demás. El proceso conocido como *init* es quien se
encarga de esta tarea.

Al proceso *init* se le asigna el PID número `1` y todo el árbol de procesos
nace de él. Además es él quien acoge a los procesos huérfanos como si hubiese
sido él el que hubiese creado. De esta forma, los procesos huérfanos pueden
seguir con su ejecución y su PID será limpiado por *init* cuando terminen.

El comando `pstree` puede ser útil para mostrar la jerarquía de procesos. En el
siguiente ejemplo se muestra la ejecución del comando en un servidor web donde
`systemd` actúa de proceso *init*, mostrándose como padre de todos los
procesos. Nótese también que no todos los procesos nacen directamente de
`systemd` en el ejemplo, en realidad muchos programas crean sus propios
procesos, llegando así a una estructura de árbol:

``` bash
# pstree
systemd-+-2*[agetty]
        |-cron
        |-dbus-daemon
        |-dhclient
        |-exim4
        |-fail2ban-server---2*[{fail2ban-server}]
        |-fcgiwrap
        |-git---git-daemon
        |-2*[gitolite-shell---git---git---{git}]
        |-nginx---nginx
        |-php-fpm7.0---2*[php-fpm7.0]
        |-rsyslogd-+-{in:imklog}
        |          |-{in:imuxsock}
        |          `-{rs:main Q:Reg}
        |-sshd---sshd---bash---pstree
        |-systemd-journal
        |-systemd-logind
        |-systemd-timesyn---{sd-resolve}
        `-systemd-udevd
```

## Sistema de inicialización

Además de las tareas que ya se han descrito, que tienen que ver con el
funcionamiento básico del sistema de procesos, el proceso *init* suele realizar
muchas tareas adicionales, ya que es el primer servicio en levantarse por el
núcleo, haciendo que tenga gran influencia en la inicialización de la máquina.

El programa *init* suele encontrarse en `/sbin/init` y es llamado por el núcleo
durante el proceso de arranque y se mantiene activo hasta que la computadora se
apague. Si el núcleo no es capaz de encontrarlo lanzará un error y no será
capaz de continuar con el proceso de arranque.

Durante el sistema de arranque, *init* es el proceso encargado de la
**inicialización del sistema**, lo que implica que es quien activa los demás
servicios, activa los módulos adecuados, etc. Es decir, prepara el *espacio de
usuario*.

Por otro lado, suele realizar también la tarea de **monitorizar los servicios**
del sistema y asegurarse que se mantienen activos. Por ejemplo, si un servicio
crítico termina debido a un fallo, el proceso *init* puede ser el encargado de
volver a lanzar el servicio y mantener así el sistema en correcto estado.

El proceso *init* es también el encargado de la **finalización del sistema**,
terminando los procesos uno por uno hasta ser él el último que quede, para
poder pedir así un apagado. Normalmente, los comandos de apagado y reiniciado
del sistema gravitan alrededor del proceso *init*, ya sea porque forman parte
de su paquete o porque son peticiones al proceso.

En definitiva, estas tres funcionalidades pueden resumirse como que este
proceso es el encargado de llevar al sistema a un estado concreto y mantenerlo
en éste.

En ocasiones se diferencia la tarea de **activación de servicios** del resto, y
se utilizan programas independientes para esta labor. Estos programas se
conocen como *process spawner*, aunque en general, todas estas labores las
realiza el mismo sistema.

Como ya se ha visto anteriormente, los sistemas modernos basados en Unix han
heredado muchas metodologías de tiempos pasados. En muchas ocasiones esto
supone una gran ventaja, ya sea por compatibilidad o porque las metodologías
acaban estandarizándose, pero el caso de los sistemas de arranque no es tan
positivo. Las herencias históricas suponían unas limitaciones que han tratado
de superarse y han traído la reimplementación del sistema en incontables
ocasiones. A pesar de ello, muchos de los antiguos conceptos introducidos por
las herramientas originales se han mantenido, a pesar de que la herramienta
para la que se acuñaron quedase obsoleta, lo que ha conllevado mucha confusión.
Es por eso que antes de abarcar los sistemas de arranque más comunes y su
funcionamiento, es necesario conocer la historia.

### Historia

Históricamente ha habido dos estilos principales de configuración del proceso
de inicialización del sistema, que han sido heredados de las dos distribuciones
de Unix más populares: el estilo System V y el estilo BSD.

#### rc scripts (BSD)

En el estilo BSD clásico, el programa *init* ejecuta un programa de la *shell*
ubicado en `/etc/rc`. Este programa es el encargado de seleccionar qué
programas o servicios deben ejecutarse y en qué orden debe hacerse, permitiendo
a los administradores del sistema elegir qué procesos deben arrancarse
automáticamente al iniciar el dispositivo[^bsd-rc-more].

Los BSD más recientes hacen uso de una carpeta de configuración en lugar de un
único programa, en la que listan el conjunto de programas a ejecutar para poner
en marcha el entorno de usuario. La carpeta suele conocerse como `/etc/rc.d` y
suele incluir en cada uno de los programas una indicación de qué se necesita
para ejecutar cada uno de ellos, de esta manera se ejecutan por `/etc/rc` en el
orden correcto (`man 8 rcorder`).

Cada uno de los programas que representan servicios debe disponer de un
conjunto de funcionalidades, `start`, `stop`, `restart` y `status`, que son los
que definen cómo debe iniciarse, pararse, reiniciarse y comprobarse el estado
del programa, respectivamente.

Cuando se desea, por ejemplo, parar la ejecución del servicio de HTTP, se puede
ejecutar lo siguiente:

``` bash
# /etc/rc.d/httpd stop
```

Este estilo es sencillo y permite la interacción directa del usuario con los
servicios aunque, por otro lado, no aporta funcionalidades avanzadas de
monitorización ni control de procesos. Es por eso que puede decirse que este
sistema no es más que un *process spawner* (ejecutor de procesos) independiente
del sistema *init*, que es el que lo llamaría.

[^bsd-rc-more]: El siguiente apartado de la documentación de NetBSD proporciona
  información más detallada de cómo se utiliza el sistema en la actualidad:  
  <https://www.netbsd.org/docs/guide/en/chap-rc.html>


#### System V init

System V init, también conocido simplemente como *init*, es quizás el más usado
de los dos, tanto que es difícil separar el nombre del concepto *init* del
nombre del propio sistema de inicialización de System V y muchas personas los
confunden.

El sistema estilo System V usa el concepto *runlevel* (nivel de ejecución) para
definir posibles estados del sistema. Pasar de un nivel a otro implica la
ejecución de varios programas que realizan la transición.

Los *runlevel* suelen ser siete en total (a veces son ocho, porque incluyen un
nivel adicional llamado `S`), y normalmente hay tres de ellos que siempre se
utilizan del mismo modo:

- `0`: Sistema apagado
- `1`: Sistema en modo usuario único
- `6`: Reiniciado del sistema

El resto de niveles quedan sin definirse y cada distribución los define a su
manera en el archivo `/etc/inittab`. Todos los programas que deben arrancarse
se suelen almacenar en `/etc/init.d` para que quien administra el sistema pueda
comprobar el estado, lanzarlos o terminarlos, igual que en el caso anterior:

``` bash
# /etc/init.d/httpd stop
```

Sin embargo, el directorio `/etc/rc.d/` se expande a `rcN.d` para cada
*runlevel*, donde la `N` representa el nivel:

``` bash
$ ls /etc/
rc0.d
rc1.d
rc2.d
rc3.d
rc4.d
rc5.d
rc6.d
rcS.d
...
```

Cada uno de los directorios `rcN.d` almacena un conjunto de enlaces simbólicos
a programas en `/etc/init.d` y es así cómo se encuentran cuales de los procesos
deben ejecutarse en cada *runlevel*.

Para definir correctamente en qué orden deben ejecutarse, los enlaces se
almacenan usando una nomenclatura concreta. Los enlaces que comienzan con la
letra `K` (de *kill*) deben terminarse al entrar a este *runlevel* y los que
comienzan con la letra `S` (de *start*) deben arrancarse. Después de la letra,
un número del `00` al `99` indica cuál es el orden en el que deben arrancarse o
terminarse los programas.

Para saltar de un *runlevel* a otro, quien administra el sistema puede ejecutar
el comando `init` o `telinit`, seguido del *runlevel* al que desea acceder. Por
ejemplo, `init 0` apaga la computadora. Internamente, el comando `init 0`
provoca la ejecución del programa `/etc/init.d/rc` que es el encargado de
activar el *runlevel* solicitado siguiendo el proceso de ejecución descrito
previamente.

Este sistema es obviamente más complicado que el sistema mínimo de BSD, que
simplemente ejecuta un programa y espera que sepa como ordenar los servicios,
pero tampoco solventa demasiados problemas.

Aunque sí que define un concepto de estado de la máquina y es capaz de
configurar los servicios para llegar a él, sigue permitiendo y fomentando el
acceso directo de los usuarios a los programas de configuración de los
servicios y, similar al caso de BSD, los programas se ejecutan en orden, sin
dar lugar al uso del paralelismo que pueda acelerar el proceso.

### Programas alternativos

Los dos estilos clásicos de Unix recién descritos son simples *process
spawners*, puesto que sólo ejecutan los procesos necesarios para llegar a un
estado del sistema. Curiosamente, el nombre del directorio `rc`, que también da
nombre al sistema BSD, viene de las palabras en inglés *run command*, que
significa literalmente «ejecutar comando». Es evidente que sus intenciones eran
simplemente lanzar los comandos y olvidarse de ellos.

Estos dos estilos también dejan algunas carencias. La más evidente la
complejidad de configuración en entornos avanzados, donde es necesario alterar
la configuración manualmente en el caso de BSD y alterar `inittab` en el caso
de System V. Cualquiera de esas dos opciones es bastante peligrosa, ya que un
fallo en la configuración puede bloquear el arranque del sistema por completo.

Otro problema heredado de las primeras implementaciones es que están pensadas
para ejecutarse de forma secuencial, levantando los servicios uno por uno. Esto
hace que aunque no todos los servicios dependan de otros deban esperar a
inicializarse, cosa que no tiene demasiado sentido en un entorno *multitarea*
como Unix.

Muchos sistemas de configuración de la inicialización se han diseñado desde
entonces, tratando de reemplazar los antiguos métodos, pero no todos han tenido
éxito, principalmente porque necesitan aportar cierto nivel de
retrocompatibilidad si desean ser adoptados por la mayoría de distribuciones y
muchos se sienten tentados a repetir las viejas fórmulas para que quienes
administran las máquinas no deben aprender un nuevo vocabulario y metodologías.

#### systemd

El más rupturista en este caso es `systemd`, un proyecto de largo recorrido que
pretende reemplazar no sólo el servicio *init* y su configuración sino gran
variedad de utilidades del sistema, que abarcan mucho más que el sistema de
inicialización[^systemd-extras].

[^systemd-extras]: Este apartado se centra en la gestión de servicios, pero
  `systemd` realiza muchas más acciones que esa. Para saber más, ver:  
  <https://systemd.io>

`systemd`, además de una inicialización de servicios extremadamente
paralelizada, proporciona monitorización de servicios, mantenimiento de puntos
de *mount*, gestión de logs de sistema, de la red y de usuarios y muchísimas
cosas más, todo ello sin perder compatibilidad con los estilos tradicionales
para facilitar la transición. Es por eso que es el sistema de *init* más usado
hoy en día[^systemd-critics].

[^systemd-critics]: A pesar de ser muy criticado, hasta el punto de que su
  desarrollador principal haya recibido amenazas.

A nivel de gestión de sistema, los servicios de `systemd` se configuran
mediante archivos de configuración que se almacenan en el directorio
`/etc/systemd`. Estos archivos pueden describir dependencias de otros módulos o
servicios del sistema de forma sencilla así como añadir especificaciones de
cómo deben ser ejecutados. Por ejemplo, pueden definir qué usuario debe
ejecutar el programa, si debe reiniciarse el proceso en caso de que termine de
forma inesperada, con qué identificador debe almacenar sus logs, etc.

En resumen, `systemd` es capaz de describir de forma declarativa gran parte de
la configuración de un sistema moderno, no sólo lo que concierne a la
inicialización de servicios. El problema que esto conlleva es que puede ser
demasiado complejo para aplicaciones con limitaciones de potencia y espacio.

`systemd` es el sistema *init* por defecto en las principales distribuciones de
Linux como Debian, Ubuntu, Arch Linux y muchas otras.


##### Nota práctica: Gestión de servicios en systemd

El siguiente ejemplo muestra un archivo de configuración de un *daemon* de
`git` en `systemd`:

``` ini
[Unit]
Description=Start Git Daemon

[Service]
ExecStart=/usr/bin/git daemon  --base-path=/var/lib/gitolite3/repositories \
    --reuseaddr /var/lib/gitolite3/repositories

Restart=always
RestartSec=500ms

StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=git-daemon

User=gitdaemon
Group=gitolite3

[Install]
WantedBy=multi-user.target
```

En este archivo de configuración sencillo se expresa todo lo necesario para
ejecutar (`ExecStart`) el programa, mantenerlo en ejecución (`Restart` y
`RestartSec`) y gestionar sus logs (`StandardOutput`, `StandardError` y
`SyslogIdentifier`). Por otro lado, se configura el servicio de modo que
pertenezca al grupo `gitolite3`, permitiendo así al programa acceder a los
directorios gestionados por `gitolite3`.

El apartado `Install` muestra una línea `WantedBy=multi-user.target` que
describe en qué estado del sistema debe ejecutarse este servicio. En `systemd`
cada *target* describe un estado del sistema, similar a lo que los *runlevel*
hacían en System V init, pero con un sistema más avanzado, descriptivo y, sobre
todo, configurable (ver `man systemd.target`).

El resto de campos del servicio disponibles pueden consultarse mediante `man
systemd.service`.

Para interactuar con los servicios `systemd` aporta una interfaz basada en el
comando `systemctl`. El siguiente ejemplo habilita y lanza el servicio del
ejemplo anterior, suponiendo que esté almacenado en `/root/git.service`:

``` bash
# systemctl enable /root/git.service 
Created symlink /etc/systemd/system/multi-user.target.wants/git.service → 
    /root/git.service.
Created symlink /etc/systemd/system/git.service → /root/git.service.
# systemctl start git.service
# systemctl status git.service
● git.service - Start Git Daemon
   Loaded: loaded (/root/git.service; enabled; vendor preset: enabled)
   Active: active (running) since Wed 2021-09-08 13:45:31 CEST; 2s ago
 Main PID: 30650 (git)
    Tasks: 2 (limit: 4915)
   CGroup: /system.slice/git.service
           ├─30650 /usr/bin/git daemon --base-path=/var/lib/gitolite3/repositories
                --reuseaddr /var/lib/gitolite3/repositories
           └─30654 git-daemon --base-path=/var/lib/gitolite3/repositories
                --reuseaddr /var/lib/gitolite3/repositories

Sep 08 13:45:31 vps235680 systemd[1]: Started Start Git Daemon.
```

Como puede verse, el último comando comprueba el estado del servicio y se
asegura de que está en marcha, pudiendo obtener información interesante sobre
él. La interfaz de comandos de comprobación y gestión de servicios es similar a
la de System V y BSD, sólo que en esta ocasión todo pasa por `systemd` en lugar
de por la ejecución de programas independientes.

A diferencia de System V init y BSD rc, el ejemplo muestra cómo habilitar un
servicio no implica alterar el contenido de ningún archivo de configuración
interno de manera manual. Es el propio `systemd` el encargado de crear los
enlaces necesarios en el sistema al habilitar el servicio y el de eliminarlos
al deshabilitarlo (`systemctl disable`).

#### Openrc

En distribuciones más reducidas se aplican otros mecanismos. Openrc es un
sistema de `rc` basado en dependencias que usa el servicio `init` aportado por
el sistema. Esto significa que Openrc únicamente ejecuta comandos y no está
pensado para ser el PID 1[^openrc-pid1].

Openrc aporta funcionalidades modernas como análisis de dependencias de
procesos, ejecución paralela, soporte para servicios complejos con muchos
componentes, etc.

Es similar a los sistemas antiguos, que se basan en programas que deben exponer
unas funciones en particular (`start`, `stop`...), pero añade una capa de
configuración para una modularidad mayor.

Es el sistema de ejecución de comandos de inicio por defecto en Gentoo y Alpine
Linux.

[^openrc-pid1]: En versiones más recientes se añadió soporte tanto para ser el
  *init* del sistema como para monitorizar servicios en componentes opcionales
  independientes.

#### runit

`runit` es un sistema *init* que puede funcionar junto a Openrc o por separado.
Es una reimplementación de *Daemontools* un software de gestión de servicios
para Unix.

A diferencia de Openrc, este componente está pensado para monitorizar y
gestionar los procesos y ser el PID 1 del sistema.

`runit` es el sistema de *init* de, entre otras, Devuan, una distribución
basada en Debian cuya característica principal es que no usa `systemd`.

#### GNU Daemon Shepherd

GNU Daemon Shepherd (o GNU Shepherd) es un *init system* desarrollado usando el
lenguaje de extensiones de GNU, GNU Guile, que nació para aplicarse en el
sistema operativo GNU/Hurd[^gnu-hurd], aunque actualmente es quizás más famoso
por ser el sistema *init* por defecto en la distribución Guix.

[^gnu-hurd]: GNU/Hurd es un sistema operativo del proyecto GNU que combina el
  entorno de usuario de GNU, con Hurd, un núcleo desarrollado por también por
  GNU.

El funcionamiento del GNU Shepherd es similar al de otros sistemas *init*. El
servicio `shepherd` queda arrancado en *background*, con el PID 1, tras leer la
configuración del sistema de servicios en `/etc/shepherd.scm`, levantándolos
como corresponda, y queda a la espera de órdenes en el *socket*
`/var/run/shepherd/socket`.

El comando `herd` envía órdenes al socket para que el servicio principal las
escuche. `herd restart` seguido del nombre de un servicio, por ejemplo, sirve
para reiniciar un servicio. El sistema también aporta los comandos `halt` y
`reboot` para terminar todos los servicios y, respectivamente, apagar y
reiniciar el dispositivo.

El GNU Shepherd no se basa en el concepto de *runlevel*, sino que cada servicio
es capaz de declarar qué aporta y qué necesita de modo que la entidad central,
el servicio `shepherd`, sea capaz de conocer qué servicios dependen de cuáles
sin tener que basarse en grupos de servicios que pueden no estar muy
relacionados entre ellos.


## Nota práctica: Sistema de arranque de Linux

Hasta el momento, se han tratado detalles relacionados con el sistema operativo
una vez que éste ya ha arrancado, pero no sobra tener ciertos conocimientos de
cómo arranca un sistema en hardware moderno. A pesar de ser un campo muy amplio
y variado, principalmente por las características concretas de cada
dispositivo, una visión general del sistema de arranque de sistemas basados en
el núcleo Linux es interesante para tener una perspectiva del contexto
adecuada. Es necesario recordar, sin embargo, que Linux es un caso muy
particular de cómo implementar un núcleo, y que este conocimiento no es
necesariamente extrapolable a otros núcleos como BSD.

### Ordenadores clásicos

Los ordenadores clásicos, ya sean servidores u ordenadores de escritorio,
tienen una separación de tareas bastante clara, mientras que en los ordenadores
embebidos las cosas tienden a complicarse. Conocer primero el caso de los
ordenadores «normales» facilita la tarea posterior ya que los conceptos son los
mismos pero las líneas están más borrosas.

#### BIOS

En los ordenadores más antiguos, al pulsar el botón de encendido, la placa base
ejecuta lo que se conoce como BIOS (*Basic Input Output Services*), un firmware
que despierta las diferentes piezas del equipo y las inicializa. La BIOS
también realiza comprobaciones de que todo esté en orden antes de iniciar el
equipo.

Esta BIOS es un estándar de-facto, nacido en los ordenadores IBM PC, que
después se adoptó por la mayoría de empresas de informática. La BIOS reside en
la placa base, escrita en una memoria no-volátil a la espera de que el
procesador la lea y active todas las partes. Cada placa base dispone de una
BIOS distinta, que describe cómo deben inicializarse y usarse sus componentes
de forma mínima para poder arrancar el dispositivo. Las BIOS además, pueden
permitir la configuración del sistema: eligiendo dispositivos de arranque,
velocidad de los ventiladores, etc. Todo partiendo desde una interfaz mínima.

Los equipos antiguos ejecutan el sistema desde la propia BIOS, ya que esta
tiene la capacidad de leer la sección MBR (*Master Boot Record*) de los
dispositivos de almacenamiento e inicializar el sistema desde ahí. En esas
secciones de los dispositivos de almacenamiento únicamente hay 512 bytes
disponibles y en ellos se debe incluir el programa que ejecute finalmente el
sistema. La BIOS simplemente saltará a esa sección y dejará que el programa en
esa localización arranque el sistema.

Como ese espacio es muy reducido, normalmente es necesario usar un programa de
arranque (*bootloader*) en dos etapas, una primera en la sección MBR, y otra
posterior más amplia en otra sección sin restricciones. Al terminar la sección
MBR, el programa de arranque saltaría a la sección sin restricciones para
continuar. Esto permite que el sistema de arranque aporte más funcionalidades
de las que pueden añadirse sólo en 512 bytes.

Una vez el *bootloader* recibe el control, el proceso es similar en todos los
sistemas.

#### UEFI

UEFI (*Universal Extensible Firmware Interface*) es una especificación
pública que nace para reemplazar al sistema anterior basado en BIOS, que no era
un estándar abierto. Aunque UEFI aporta soporte de compatibilidad con BIOS, el
objetivo de UEFI es mucho más ambicioso que ser un simple reemplazo: UEFI es
una interfaz estándar entre el sistema operativo y el firmware de la máquina.

UEFI define gran cantidad de funcionalidades adicionales para el
firmware[^uefi]. Una de la más relevantes es el uso de GPT (*GUID Partition
Table*) como sustitución de MBR, con lo que UEFI es capaz de detectar los
diferentes puntos de arranque de forma mucho más efectiva. Además, UEFI define
el concepto de «Aplicación UEFI» (*UEFI Application*), aplicaciones que,
guardadas en la partición de sistema *EFI* (*EFI System Partition*), pueden
ejecutarse desde UEFI directamente[^uefi-loading] e incluso llamarse entre
ellas.

Las aplicaciones UEFI en este caso pueden ser el propio núcleo del sistema
operativo o un *bootloader*. A diferencia del caso de la BIOS, donde es
necesario manejar el reducido espacio disponible en MBR[^GPT-BIOS], en UEFI las
aplicaciones se ejecutan de forma mucho más simple, porque el sistema las
reconoce directamente.

Estas aplicaciones normalmente se montan en `/boot/efi/` por lo que pueden
accederse en el sistema de archivos una vez levantado el sistema.

[^GPT-BIOS]: Algunas BIOS más modernas también usan GPT, aunque no es muy
  común.

[^uefi]: Uso de particiones más grandes, interfaz gráfica multilenguaje,
  conectividad de red, un diseño modular, una interfaz de programación en C y
  un larguísimo etc.

[^uefi-loading]: Este artículo tiene una explicación muy profunda del sistema
  de arranque en UEFI, mucho más detallada de lo que se expone en este
  documento:  
  <https://www.happyassassin.net/posts/2014/01/25/uefi-boot-how-does-that-actually-work-then/>


#### Bootloader

El *bootloader* es una etapa hasta cierto punto opcional. La tarea del
*bootloader* es la de cargar el núcleo en la memoria y comenzar la ejecución
del sistema. En UEFI, el propio núcleo puede implementarse como una aplicación
UEFI de modo que no sea necesario usar un *bootloader* mientras que en las BIOS
que usan MBR será necesario implementar al menos un *bootloader* mínimo que sea
capaz de saltar al código del núcleo, entre otras tareas.

En cualquier caso, el *bootloader* es un componente interesante ya que puede
aportar funcionalidades adicionales como seleccionar entre los distintos
sistemas operativos en la máquina (si los hubiera) o arrancar con ciertas
características adicionales. Algunos *bootloaders* aportan también una interfaz
de comandos mínima donde pueden comprobarse diversos aspectos de la máquina y
realizarse tareas de mantenimiento, sin haber cargado el sistema operativo aún.

En los sistemas Linux el *bootloader* más famoso hoy en día es GRUB (*GNU GRand
Unified Bootloader*) en su versión 2, aunque anteriormente lo fueron otros como
LILO o la propia versión anterior de GRUB.

GRUB almacena su configuración en `/boot/grub/grub.cfg` y la usa para mostrar
su menú de selección de sistema operativo y su terminal de comandos así como
para indicar de dónde debe tomarse la imagen del núcleo y del `initramfs`.

En sistemas que usan UEFI es sencillo ver que GRUB es una aplicación UEFI
simplemente listando el contenido de `/boot/efi/`.

En sistemas con MBR, GRUB se ejecuta en etapas debido a las limitaciones de
tamaño que MBR impone, pero el resultado es el mismo.

Una vez que GRUB ha arrancado y el sistema operativo se ha seleccionado, GRUB
lo carga en la memoria y lo ejecuta, cediéndole el control.

#### Núcleo

El *bootloader* carga el núcleo en la memoria partiendo de una localización en
disco. El núcleo suele estar comprimido, pero él mismo tiene la capacidad de
descomprimirse. Una vez cargado en la memoria comienza a ejecutarse.

Este núcleo recién cargado no abandona la memoria del ordenador hasta que éste
se apague por lo que interesa que sea lo más pequeño posible, o al menos que no
sea demasiado grande. El núcleo Linux hoy en día dispone de *drivers* para
infinidad de dispositivos distintos, por lo que cargar todos en memoria sólo
para asegurarse de que se encontrarán los *drivers* adecuados para el
dispositivo actual no tiene sentido. Sin embargo, cargar un núcleo sin drivers
no permite que éste sea capaz de leer desde otras localizaciones como el disco
duro en busca de los drivers que necesita, así que es necesario llegar a un
punto intermedio.

El núcleo Linux resuelve este caso mediante un sistema que se conoce como
`initramfs`[^initramfs-naming].

[^initramfs-naming]: Anteriormente existía un mecanismo llamado `initrd`
  (*Initial RAM Disk*), disco inicial en memoria RAM, que se quedó obsoleto con
  la aparición de `initramfs`. Hoy en día hay proyectos, como por ejemplo GRUB,
  que usan los dos nombres indistintamente para referirse al concepto moderno,
  `initramfs`.

##### initramfs

`initramfs` (*Initial RAM FileSystem*) es un sistema de archivos inicial en
memoria que permite que el núcleo incluya datos a la hora de cargarse de forma
que más adelante puede liberarlos.

El `initramfs` se define como un archivo `cpio`, normalmente comprimido, que se
carga en memoria junto con el núcleo. Linux utiliza el mecanismo `tmpfs`, un
sistema de almacenamiento temporal en la memoria volátil, para cargar el
`initramfs`, donde dispone de acceso a los datos que necesita para arrancar.
Este sistema de archivos temporal se carga en la raíz del sistema y actúa como
si de un sistema completo se tratara, aunque sea únicamente temporal.

El `initramfs` contiene todo lo necesario para que se pueda realizar un
arranque correcto de la máquina, esto es, detectar todos los dispositivos
conectados a la máquina, cargar sus respectivos drivers, montar los discos e
inicializar los procesos.

Una vez cargado el `initramfs`, el núcleo ejecuta el programa `/init`, que será
el encargado de inicializar los procesos, entre los cuales se encuentra `udev`,
que habilita el acceso a los dispositivos y el cambio a la raíz final («`/`»)
(`man switch_root`). En este punto el núcleo no espera que la llamada a `init`
termine, puesto que éste será el proceso padre en el sistema y simplemente se
duerme, a la espera de llamadas a sistema o eventos que requieran de su
intervención. El programa `init` sin embargo, deberá reemplazarse a sí mismo
por el proceso `init` final, normalmente `/sbin/init`, una vez termine el
montado del sistema de archivos completo, para permitir que el programa *init*
prepare el entorno de usuario y se haga cargo de los procesos como corresponde
(este paso suele realizarse desde `switch_root` en una sola etapa).

##### initrd

`initrd` (*Initial RAM Disk*), disco inicial en memoria RAM, quedó obsoleto con
la aparición de `initramfs`, pero aún es posible verlo en algunos dispositivos.
Es el proceso que se realizaba antiguamente, que en lugar de cargar un sistema
de archivos temporal cargaba un archivo de dispositivo (normalmente `/dev/ram`)
que después se montaba como raíz en el sistema («`/`»). Este proceso era más
costoso porque requería del montado de la raíz y conceptualmente era más
complejo.

Una vez levantado el disco en memoria, el núcleo podía obtener todo lo
necesario desde éste, incluyendo los drivers de acceso al almacenamiento, donde
se encuentra la raíz del sistema de archivos.

Al tener todo cargado en memoria, el núcleo llamaba a un programa llamado
`/linuxrc`, `/initrd` o similar, que era el encargado de inicializar todos los
sistemas y pivotar a la raíz (`man pivot_root`) final desde la raíz temporal
del disco en memoria. Una vez terminada esta acción, el núcleo entendía que
todo estaba listo así que liberaba el disco en memoria y llamaba al programa
`/sbin/init`, que es el que levantaba los servicios necesarios y actuaba de
`init`.

Igual que en el caso anterior, el núcleo entraba en pausa, a la espera de
recibir eventos que requiriesen de su atención.

La diferencia principal, aparte de que el mecanismo de cargado de datos es
diferente, es que Linux llama a dos programas independientes en este segundo
caso, mientras que en el primero es el propio `/init` el encargado de hacer
toda esa labor.

##### Modo compatibilidad

Si bien es cierto que el proceso antiguo ha dejado de usarse, el núcleo tratará
de montar `/` y ejecutar `/sbin/init` si durante el proceso no encuentra el
programa `init` en el `initramfs`. De este modo se da soporte a los programas
`init` más clásicos que se guardan como un ejecutable en `/sbin/init` en lugar
de en el `initramfs`.

##### Otras configuraciones

Como el *bootloader* es capaz de cargar el núcleo con varios parámetros
adicionales, algunas distribuciones alteran el proceso de ejecución del
programa `init` o hacen alteraciones en cómo se desarrollan las tareas. Al
final lo importante es conocer que el núcleo no puede cargarse solo, puesto que
no conoce los drivers ni cómo se desea arrancar el sistema, y debe ir
acompañado, por tanto, por un conjunto de datos que le ayuden.

### Sistemas embebidos

En los sistemas embebidos las líneas se diluyen ya que no suele haber una
diferencia tan grande entre el firmware y el propio software del dispositivo
(es por eso que se conocen como sistemas embebidos).

En estos sistemas el software suele cargarse de una localización particular de
forma automática, por definición de fábrica. Esta localización puede tratarse
de una memoria ROM accesible mediante I2C, una tarjeta SD u otras.

Algunos dispositivos más avanzados dispondrán de BIOS y su proceso será similar
al de los ordenadores clásicos, pero es poco probable. UEFI, en cambio, es casi
imposible de encontrar.

Esto hace que los *bootloaders* tengan otras características. Uno de los más
comunes en el entorno de Linux, DAS U-Boot (conocido como UBoot), es digno de
estudiar.

#### Das U-boot

Uboot se carga directamente desde la interfaz definida por el dispositivo, ya
sea una memoria ROM, una tarjeta SD o cualquier otra, si es necesario
separándose en varias etapas para adaptarse a las limitaciones de espacio que
puedan aplicar (similar al caso de MBR).

Este *bootloader* realmente actúa también como firmware, aportando
inicialización de dispositivos previa a la ejecución del sistema operativo. Su
característica principal es que implementa el estándar UEFI para sistemas
embebidos (EBBR, *Embedded Base Boot Requirements*), que asegura una
compatibilidad entre dispositivos embebidos. Mediante la implementación de este
estándar, tanto GRUB como un núcleo Linux pueden ejecutarse como aplicaciones
UEFI sin mayor problema, aunque lo más habitual sea lanzar directamente el
núcleo y delegar en UBoot las tareas que normalmente están asociadas a GRUB.

UBoot además es capaz de gestionar *devicetrees*, permitiendo la correcta
configuración del núcleo para la plataforma en la que se ejecuta, detalle
especialmente necesario en el entorno embebido.

Por otro lado, a diferencia del caso de los ordenadores clásicos, donde la
ordenación de la memoria es siempre similar y por tanto se usan las secciones
por defecto, como los dispositivos embebidos tienen características muy
concretas, UBoot permite la selección exhaustiva de dónde debe cargarse cada
componente en la memoria y habilita una configuración muy granular.

Otra funcionalidad interesante de UBoot, que comparte con GRUB y UEFI, es que
aporta acceso a una terminal de comandos. Los comandos ejecutables en estas
plataformas son bastante reducidos y, lógicamente, están limitados al contexto
en el que se encuentran, pero en Uboot estos comandos son especialmente
interesantes ya que aportan toda la funcionalidad de escritura y lectura de
memoria y almacenamiento. Esta terminal, accesible desde un puerto serie, puede
ser primordial durante el desarrollo, ya que permite escribir nuevas versiones
de Uboot o instalar un nuevo sistema operativo en la máquina sin necesidad de
ejecutar el sistema completo, detalle especialmente útil en casos de corrupción
del sistema.
