# Entorno de trabajo

Una vez conocida la plataforma y adquirida una visión general de este sistema
operativo multiusuario y multitarea, es momento de comenzar a comprender el
entorno de trabajo que expone a quienes lo usan.

Como se ha ido describiendo, el sistema operativo puede describirse en tres
componentes principales: el núcleo, la shell y programas adicionales. Todos
ellos han sido mencionados en mayor o menor medida pero aún no se ha analizado
aún cómo sacar partido al sistema completo desde la perspectiva de quien debe
trabajar con él. Para ello, la atención debe centrarse en los dos componentes
más externos, la shell y los programas adicionales, y cómo trabajan juntos para
aportar la experiencia de usuario tan particular de Unix.

Por supuesto, en Unix, como en cualquier otro sistema con este nivel de
popularidad, existen infinidad de aplicaciones por lo que tratarlas todas es
imposible. En este documento se tratarán aplicaciones de bajo nivel accesibles
desde la terminal de comandos, por las razones que ya se trataron previamente,
y se aparta el uso de entornos de escritorio de la ecuación ya que son más
parecidos a un uso convencional de una computadora moderna y hoy en día son
bastante accesibles.

Otra razón por la que se centra la atención en la terminal es que ésta expone
unas funcionalidades avanzadas muy poderosas en Unix, siendo no sólo un
pegamento perfecto para la composición entre varios programas, sino también un
entorno de trabajo sorprendentemente poderoso que, según a quién se pregunte,
sigue sin tener rival en la actualidad.

Más allá de las opiniones personales de cada uno, Unix está muy presente en
entornos donde el acceso a una interfaz gráfica no es una opción, así que
cuanto mejor se conozca el entorno de trabajo mejor para quien tenga que
lidiar en esas situaciones.


## Uso avanzado de la shell

Como entorno de trabajo la *shell* proporciona muchas facilidades a quien la
usa, algunas son internas, implementadas por la propia *shell* y otras son
externas, aportadas por el sistema operativo para que la *shell* las aproveche
de la mejor manera.

### Readline

Las *shells* compatibles con POSIX incluyen una funcionalidad avanzada de
edición de línea, basada en combinaciones de teclas, que facilita la edición
rápida de los comandos.  Esta funcionalidad incluye gestión del historial,
edición rápida, envío de señales a los programas, etc.

El proyecto GNU, en el momento de desarrollar su propia versión de la *shell*,
Bash, desarrolló su propia implementación de este comportamiento. Con el
tiempo, esta funcionalidad se separó a una biblioteca independiente, conocida
como GNU Readline, que es una de las bibliotecas más conocida para esta labor.
GNU Readline no es la única implementación de este comportamiento, pero quizás
sí la más frecuente es por eso que es la que se selecciona como referencia para
este apartado[^gnureadline].

Es importante añadir también que este tipo de edición de línea se usa en muchos
otros programas, por lo que es también interesante conocerla con otros fines.

Readline puede configurarse de dos modos: estilo Vi y estilo Emacs, siendo el
estilo por defecto el segundo y también el más usado. Estos estilos están
relacionados con dos de los editores de texto clásicos más relevantes ya que
tratan de emular su funcionamiento y sus combinaciones de teclas. Como el
estilo Emacs es el más común, en este documento sólo se tratarán ejemplos de
este modo, pero todos tienen su equivalente (ver `man bash` y `man readline`).

Los comandos más conocidos y útiles son los siguientes:

- `Tab` o `Ctrl+I`: Autocompleta el comando o argumento analizando el contexto
  actual: archivos en el directorio, programas en el `PATH` o archivos de
  autocompletado conocidos por la *shell*.
- `Ctrl+U`: Borra la línea de texto desde la posición del cursor hacia la
  izquierda.
- `Ctrl+K`: Borra la línea de texto desde la posición del cursor hacia la
  derecha.
- `Ctrl+W`: Borra la última palabra hacia la izquierda.
- `Ctrl+P` o `↑`: Carga el comando previo desde el historial.
- `Ctrl+N` o `↓`: Carga el siguiente comando desde el historial.
- `Ctrl+C`: Envía la señal SIGINT al comando en ejecución.
- `Ctrl+Z`: Envía la señal SIGTSTP al comando en ejecución.
- `Ctrl+D`: Envía un marcador de final del archivo (`EOF`) al comando actual o
  a la *shell*.

Lo que mucha gente desconoce de Readline es que los comandos de borrado se
almacenan en un historial propio por lo que el texto borrado puede pegarse:

- `Ctrl+Y`: Pega el último borrado.
- `Alt+Y`: Pulsado después de un pegado, sustituye el texto recién pegado por
  el borrado anterior del historial, permitiendo rotar por todo el historial de
  borrado pulsando la combinación varias veces.

Y la lista sigue con comandos de salto (`Ctrl+A`, `Ctrl+E`, `Alt+B`,
`Alt+F`...), de búsqueda en el historial (`Ctrl+R`, `Ctrl+S`), deshacer y
rehacer cambios en la línea actual (`Ctrl+_`) y comandos complejos que implican
combinaciones de teclas avanzadas (`Ctrl+X,Ctrl+U`; `Ctrl+X,Ctrl+E`...).

Todos ellos son extremadamente útiles para una relación agradable con la
terminal pero tampoco tiene demasiado sentido aprenderlos de memoria para
trabajar. Lo más sensato es ir incluyéndolos poco a poco al repertorio que cada
cual maneja.

[^gnureadline]: Esta biblioteca puede usarse en programas nuevos sin demasiada
  dificultad y es una genial manera de añadir una buena interfaz de comandos al
  programa. Existen también otras alternativas similares como libedit (popular
  en BSD), linenoise u otras, aunque casi todas comparten las combinaciones de
  teclas más conocidas.

### Redirecciones de entrada y salida

Por defecto, los comandos ejecutados en la terminal de comandos exponen sus
resultados en pantalla pero gracias a que «*todo es un archivo*» esto puede
flexibilizarse dando lugar a combinaciones muy interesantes.

En primer lugar, la **salida** del comando puede desviarse a un archivo
mediante el símbolo «`>`» o el símbolo «`>>`». La única diferencia entre las
dos opciones es que la primera sobreescribe el contenido del archivo mientras
que la segunda añade contenido al final de éste.

``` bash
$ ls /var
cache  db  empty  guix  lib  lock  log  run  tmp
$ ls /var > listado
$ cat listado
cache  db  empty  guix  lib  lock  log  run  tmp
```

Del mismo modo que la salida puede redirigirse también puede redirigirse la
**entrada** mediante «`<`». Algunos comandos (`wc`, `cat`, `grep`...) obtienen
su entrada por defecto desde la terminal y al ejecutarlos esperan a que el
usuario introduzca texto y lo finalice mediante un aviso de finalización de
archivo o `EOF` (*end of file*), que en las *shells* compatibles con POSIX se
envía mediante `Ctrl+D`. Estos comandos no están diseñados para funcionar así,
sino que se diseñan como comandos de filtrado que obtienen su entrada desde una
redirección.

``` bash
$ wc < listado
 9  9 41
```

En realidad muchos de estos comandos de filtrado de texto que están diseñados
para obtener su entrada desde una redirección también son capaces de abrir una
archivo y leer desde él cuando se les envía como argumento, por lo que usar una
redirección de entrada no tiene demasiado sentido en este caso:

``` bash
$ wc listado
 9  9 41
```

Sin embargo, existe una capacidad adicional de redirección que es realmente
valiosa para estos comandos: **la tubería** sin nombre (*pipeline* o
simplemente *pipe*). La tubería, aplicable con el símbolo «`|`», puede
entenderse como una combinación de ambos conceptos, la redirección de entrada y
de salida, en un único paso:

``` bash
$ ls /var | wc
 9  9 41
```

Como puede apreciarse, en este caso no se ha necesitado de ningún archivo
intermedio y la salida del primer comando ha fluido a la entrada del segundo
sin ningún problema.

Estos mecanismos son perfectamente compatibles entre ellos y la salida de una
tubería entre dos comandos puede redireccionarse o enviarse a través de otra
tubería. Este sistema de coordinación entre pequeños programas que intercambian
información a través de tuberías es una parte primordial de Unix y en este
punto refleja con la mayor claridad posible su filosofía:

- «*Escribe programas que hagan una cosa y la hagan bien*»: en el ejemplo `ls`
  realiza la tarea de listar el directorio y `wc` de contar el número de
  caracteres y palabras. No realizan tareas adicionales, pero las suyas las
  realizan correctamente.
- «*Escribe programas para trabajar juntos*»: `wc` está diseñado para obtener
  su entrada desde un archivo o desde una redirección, esto es, para colaborar
  con un programa que se la entregue.
- «*Escribe programas para manejar flujos de texto, porque esa es una interfaz
  universal*»: tanto `ls` como `wc` tratan con texto, la interfaz universal, es
  por eso que pueden interactuar sin problema y entendiéndose mutuamente.


#### Fundamento técnico

En Unix los archivos se gestionan mediante descriptores de archivo, simples
números que identifican cada archivo abierto por el programa en ejecución y
estos son los que permiten que este sistema de redirecciones funcione sin
ninguna fricción.

Cada programa lleva asignada su propia lista de descriptores de archivo
independiente de los otros programas. A medida que nuevos archivos se van
abriendo por el programa se van asignando nuevos descriptores, comenzando por
el número `0` e incrementando desde ahí de forma ordenada. Cerrar archivos
elimina el identificador del archivo (su descriptor), permitiendo así que se
use para otro archivo que se vaya a abrir más adelante.

Este sistema se explota junto con la filosofía «*todo es un archivo*» para
preparar el contexto de las redirecciones. En los sistemas Unix, los programas
comienzan con tres descriptores de archivo ya activos, significando esto que
parten con tres archivos ya abiertos. Estos archivos se conocen como *stdin*,
*stdout* y *stderr*[^std] y hacen referencia a la entrada estándar, salida
estándar y salida de errores y se abren en ese orden, asignando los
descriptores `0`,`1` y `2`, respectivamente. Ver `man stdin`.

Estos tres archivos forman parte del estándar POSIX y existen en el sistema de
archivos de la máquina, normalmente en `/dev/`, siendo enlaces a pseudoarchivos
de dispositivo de acceso por caracteres.

La implementación interna de los propios pseudoarchivos se escapa al alcance de
este documento, pero la implementación de las redirecciones es interesante.

Al realizar una redirección, ya sea una tubería o una redirección simple, la
*shell* cierra el descriptor de archivo correspondiente a lo que se desea
redireccionar y abre en su lugar el archivo al que realizar la redirección
justo antes de lanzar el programa. Para el programa este mecanismo es
totalmente transparente, puesto que lo que se encuentra es con los tres
primeros descriptores de archivo ya abiertos, como en una ejecución cualquiera.
La diferencia es que cuando trate de escribir en ellos, el resultado saldrá por
otro lugar.

Si se desea saber más, este sistema involucra varias peticiones al núcleo cuyo
funcionamiento se describe en el manual. Ver `man dup` y `man pipe`.

[^std]: *std* de *standard* (estándar) y *in* de *input* (entrada), *out* de
  *output* (salida) y *err* de *error* (error).

#### La salida de errores

La salida de errores es una salida especial, diseñada de modo que los programas
puedan exponer sus mensajes de error de una forma independiente a su salida.

Una peculiaridad adicional de la salida de errores es que, a diferencia de la
salida estándar, ésta no aplica buffering, por lo que su salida es más
inmediata.

Por lo demás, la salida de errores también puede redireccionarse usando «`2>`»,
o «`2>>`» si se desea añadir el resultado al archivo.

#### Redirección genérica

Igual que las entradas y salidas estándar son simples archivos, cualquiera de
los archivos de entrada o salida pueden redireccionarse. La sintaxis de las
redirecciones genéricas es la misma que las redirecciones de salidas estándar,
pero debe indicarse de qué descriptor de archivo se desea redireccionar, como
en el caso de la salida de errores, asignada al descriptor `2`, pero con
cualquier descriptor que se desee direccionar.

También es posible direccionar las salidas a otras salidas, de modo que varias
salidas se mezclen, para ello es necesario indicar no un nombre de archivo como
salida sino un descriptor de archivo al que se desea direccionar, usando el
símbolo et, «`&`» seguido del número del descriptor:

``` bash
$ chown root --recursive carpeta >/dev/null 2>&1
$
```

> NOTA: Las redirecciones se aplican en orden, en este caso aplicarlas en el
> orden opuesto no resultarían en lo mismo.

El comando del ejemplo descarta tanto la salida de errores como la estándar,
porque ambas se redireccionan al pseudoarchivo `null`, por lo que no muestra
nada en la pantalla, aunque el comando siga realizando su tarea. Esto es
especialmente útil para lanzar programas en segundo plano y que no molesten
expulsando su salida a la pantalla.

Del mismo modo que las redirecciones pueden alterar las salidas o las entradas
de un programa, también es posible preparar acceso a archivos para que estén
disponibles en un descriptor de archivo en particular para poder redireccionar
varios programas a éste más adelante, cosa que es difícil usando los nombres de
los archivos de forma individual. El comando interno `exec` permite realizar
este tipo de acciones, pero es raras veces es necesario llegar a este nivel de
complejidad:

``` bash
$ exec 5<>/dev/tcp/tilde.team/70
$ echo -e "~giacomo\r\n" >&5
$ cat <&5
```

> NOTA: Este ejemplo usa archivos de dispositivo especiales disponibles en
> Linux, que exponen puertos TCP y UDP para hacer una consulta mediante el
> protocolo Gopher.

En definitiva, el mecanismo de redirección de archivos es muy poderoso, pero
genérico, que está muy ligado a la implementación de «*todo es un archivo*».

#### Nota práctica: tuberías con nombre

Las tuberías con nombre introducidas en el apartado sobre el sistema de
archivos guardan relación con las tuberías sin nombre introducidas en este
apartado, la diferencia es que las tuberías sin nombre no necesitan de ningún
archivo, sino que toda la comunicación se trata en el núcleo, sin tener ninguna
referencia en el sistema de archivos.

El propio ejemplo previo:

``` bash
$ ls /var | wc
 9  9 41
```

Puede reescribirse en términos de una tubería con nombre:

``` bash
$ mkfifo tuberia
$ ls /var > tuberia &
[1] 10030
$ cat tuberia &
[2] 10044
cache
db
empty
guix
lib
lock
log
run
tmp
[1]-  Done                    ls --color=auto /var > tuberia
[2]+  Done                    cat tuberia
$ rm tuberia
```

> NOTA: Los mensajes de aviso de control de procesos que muestra la terminal en
> el ejemplo pueden descartarse mediante una redirección de la salida de
> errores.

Es interesante ver que si no se ejecutan los comandos en segundo plano
mediante «`&`», los comandos se pausan y la *shell* no retorna el *prompt*.
Esto es importante y también ocurre en las tuberías sin nombre.

Las tuberías requieren de los dos extremos, ambos se ejecutan en paralelo y se
coordinan para poder leer y escribir. No se escribe hasta que en el otro
extremo haya tratado de leer y se haya quedado a la espera.

Es interesante además darse cuenta que la *shell* realiza la tarea de lanzar
los extremos de las tuberías sin nombre de en paralelo de forma automática.


### Expansión de texto

La *shell* es capaz de expandir texto a partir de un patrón introducido por el
usuario, aunque este sistema forma parte de un mecanismo más avanzado que se
comprenderá en el apartado sobre programación en la *shell*, el uso mínimo de
este sistema de expansiones es extremadamente útil en el uso diario de la
terminal de comandos.

#### Expansión de nombre de archivo

En la expansión de nombre de archivo, la *shell* expande el contenido de
cualquier patrón reconocido por nombres de archivo en caso de que sea posible.
Por ejemplo:

``` bash
$ cd /var
$ ls
cache  db  empty  guix  lib  lock  log  run  tmp
$ echo *
cache db empty guix lib lock log run tmp
```

En el ejemplo se observa que visualizar «`*`» mediante `echo` se muestran los
archivos en el directorio actual. Esto ocurre porque la *shell* aplica el «`*`»
como un patrón y la contrasta con los archivos en la carpeta actual.

Del mismo modo pueden filtrarse los archivos, escribiendo parte del nombre:

``` bash
$ echo g*
guix
```

En este caso la *shell* considera, acorde a su sintaxis sobre patrones, que la
letra «`g`» es un literal y el «`*`» es una sección que se resuelve a cualquier
contenido posible. El único archivo que se ajusta a este patrón es `guix` así
que es el único que se aplica.

Por supuesto, los patrones aportan muchas más más capacidades que el uso del
asterisco como expansión pero éste es el más usado. Más adelante se tratan de
forma más concreta las expansiones de la *shell* y se introduce su sintaxis
completa.

Las expansiones de la *shell* se sustituyen en el sitio, por lo que los
comandos mostrados resuelven la expansión antes de llegar a ejecutarse. Esto
significa que en el ejemplo previo, `echo g*`, la expresión regular se expande
antes de ejecutar el comando interno `echo`, sustituyéndose por `echo guix`, lo
que visualiza `guix` en la pantalla. Este detalle es muy importante ya que
algunos comandos requieren de un número de argumentos concreto y al aplicar una
expansión es difícil prever cuántos se van a insertar, ya que depende del
resultado de la expansión. Más evidente de ver es con el primer ejemplo:

``` bash
$ echo *
cache db empty guix lib lock log run tmp
$ echo cache  db  empty  guix  lib  lock  log  run  tmp
cache db empty guix lib lock log run tmp
```

Ambas ejecuciones son equivalentes porque, tal y como se ha expuesto, la
resolución de la expresión regular ocurre primero. En este caso el comando
`echo` recibe nueve argumentos y no uno como podría esperarse al ver el comando
antes de expandirse.

Evidentemente los ejemplos mostrados son un poco absurdos, incluso inútiles en
el día a día, pero fácilmente pueden extrapolarse a comandos interesantes como
mover todos los archivos con la extensión `.mp3` a un directorio en particular:

``` bash
$ mkdir Music
$ mv *.mp3 Music
```

O buscar el uso de la palabra `printf` en todos los archivos de código fuente
C++ en un directorio:

``` bash
$ grep printf src/*.cpp
```

Conocer las expansiones de expresiones regulares es extremadamente poderoso
para ejecutar acciones que apliquen a multitud de archivos. Dominarlas aumenta
la productividad de quien las usa de una forma increíble, sobre todo combinadas
con las herramientas de filtrado de texto que se verán más adelante.

#### Expansión de la tilde

La *tilde expansion* o expansión de la tilde (o vírgula), «`~`», ya se
introdujo en capítulos previos únicamente para contar que se sustituye por el
directorio personal del usuario, pero la expansión es más potente que eso.

En primer lugar, la expansión resuelve a los directorios personales de
cualquiera de los usuarios del sistema combinando la vírgula seguida de el
nombre del usuario:

``` bash
$ echo ~root
/root
```

Además de este mecanismo, las *shell*s combinan la expansión de la tilde con
los comandos internos `pushd` y `popd`, que permiten guardar una lista de
directorios visitados previamente. Para entender este sistema lo mejor es
visitar la documentación, ya sea mediante `help pushd`, `help popd`, `help
dirs` o la documentación de la propia *shell* (por ejemplo `man bash`).

### Configuraciones

Las *shell*s pueden normalmente configurarse a través de varios archivos. Cada
*shell* tiene los suyos propios por lo que comentarlos todos es complicado.

Bash, por ejemplo, utiliza los archivos `.bashrc` y `.bash_profile` (junto a
varios otros) en el directorio personal del usuario para configurarse a su
gusto, aunque también usa otros archivos de configuración a nivel de sistema
(`/etc/profile`) para describir su funcionamiento global.

Los archivos de configuración de la *shell* son archivos que se ejecutan por
ésta, como si alguien los hubiera introducido al iniciar la sesión o
identificarse en el sistema. En el próximo capítulo se trata la *shell* como un
entorno de programación y se estudia este comportamiento en detalle.

De momento basta con saber que los archivos de configuración se la *shell*
principalmente realizan dos tareas: asignar variables de entorno y aliases. A
pesar de que en realidad podrían ejecutar cualquier comando.

#### Variables

Las variables de entorno ya se introdujeron en apartados previos, pero no en
demasiado detalle. Muchos programas dependen de las variables de entorno para
funcionar o esperan encontrar ciertas variables para configurarse.

Tal y como ya se introdujo en el pasado, varias variables como `EDITOR`,
`PAGER`, `PATH`, etc. El mejor lugar para activarlas o alterarlas son estos
archivos de configuración.

#### Aliases

Los aliases son un mecanismo de personalización muy poderoso que permite al
usuario simplificar ejecuciones a comandos sustituyéndolas por versiones más
sencillas de escribir. Para configurar nuevos aliases se usa el comando interno
`alias`, que también sirve para mostrar los alias activos.

Por ejemplo, muchas distribuciones de Linux añaden este alias, junto a otros
similares, por defecto:

``` bash
$ alias ls='ls --color=auto'
```

De esta manera, cuando el usuario ejecuta `ls` su comando se sustituye por la
versión con colores activados. Si el usuario añadiera argumentos adicionales,
éstos se añadirían al final del comando y todo funcionaría a la perfección:

```
$ ls ~
```

Se expande de la siguiente manera:

```
$ ls --color=auto ~
```

Y así se evita tener que escribir `--color=auto` en cada ejecución para obtener
una salida con colores que facilite la lectura.

Con el tiempo, cualquiera que use un sistema Unix acabará creando nuevos
aliases que se ajustan a su forma de trabajo. Uno muy habitual entre
administradores de sistema que necesitan conectarse a máquinas mediante el
protocolo SSH es el siguiente:

``` bash
$ alias r='ssh -l root'
```

Los alias, al igual que las variables, se pierden al finalizar la sesión
actual, es por eso que suelen almacenarse en el archivo de aliases,
`.bash_aliases` en Bash, que se carga automáticamente al iniciar la sesión.
Realmente, los aliases son ejecuciones de comandos de la *shell* cualquiera,
así que también pueden incluirse en otros archivos de configuración. La
existencia de un archivo a parte sirve únicamente para facilitar la
modularidad[^bash_aliases].

[^bash_aliases]: En realidad se puede incluso incluir otro tipo de
  configuración en el archivo de aliases, pero sería un poco absurdo hacerlo,
  ya que iría en contra de su propósito principal.


## Herramientas útiles

Una *shell* no es poderosa si no está apoyada por un conjunto de herramientas.
Además, las capacidades de la *shell* dejan de tener sentido si estas
herramientas no colaboran con ella.

### Filtrado de texto

Los programas de filtrado de texto tienen todo el sentido en un entorno
diseñado pensando en que «*los flujos de texto son la interfaz universal*».
Los flujos de texto se refieren a los datos que fluyen a través de las
tuberías descritas previamente.

Muchos programas básicos de Unix son capaces de alterar flujos (*streams*) de
texto. Esto incluye programas de conteo de líneas y palabras como `wc` o
incluso lenguajes de programación orientados al procesamiento de texto como
`awk`, pero, en definitiva, las herramientas de filtrado de texto están
diseñadas para obtener su entrada desde la entrada estándar y volcar su
resultado en la salida estándar, de modo que se puedan combinar en tuberías más
largas.

El uso de estas herramientas cambia mucho en función de las tareas que deben
realizarse pero se recomienda conocer unos mínimos de varias herramientas
generales que pueden facilitar el trabajo general en Unix.

La más interesante quizás sea `grep`, una herramienta de filtrado de texto
capaz de aplicar expresiones regulares compatibles con POSIX sobre *streams* de
texto. Las expresiones regulares compatibles con POSIX son similares, pero más
complejas que los patrones de la *shell*: ver `man 7 regex`.

Un ejemplo sencillo del uso de `grep` puede ser el de buscar textos en logs de
mensajes de un programa. El siguiente ejemplo, ejecutado en dos etapas, busca
todas las líneas del archivo `/var/log/messages` que incluyen «`ntp`» y filtra
de nuevo ese resultado para encontrar únicamente las líneas que incluyen el
texto «`Soliciting`», de modo que se obtengan las peticiones de solicitud
`ntp`.

``` bash
$ cat /var/log/messages | grep ntp | grep Soliciting
Sep 17 11:28:03 localhost ntpd[418]: Soliciting pool server [IP]
Sep 17 11:28:03 localhost ntpd[418]: Soliciting pool server [IP]
Sep 17 11:29:09 localhost ntpd[418]: Soliciting pool server [IP]
Sep 17 11:30:14 localhost ntpd[418]: Soliciting pool server [IP]
Sep 17 11:31:21 localhost ntpd[418]: Soliciting pool server [IP]
Sep 17 11:32:27 localhost ntpd[418]: Soliciting pool server [IP]
Sep 17 11:32:28 localhost ntpd[418]: Soliciting pool server [IP]
Sep 17 11:32:29 localhost ntpd[418]: Soliciting pool server [IP]
Sep 17 11:32:30 localhost ntpd[418]: Soliciting pool server [IP]
Sep 17 11:33:39 localhost ntpd[418]: Soliciting pool server [IP]
Sep 17 11:33:40 localhost ntpd[418]: Soliciting pool server [IP]
```

> NOTA: Este caso particular puede combinar los dos primeros pasos en una única
> ejecución de `grep` usando el argumento opcional de `grep` para aportar un
> archivo de entrada. Pero se mantiene así para demostrar el interés de las
> tuberías.

De forma similar a `grep`, `sed` es capaz de atacar flujos de texto pero en
este caso para aplicar alteraciones. Por ejemplo, en el resultado anterior, con
el objetivo de escribir el resultado en este documento se han eliminado las
direcciones IP sustituyéndolas por el texto `[IP]`[^ip]. Esto se ha realizado
mediante una ejecución de `sed` similar al siguiente:

[^ip]: Las direcciones IP (en la versión 4) están formadas por cuatro números
  del 0 al 255 separados por un punto. Por ejemplo: `1.2.3.4`

``` bash
$ cat /var/log/messages | grep ntp | grep Soliciting | sed -e 's/[0-9.]*$/[IP]/g'
```

Del mismo modo que se pueden usar para eliminar las direcciones IP, puede
usarse el mismo mecanismo para aislar los mensajes y únicamente mantener las
direcciones IP para un procesado posterior.

Para mensajes como estos, basados en columnas, el mejor aliado es `awk`, un
lenguaje de programación estandarizado en POSIX diseñado para el filtrado de
texto, en el siguiente ejemplo se usa `awk` para extraer la última columna (las
direcciones IP) para después enviarlas a `ping` para hacer una prueba de
conectividad.

> NOTA: Como en el ejemplo previo, aquí también se han sustituido las
> direcciones IP por el texto «`[IP]`».

``` bash
$ cat /var/log/messages | grep ntp | grep Soliciting | awk '{print $9}' | xargs ping -c 2
PING [IP] ([IP]): 56 data bytes
64 bytes from [IP]: icmp_seq=0 ttl=58 time=11.977 ms
64 bytes from [IP]: icmp_seq=1 ttl=58 time=11.214 ms
--- [IP] ping statistics ---
2 packets transmitted, 2 packets received, 0% packet loss
round-trip min/avg/max/stddev = 11.214/11.596/11.977/0.382 ms
PING [IP] ([IP]): 56 data bytes
64 bytes from [IP]: icmp_seq=0 ttl=56 time=16.347 ms
64 bytes from [IP]: icmp_seq=1 ttl=56 time=18.218 ms
...
```

Del mismo modo que `awk` es interesante para la extracción de las columnas,
`xargs`, mostrado también en el ejemplo, es muy interesante para la gestión de
argumentos. El objetivo de `xargs` es obtener líneas de texto e introducirlas
al comando indicado a modo de argumentos. En el ejemplo introduce una lista de
de direcciones IP a `ping`, según las recoge de la entrada estándar, pero
convertidas a los argumentos de entrada de modo que el `ping` final que termina
ejecutándose es el siguiente, pero usando la lista de direcciones IP
proporcionada por el comando previo:

``` bash
$ ping -c 2 [IP] [IP] [IP] [IP]
```

No es interesante para los objetivos de este documento profundizar demasiado en
estas herramientas sino enseñar lo poderosas que pueden resultar combinadas
correctamente. Para dominarlas debe consultarse el manual de cada una de
las herramientas y de forma paulatina ir incluyéndolas en el repertorio de cada
una.

### Paginadores

En algunos casos la salida de un comando puede ser demasiado larga y llenar la
pantalla de texto, lo que complica su visualización. Comandos como `head` y
`tail` pueden ayudar a visualizarla ya que son capaces de eliminar todo el
texto excepto la cabeza (*head*) o la cola (*tail*), pero no permiten
visualizar todo el contenido de una forma amigable.

Para esta tarea se crearon los comandos de paginación como `more` y `less`,
siendo el segundo más moderno y añadiendo muchas funcionalidades sobre el
primero.

No es necesario hablar demasiado de estos comandos de paginación más allá de
definir su utilidad debido a que cualquiera que haya seguido este documento y
haya tratado de consultar la documentación usando el comando `man` ya se los ha
encontrado. `man` visualiza la documentación mediante el paginador por defecto
que se encuentra en la variable de entorno `PAGER`.

Los paginadores pueden usarse también de forma aislada para abrir archivos de
texto o como última etapa en una tubería para visualizar el resultado de una
forma navegable sencilla.

Los paginadores mencionados habilitan combinaciones de teclas para
funcionalidades avanzadas (búsqueda, navegación...) que pueden consultarse
pulsando `h`.

### Edición de texto

Siendo el texto «*la interfaz universal*» y estando presente en todas partes a
través del sistema ya sea en archivos de configuración como en el resultado y
la entrada de los comandos, es lógico que Unix disponga de unos programas de
edición de texto especialmente potentes.

Los primeros editores de texto en Unix eran editores de línea. Esto se debe a
que estos sistemas no disponían de una gestión de gráficos suficiente para
visualizar varias líneas de forma dinámica e incluso en muchos casos su salida
no era una pantalla, sino rollos de papel que se iban imprimiendo con el
resultado de los comandos.

Desde que eso dejó de ser así, varios editores de texto visuales se
convirtieron en los estándares de-facto en los sistemas similares a Unix.

**Vi** fue uno de los primeros, nacido como interfaz visual para el editor de
línea `ex`, **Vi** es un editor modal muy potente del que nació años más tarde
un hermano mayor más avanzado conocido como **Vim**, más recomendado si el uso
que se hace del editor es muy avanzado.

**Emacs** nació de un modo similar como editor de macros del editor y lenguaje
de programación TECO hoy en día es uno de los editores de texto más potentes y
complejos disponible.

Actualmente también es muy frecuente el editor **nano** y es el editor por
defecto en muchas distribuciones a pesar de ser mucho más simple que los
anteriores o quizás por el hecho de serlo.

**Vi** y **Emacs** tienen en común que ambos han sido tan populares que han
creado familias completas de editores y han llegado a un nivel de influencia
tal que, como se estudia al principio de este capítulo, bibliotecas como
Readline tratan de emularlos. Sin embargo, como editores son muy
diferentes[^vi-emacs].

[^vi-emacs]: También tienen en común ser los dos contendientes de una guerra de
  editores de texto que lleva separando a los usuarios de Unix desde el
  principio de los tiempos.

**Emacs** y **nano** son más similares en lo que se refiere al trato con un
usuario nuevo. Ambos editores escriben en el texto al pulsar teclas y ambos se
basan en combinaciones de teclas con modificadores (`Ctrl`, `Alt`...) para
realizar acciones como guardar el archivo o cerrar el editor.

**Vi**, sin embargo, es un editor modal. Esto quiere decir que la interacción
con él se interpreta de forma distinta en función del modo en el que el editor
se encuentre. El editor parte de lo que se conoce como modo *normal* que
permite realizar comandos. Pulsar la tecla `i` en el modo normal lleva al
editor al modo *edición* en el que todo lo que se escriba se inserta en el
archivo. Pulsar la tecla `ESC` en el modo *edición* devuelve al editor al modo
normal. El modo normal, lejos de ser un simple puente al modo edición, es donde
pueden ejecutarse los potentes comandos que el editor aporta.

**Emacs** y **Vi** disponen de una documentación excelente y muchísimos
recursos online para aprender a utilizarlos, ya que la curva de aprendizaje
puede ser un poco empinada al principio. **nano** sin embargo, es mucho más
sencillo y la barra inferior del editor ya muestra las acciones más habituales
que se pueden efectuar en el mismo.

La selección del editor dependerá mucho de los gustos y afinidades de cada
persona, pero es necesario recordar que en caso de tratar con servidores u
ordenadores embebidos es posible que un editor muy avanzado no esté disponible
y sea necesario usar **Vi** o **nano**, ya que son los editores por defecto en
muchas distribuciones. Conocer lo mínimo de la edición modal y saber al menos
editar un archivo y guardarlo tanto en **nano** como en **Vi** es primordial si
se va a trabajar en los entornos mencionados.

Para el uso diario en el escritorio se recomiendan **Emacs** o **Vim**, un
**Vi** mejorado, por la extensibilidad que aportan y por su potencia, aunque
existen muchos otros editores, sobre todo gráficos, muy poderosos e
interesantes.
