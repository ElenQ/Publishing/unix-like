# El sistema de archivos

El sistema de archivos es parte fundamental ya que «*todo es un archivo*», es
por eso que conocerlo en detalle facilita mucho la tarea de gestión del
sistema.

## Generalidades

Los sistemas tipo Unix utilizan el símbolo «`/`» como separador entre
subdirectorios y parten de una única raíz, llamada «`/`». Cuando se insertan
nuevos dispositivos de almacenamiento, éstos se «montan» (ver `man mount`)
haciendo que su estructura de archivos aparezca en la carpeta deseada.

> Metáfora: Si el sistema de archivos es un árbol, un `mount` es un injerto.

El sistema de «montado» es extremadamente importante, ya que no sólo sirve para
acceder dispositivos de almacenamiento, sino que también es capaz de aportar
acceso a secciones de la memoria a través del sistema de archivos. Este
mecanismo se utiliza para exponer información sobre el núcleo de forma
fácilmente accesible mediante herramientas de lectura y escritura habituales.
Ya sabes, «*todo es un archivo*».

### Rutas

Los archivos de un sistema se identifican mediante su ruta («*path*» en
inglés). Las rutas se describen como *absolutas* cuando comienzan por la
carpeta raíz, indicando todos los subdirectorios que han de seguirse hasta
llegar a al archivo; y como *relativas* cuando se parte de la carpeta *actual*.

`/var/log/syslog` indica la ruta *absoluta* del archivo `syslog`, al que se
accede a través de la carpeta `log` en la carpeta `var` que se encuentra en la
carpeta raíz «`/`».

Sin embargo, si el programa actual se ejecutase en la carpeta `var`, podría
señalarse al mismo archivo mediante la ruta *relativa*: `log/syslog`.

La diferencia entre las rutas radica en la primera «`/`», si se incluye,
significa que debe comenzarse a contar desde la carpeta raíz. En caso de no
incluirse, el sistema buscará desde la carpeta actual, almacenada en la
variable de entorno `PWD` (ver `man pwd` y `help cd`).

En algunos casos es necesario indicar la carpeta superior a la actual o
especificar que uno se refiere a la carpeta actual. Para ello se usan los
símbolos «`.`» para la carpeta actual y «`..`» para la carpeta superior.

``` bash
$ pwd
/var/log
$ ls syslog
syslog
$ cd www
$ pwd
/var/www
$ ls ../log/syslog
../log/syslog
```

El símbolo para indicar la carpeta actual es más peculiar, puesto que las rutas
relativas parten automáticamente de la carpeta actual, pero existe un caso en
el que es importante usarlo: eliminar ambigüedades de la llamada a comandos en
la *shell*. Suponiendo que se dispone de un programa ejecutable en la carpeta
actual llamado `mi_programa`:

``` bash
$ mi_programa
-bash: mi_programa: command not found
$ ./mi_programa
- Ejecución correcta de mi_programa
```

El ejemplo muestra como en una primera ejecución sin indicar la carpeta actual
la *shell* lanza un error de «Comando no encontrado». Esto se debe a que la
*shell* automáticamente busca el programa en el `PATH` (ver `man bash` o el
manual de la shell de sistema).  Al indicar «`./`» al inicio del comando la
*shell* comprende que el programa no se encuentra en el `PATH` sino en el
directorio actual y así lo encuentra y ejecuta correctamente.

### Archivos ocultos

En ocasiones interesa ocultar los archivos de la simple vista, normalmente
cuando se trata de archivos de configuración que deben protegerse mínimamente o
se trata de archivos internos de los programas que no están diseñados para la
edición manual. En los sistemas tipo Unix estos archivos se prefijan con el
símbolo «`.`».

Esto no significa que los archivos no puedan verse, sino que debe activarse un
modo especial para poder visualizarlos:

``` bash
$ mkdir carpeta_de_prueba
$ cd carpeta_de_prueba
$ touch .archivo_oculto
$ touch archivo_visible
$ ls
archivo_visible
$ ls -a
.  ..  .archivo_oculto  archivo_visible
```

Como puede observarse, la carpeta actual y la carpeta superior también son
archivos ocultos, ya que empiezan por «`.`». Ver `man ls`.

### Extensiones

En Unix las extensiones no son necesarias para el correcto funcionamiento del
sistema aunque se utilizan con el fin de simplificar la detección del tipo de
archivo o para mejorar la legibilidad de la salida de las herramientas.

Como no son necesarias, las extensiones no imponen ningún tipo de restricción
en longitud ni en forma, aunque lo normal es utilizar «`.`» para delimitarlas.

De todos modos, hoy en día las extensiones aparecen en infinidad de ocasiones
ya sea por convención o por necesidad así que es necesario conocer de su
existencia.

### Detección del tipo de archivo

En lugar de usar las extensiones como mecanismo, en Unix se analizan los
archivos en busca de información que revele su tipo. El comando `file` es una
muestra de este funcionamiento, analizando los formatos y *números mágicos* de
los archivos es capaz de conocer su tipo. Además, posibilita la creación de
*archivos mágicos* que permiten extender el funcionamiento del comando para
que éste funcione en nuevos formatos. Para saber más ver `man file`.

### La jerarquía

El sistema de archivos es parte fundamental de Unix desde sus primeras
versiones ya que es la parte que habilita muchos de los comportamientos que
hacen de Unix un sistema operativo tan singular. Exponer todas las interfaces
posibles al sistema de archivos implica tener un sistema poderoso, con una
jerarquía muy bien definida que permita seleccionar correctamente cada
interacción, sin lugar a ambigüedades.

Desde el establecimiento de Unix como sistema operativo de referencia, ha
habido muchos intentos de estandarizar y documentar su jerarquía de archivos.
El más exitoso se produjo poco después de la aparición de Linux, y se denominó
FHS («*File Hierarchy Standard*»). Este estándar se mantiene por la Linux
Foundation y, aunque opcional, es muy común.

A pesar de ser opcional, casi todos los sistemas similares a Unix usan una
estructura similar de una forma u otra o son compatibles con ella, debido a que
este estándar no sólo es lo que la mayor parte de herramientas esperan
encontrarse sino que recoge muchas prácticas que llevan existiendo desde los
principios de Unix.

A continuación un resumen de las diferentes partes principales de la jerarquía
FHS.  Para saber más: `man hier`.

- `/`: Directorio raíz, donde toda la estructura comienza.
- `/bin`: Contiene ejecutables del sistema para su mantenimiento y arranque.
- `/sbin`: Similar a `/bin`, contiene programas ejecutables pero estos no
  suelen ejecutarse por usuarios, sino por el sistema.
- `/lib`: Contiene bibliotecas dinámicas necesarias para la ejecución del
  sistema.
- `/boot`: Contiene archivos estáticos para el proceso de arranque. El `initrd`
  suele incluirse en este directorio (ver apartado sobre proceso de arranque) o
  en `/`.
- `/etc`: Contiene los archivos de configuración de la máquina.
- `/dev`: Recoge todos los archivos de dispositivo o archivos especiales:
  terminales, acceso en crudo a dispositivos de almacenamiento, botones de
  encendido/apagado, archivo de ceros `/dev/zero`, archivos de generación de
  números aleatorios `/dev/random` y `/dev/urandom`, archivo vacío `/dev/null`,
  etc.
- `/proc`: Punto de montado del sistema `proc`, que contiene información de los
  procesos en marcha en el sistema. Se trata de un sistema de archivos mapeado
  a memoria.
- `/sys`: Punto de montado del sistema `sysfs`, similar a `proc`, que aporta
  información sobre el núcleo, pero de manera mejor estructurada.
- `/var`: Contiene archivos que cambian de tamaño. Su contenido más relevante
  son los logs (`/var/log/`), archivos donde se almacena el historial de
  acontecimientos de los diferentes programas del sistema. Aunque también
  contiene el directorio por defecto de los servidores web (`/var/www`), entre
  otras cosas.
- `/tmp`: Almacena archivos que pueden eliminarse sin previo aviso como
  archivos temporales de aplicaciones o similar. Normalmente se vacía esta
  carpeta de forma automática al apagar el dispositivo.
- `/usr`: Pensada para que pueda montarse desde una partición independiente,
  esta carpeta incluye programas (`/usr/bin`), configuraciones (`/usr/etc`),
  bibliotecas (`/usr/lib`), cabeceras (`/usr/include`), documentación
  (`/usr/doc`), etc. Sus principales condiciones son que su contenido no se
  altere y que se exponga como sólo lectura para la mayoría de los programas,
  de este modo puede montarse desde el mismo dispositivo en varias máquinas
  para que éstas compartan su configuración. En la práctica, principalmente
  contiene todo lo relacionado con aplicaciones no esenciales.
- `/media`: En esta carpeta se montan los dispositivos extraíbles como lectoras
  de discos.
- `/mnt`: Esta carpeta se usa como punto de montaje temporal.
- `/home`: En sistemas con múltiples usuarios, las carpetas personales de cada
  uno de ellos se almacenan aquí.
- `/root`: En lugar de usar la carpeta `/home`, el usuario administrador,
  `root`, usa la carpeta `/root`.


### Nota práctica: Programas útiles

Además de los programas estudiados hasta ahora, el sistema aporta algunos
programas más que es interesante añadir al repertorio:

- `dirname` y `basename`: comandos útiles a la hora de tratar nombres de
  archivos programáticamente, aunque parte de su funcionalidad puede realizarse
  mediante las *Shell Expansions* que se verán más adelante.
- `find`: potentísimo comando que permite buscar en el sistema de archivos,
  pero que soporta gran cantidad de filtros y funcionalidades adicionales.
- `df`: reporta información sobre el uso del disco duro así como información
  sobre los puntos de montado.
- `du`: estima el espacio en disco utilizado por archivos y carpetas de forma
  recursiva.
- `mount`: «monta» sistemas de archivos en el sistema tal y como se ha descrito
  previamente. Ejecutar el comando sin argumentos muestra los puntos de montado
  actuales. `umount` es la operación inversa a `mount`.

## Detalle interno: inodos

Los sistemas de archivos tipo Unix almacenan metadatos con los archivos usando
unas estructuras de datos que se conocen como *inodo* («*inode*» en inglés). El
sistema de archivos completo se basa en este concepto de *inodos* y el estándar
POSIX los contempla. Sin entrar demasiado en detalle, los *inodos* almacenan
casi toda la información que se tratará a continuación: usuarios, grupos, modo
del archivo, número de enlaces, tamaño, etc.

El sistema almacena una lista de las rutas de todas las rutas a archivos y en
lugar de hacer referencia directa al contenido de los archivos, se señala al
*inodo* correspondiente a ese archivo, que es el que después referencia al
contenido real en caso de que desee extraerse.

Toda esta información puede verse desde una perspectiva abstracta que no
requiere conocer de la estructura de los *inodos*, es por eso que no se tratan
en detalle en este apartado. Lo que sí que es importante es conocer que existen
y que pueden agotarse debido a cómo están definidos. Sistemas con demasiados
archivos muy pequeños pueden agotar los *inodos*, lo que imposibilita la
creación de nuevos archivos aunque pueda haber espacio de almacenamiento
disponible. Algunos sistemas de archivos superan esta dificultad, pero no todos
lo hacen.

## Enlaces

Los archivos pueden enlazarse de forma que varias rutas accedan en realidad al
mismo archivo. Los sistemas similares a Unix proporcionan dos tipos de enlaces
que en inglés se conocen como *soft link* (se traduce como *enlace simbólico*
en lugar de la traducción literal «*enlace blando*») y *hard link* (*enlace
duro*). Las diferencias entre los dos tipos de enlaces radican en el uso
interno que se hace de los *inodos*.

Los **enlaces duros** crean la ruta del archivo pero hacen referencia al
*inodo* ya existente. De esta manera se accede al mismo archivo desde ambas
rutas. De igual modo, se comparten también los metadatos del archivo. Un
*enlace duro* no permite conocer cuál de los dos puntos de entrada fue creado
primero y ambos serán equivalentes.

Los **enlaces simbólicos** construyen un nuevo *inodo* y es éste el que señala
al contenido del archivo original. De este modo, el nuevo *inodo* puede
almacenar diferentes metadatos a los del archivo enlazado.

Dicho de otra forma, los enlaces simbólicos son archivos especiales que señalan
a otro archivo, mientras que los enlaces duros son una referencia adicional a
un archivo que existía previamente.

Los enlaces tanto duros como simbólicos se crean con el comando `ln`. Ver `man
ln`.


### Borrado de archivos y enlaces duros

Los *inodos* mantienen entre sus datos un número que indica la cantidad de
referencias a ese *inodo*. Para eliminar archivos, el sistema elimina la pareja
ruta-inodo y reduce el conteo de referencias del *inodo*. Si el número llega a
ser cero, el *inodo* se descarta y el contenido al que el *inodo* apuntaba se
considera espacio libre en el sistema.

Este mecanismo tiene relación con los *enlaces duros* ya que estos son los
únicos que incrementan el número de referencias a un *inodo*. En caso de un
archivo con un *enlace duro*, eliminar tanto la ruta original como la enlazada
no elimina el archivo, sólo reduce la cantidad de referencias. Para eliminar el
contenido, es necesario eliminar todos los puntos de acceso al archivo.

### Borrado de archivos y enlaces simbólicos

El caso de los enlaces simbólicos es diferente ya que no se usan las
referencias al *inodo* para realizar el enlace. Borrar el archivo enlazado
mantendrá al enlace en el sistema, pero éste no podrá acceder al contenido
original, así que quedará roto. Herramientas como `ls` son capaces de reportar
enlaces rotos.

## El sistema de permisos

El sistema de archivos dispone de un sistema de permisos muy potente que
permite o prohíbe la lectura, escritura o ejecución de los archivos en función
del usuario que lo intente. Como ya se ha adelantado, esta información se
almacena en el *inodo* del archivo, junto con el resto de metadatos.

### Permisos de acceso

El permiso de acceso define cómo se puede interactuar con el archivo,
normalmente está definido con las letras `rwx`, de *read*, *write* y *execute*,
e indica si el archivo puede leerse, escribirse y/o ejecutarse,
respectivamente.

En los directorios, los permisos se exponen de la misma manera que en un
archivo común, pero no representan lo mismo, ya que algunos de estos permisos
no tienen sentido para el caso de los directorios (por ejemplo, los directorios
no pueden ejecutarse). El hecho de que se representen del mismo modo se debe a
que se usan los mismos campos del *inodo* para representar la información y es
más cómodo llamarlos de la misma manera. En un directorio, el permiso de
lectura permite listar sus contenidos, el permiso de escritura permite crear y
eliminar sus contenidos o el propio directorio y el permiso de ejecución
permite saltar al directorio (mediante un `cd`).

Tratar de realizar una de las acciones en un archivo que no lo permita supone
un fallo y la acción no se lleva a cabo.

A la hora de representar los permisos se suelen usar las propias letras `rwx`,
indicando con un guión las que no estén activas. Por ejemplo, `rw-`
significaría que el archivo puede leerse y escribirse, pero no ejecutarse.

A nivel interno, estos permisos se definen mediante tres bits, es por eso que
en ocasiones se expresan como un número en el sistema octal. Por ejemplo, `7`
(`111` en binario), activa los tres bits, permitiendo las tres acciones; el
número `5` (`101` en binario), activa únicamente las posiciones `r` y `x`,
deshabilitando así la escritura.

### Usuario y grupo

Como Unix es un sistema multiusuario, es necesario algo más potente para poder
hacer una gestión de permisos que sea capaz de separar contextos haciendo que
los usuarios sólo tengan acceso a sus propios archivos o a los archivos que se
les quiera mostrar.

Para conseguir esto, todos los archivos del sistema incluyen dos campos
adicionales en sus *inodos*: el usuario dueño del archivo y el grupo al que el
archivo pertenece.

A nivel interno, estos campos se asignan mediante identificadores numéricos
conocidos como `uid` (*user identifier* o identificador de usuario) y `gid`
(*group identifier* o identificador de grupo), pero hacen referencia a los
usuarios y grupos que ya fueron introducidos en el bloque anterior.

Añadir esta información permite una gestión de permisos mucho más granular,
definiendo los permisos en tres niveles: los permisos del usuario dueño del
archivo, los de los usuarios en el grupo del archivo y los permisos del resto
de usuarios.

Para hacer esto sólo es necesario indicar los permisos `rwx` introducidos en el
apartado anterior en tres ocasiones, la primera representando al usuario dueño
del archivo, la segunda a los usuarios del grupo y la tercera al resto. A este
sistema se le conoce como `UGO`, que viene de las siglas de «*User, Group and
Others*» (usuario, grupo y otros).

Entonces, un archivo cuyo dueño sea `Ekaitz` y que pertenezca al grupo `redes`
y disponga de los permisos `rwxrw-r--` indica que el usuario `Ekaitz` puede
leer, escribir y ejecutar el archivo, los usuarios del grupo `redes` sólo
pueden leerlo y escribirlo y el resto de usuarios únicamente pueden leer sus
contenidos.

Este mismo archivo, representado de forma octal no sería más que una
concatenación de los permisos para usuario, grupo y otros (interesante
recordarlo a través de las siglas `UGO`[^ugo]): `764`.

[^ugo]: Una buena regla mnemotécnica es usar el nombre Hugo.

### Gestión de permisos

Los permisos, por supuesto, pueden cambiarse, pero también es posible cambiar
el dueño o el grupo al que los archivos pertenecen. Unix proporciona unos
comandos para realizar estas tareas, todos comienzan por `ch` (del inglés
*change*, cambiar) y terminan por tres letras que indican qué se desea alterar:
`mod` para el modo (*mode*), que incluye los permisos aunque también unos
conceptos más que se verán a continuación, `grp` para el grupo (*group*) y
`own` para el dueño (*owner*). Ver `man chmod`, `man chgrp` y `man chown`.

### Permisos por defecto

Los permisos por defecto de los archivos creados mediante la *shell* se pueden
definir mediante el comando interno `umask`. Una vez activado el comando
`umask`, siempre que se cree un nuevo archivo o directorio en la sesión actual,
se le aplicará la máscara establecida automáticamente. Esta máscara actúa como
máscara binaria sobre el sistema de permisos, desactivando los bits que se
hayan indicado en el comando `umask`. Ver `help umask`.

## Modo de acceso

El funcionamiento estándar de cualquier archivo corriente («*regular file*»)
debe ser el de aportar acceso a una sección del sistema de almacenamiento de
donde se obtiene su contenido.

Pero no todos los archivos de un sistema se acceden del mismo modo.

Por un lado, el sistema de permisos descrito en el apartado anterior puede
limitar el funcionamiento básico de cómo deben accederse los archivos, por
ejemplo, impidiendo completamente el acceso a su contenido.

Por otro lado, ya se han visitado dos ejemplos de archivos que alteran el modo
de acceso por su propia naturaleza: los directorios y los enlaces simbólicos.
Los primeros únicamente hacen referencia a los archivos que contienen y los
segundos no exponen ningún contenido y en su lugar hacen referencia a otro
archivo.

En lugar de tratar estos conceptos como detalles independientes, los sistemas
tipo Unix los agrupan y almacenan en los *inodos* en un solo campo conocido
como modo (*mode* en inglés).

En la práctica suelen usarse 16 bits para representar el modo completo, siendo
los 9 últimos usados para indicar los permisos, tratados en el apartado
anterior, tal y como POSIX dicta.

Los 7 bits más significativos se utilizan para representar las peculiaridades
adicionales del acceso. Los 4 bits más significativos (dos cifras octales)
suelen usarse para representar el tipo del archivo, mientras que los 3
siguientes se usan para los bits `setuid` y `setgid`, que realizan un cambio de
identidad, y el bit `sticky` (pegajoso).

### Tipos de archivos

Los sistemas Unix modernos proporcionan varios tipos de archivos más que
afectan a cómo deben accederse.

> NOTA: Es importante no mezclar conceptos. En este punto no se trata de
> analizar el tipo de los archivos en función de su contenido, sino de su forma
> de acceso. Un archivo de música y uno de texto tienen contenidos en
> formatos distintos pero ambos son archivos corrientes y son las aplicaciones
> las encargadas de decidir cómo leerlos. Los archivos especiales, como los que
> se tratan en este apartado, son excepciones al sistema habitual de lectura y
> escritura.

#### Archivos comunes

Los tipos de archivo más comunes para el uso diario son los **archivos
corrientes** (*regular file*) que se acceden de forma normal. Su acceso,
edición y eliminación ya se ha visitado durante el documento y se seguirá
visitando en el futuro.

Los **directorios** (*directory*) también son archivos muy comunes. Se crean
mediante el comando `mkdir` y se eliminan mediante `rmdir`, aunque existen
otras opciones.

Los **enlaces simbólicos** (*soft link*) también son frecuentes en los
sistemas. Se crean mediante el comando `ln` y se eliminan como cualquier
archivo corriente.

#### Archivos para IPC

La necesidad de usar varios tipos de archivo nace de dos características
principales del sistema Unix combinadas: el hecho de que sea un sistema
multitarea y la filosofía «*todo es un archivo*».

Como sistema multitarea que es, Unix necesita de algún mecanismo para comunicar
las diferentes tareas en ejecución. El sistema de archivos resulta ser un
mecanismo ideal para la comunicación entre procesos o IPC (*Inter-Process
Communication*), porque permite a los programas comunicarse del mismo modo que
si estuvieran accediendo a un archivo corriente. Esto facilita la programación
y simplifica conceptos.

Las **tuberías con nombre** (*named pipe*) son uno de los dos modos de
comunicación entre procesos basada en archivos. Las tuberías actúan como una
cola FIFO: lo primero en escribirse en ellas será lo primero en leerse. De este
modo, un proceso puede escribir en la tubería mientras que otro proceso puede
leer de ella en el orden adecuado. Las tuberías se crean mediante los comandos
`mkfifo` o `mknod`. Su funcionamiento es muy similar al de las *tuberías sin
nombre* que se estudia junto a la redirección en la *shell*, la única
diferencia es que las *tuberías con nombre* se mantienen como un archivo en el
sistema y se operan usando su nombre, mientras que las *tuberías sin nombre*
son anónimas y no queda rastro de ellas una vez su uso termina.

Los **sockets de dominio Unix** (*Unix domain socket*) son otro tipo de archivo
orientado a la comunicación entre procesos. Estos archivos copian el
funcionamiento de un *socket* de red clásico (TCP, UDP, etc) pero la
comunicación ocurre dentro del núcleo, sin salir de la máquina. Al emular a los
*sockets* de red clásicos, las aplicaciones pueden aportar la posibilidad de
ejecutarse en un *socket de dominio Unix* o a través de la red sin la necesidad
de reescribir su sistema de comunicación completo, habilitando así la opción de
una comunicación segura siempre que las aplicaciones se ejecuten en la misma
máquina. Los *sockets de dominio Unix* se crean mediante llamadas a sistema en
las propias aplicaciones que realizan este tipo de comunicación y el sistema de
archivos actúa como identificador del *socket*.


#### Archivos de dispositivo

Los archivos de dispositivo son otro tipo de archivos especiales que aportan
acceso en crudo a dispositivos físicos o virtuales.

El ejemplo más fácil de comprender es el de los dispositivos de almacenamiento
masivo, como un disco duro o un *stick* USB. Éstos dispositivos se montan
(`mount`) en la carpeta seleccionada (`/media`, `/mnt` u otra), para aportar
acceso a su sistema de archivos, pero también es posible acceder a su contenido
en crudo (normalmente en la carpeta `/dev`). La diferencia entre los dos modos
de acceso es que en el primero el sistema de archivos está disponible, con sus
rutas, permisos y todo lo relacionado con éste, mientras que en el segundo los
datos se leen y escriben de forma literal sin analizar sus estructuras.

Los archivos de dispositivo se acceden como si de un archivo regular se tratara
cuyo contenido son los bytes o caracteres almacenados en el dispositivo. El
núcleo se encarga de gestionar que las operaciones de lectura y escritura que
se aplicarían a un archivo corriente actúen como deben en el dispositivo,
gestionando todas las características físicas o virtuales necesarias (rotación
de discos magnéticos, *buffering*, etc.)

Los archivos de dispositivo presentan dos modos de acceso posibles: el modo
**bloque** y el modo de **caracteres**. De cada dispositivo y sus
características dependerá la forma de acceso.

Los archivos de **caracteres** (*character*) aportan acceso directo, en crudo,
al dispositivo, sin *buffering*. Hoy en día se los conoce como archivos de
acceso en crudo (*raw*), debido a que el nombre clásico lleva a equívoco
haciendo pensar que deben accederse por caracter cuando no es así
necesariamente.

Los archivos de **bloque** deben poder accederse en bloques (*block*) de
cualquier tamaño y cualquier alineamiento. Estos dispositivos aplican
*buffering*, por lo que no es posible saber si el núcleo realmente ha
transferido la información desde el *buffer* al dispositivo físico. Ver `man
sync`.

Al tratar con dispositivos subyacentes, el núcleo asigna a cada archivo de
dispositivo una pareja de valores: el *major number* y el *minor number*. El
primero de ellos indica cuál es el *driver* de dispositivo que debe usarse como
método de acceso y el segundo identifica el dispositivo en particular que está
usando ese driver.

##### Archivos de dispositivo especiales

Los archivos de dispositivo no tienen por qué corresponder a un dispositivo
físico real en los sistemas tipo Unix. De hecho, el sistema suele aportar
varios archivos especiales que no acceden realmente a ningún dispositivo y cuyo
acceso es gestionado por el núcleo para aportar diferentes funcionalidades.

Este tipo de archivos suelen almacenarse en la carpeta `/dev`, siguiendo el
FHS, y agrupan diferentes funcionalidades que suelen ser útiles acompañadas de
la redirección de la shell, que se estudiará más adelante. Los más usados son
los siguientes:

- `/dev/null`: archivo acepta y descarta todo lo que se escriba en él y se
  comporta como un archivo vacío cuando se trata de leer de él.
- `/dev/zero`: se comporta como `/dev/null` cuando se escribe en él, pero en la
  lectura se comporta como un archivo infinito lleno de ceros (byte `0`).
- `/dev/random`: expone un generador de números aleatorios. Al leer de este
  archivo se entregan números aleatorios continuamente. En Linux, se entregan
  dos archivos de este tipo, `random` y `urandom`, que aportan funcionalidades
  distintas (ver `man urandom`).

Cada uno de estos dispositivos usa una pareja de  *major number* y *minor
number* concreta de modo que el núcleo sabe cómo deben comportarse.

##### Nota práctica: dispositivos de almacenamiento

Al insertar un nuevo dispositivo de almacenamiento, el sistema automáticamente
añade su acceso en crudo, ya que ésta es su forma de demostrar que el
dispositivo ha sido detectado y está disponible.

A no ser que el sistema esté configurado para hacerlo (ver `man fstab`), los
dispositivos no se montarán automáticamente, entre otros motivos, porque no
todos estarán diseñados para montarse.

Normalmente, los dispositivos físicos de almacenamiento se añaden a la carpeta
`/dev` siguiendo un sistema de identificación que depende de varios factores:
el driver utilizado, el número de dispositivo, etc. Por ejemplo: `/dev/sda` usa
las letras `sd` para indicar que se trata del driver de almacenamiento masivo y
la letra `a` para indicar que es el primer dispositivo detectado de este
tipo, en caso de insertar otro que use el mismo driver, se habilitaría en
`/dev/sdb` y así sucesivamente.

Los dispositivos de almacenamiento también exponen sus particiones
(*partition*) de forma individual indicadas mediante un número tras su nombre.
Por ejemplo: si el dispositivo `/dev/sda` dispone de dos particiones, éstas se
habilitan en `/dev/sda1` y `/dev/sda2`, de modo que puedan accederse de forma
individual.

El caso de uso quizás más frecuente de este tipo de dispositivos es el de
formatear discos. Para ello es necesario desmontar (`umount`) los dispositivos
en caso de que estén montados y trabajar directamente con el archivo de
dispositivo. Ver `man fdisk`, `man cfdisk` y `man mkfs`.

También es posible instalar archivos de imágenes ISO directamente al
almacenamiento de los dispositivos, una operación muy habitual a la hora de
crear discos o *sticks* USB de instalación de distribuciones de Linux, mediante
los archivos de dispositivo que el sistema expone. Ver `man dd` y `man cp`.

Los dispositivos de almacenamiento suelen tratarse como archivos de **bloque**,
que incluyen *buffering*, por lo que es necesario asegurarse de que las
transferencias han terminado antes de extraerlos (ver `man sync`).

##### Nota práctica: Reglas udev

Antiguamente, los archivos en el directorio `/dev` se exponían de forma
estática. Hoy en día existen diferentes sistemas de gestión de dispositivos que
gestionan el directorio `/dev` de forma dinámica, actualizándolo en función de
los dispositivos insertados en la máquina, y también se encargan de los
dispositivos que no necesariamente resultan en un archivo, como son las
conexiones de red.

**Linux** dispone de un sistema de gestión de dispositivos avanzado conocido
como `udev` (ver `man udev`), que no sólo gestiona la carpeta `/dev` sino que
monitoriza todos los dispositivos conectados a la computadora y opera en
consecuencia.

El sistema `udev` aporta configurabilidad mediante archivos de reglas (*rules
files*) donde pueden definirse infinidad de parámetros que describen las
acciones que el núcleo debe tomar al encontrar un dispositivo concreto al más
mínimo detalle.

Un uso muy habitual de este sistema es el de aportar a usuarios acceso a
archivos de dispositivo, ya sea para poder acceder a un puerto serie o para
poder gestionar la retroiluminación de su pantalla manualmente, ambas se
realizan mediante una regla sencilla que aplique un cambio de grupo al archivo
creado por el sistema.

### Bits de cambio de identidad

Los bits `setuid` (*set user id*) y `setgid` (*set group id*) permiten a
cualquier usuario del sistema ejecutar un archivo usando el grupo (`setgid`) o
usuario del archivo (`setuid`). Normalmente esto implica una elevación de los
privilegios del usuario ejecutando el archivo.

El ejemplo más evidente de este funcionamiento es el programa `ping`, que sirve
para comprobar si se dispone de conexión de red a otro dispositivo. Para poder
hacer una comprobación así, es necesario tener acceso a las interfaces de red,
un permiso bastante elevado. Como el propio comando `ping` no es dañino, ni
permite al usuario reconfigurar la red de ninguna de las maneras, se activa el
bit `setuid` para que pueda ejecutarse el archivo en modo administrador a pesar
de que el usuario no tenga permisos de administrador.

El funcionamiento del bit `setgid` es similar, pero haciendo uso del grupo en
lugar del dueño del archivo a ejecutar.

A la hora de mostrar el modo de un archivo, los bits `setuid` y `setgid` suelen
mostrarse mediante una `s` en el espacio para la `x` del campo `U` (usuario) y
`G` (grupo), respectivamente. Por ejemplo, `rwsr-xr--` indica que el archivo
tiene activado el bit `setuid`, mientras que `rwxr-sr--` indicaría que es el
bit `setgid` el que está activado[^setXid].

[^setXid]: A nivel mnemotécnico es fácil saber cuál de las posiciones se
  relaciona con qué bit, si la `s` está en la parte `U` de `UGO` significa que
  se trata de `setuid` (nótese la U) y si está en la parte `G` significa que se
  trata de `setgid` (nótese la G).

El funcionamiento de los cambios de identidad varía en función de dónde se
activen así que es necesario estudiarlo cuidadosamente antes de aplicarlo. Aun
así, ninguno de los dos cambios de identidad es muy frecuente por las
complicaciones que implican y los posibles fallos de seguridad que pueden
aportar.

### Bit pegajoso

El bit pegajoso o `sticky` tiene diferente uso en función de dónde se aplique.

Antiguamente podía usarse para mantener programas en la memoria y facilitar la
carga de los mismos en una segunda ejecución, aunque hoy en día esta
funcionalidad ha quedado en desuso.

Actualmente sólo tiene sentido aplicarlo en directorios, donde prohíbe que
usuarios que no son propietarios del archivo (a excepción de `root`) eliminen o
muevan el directorio marcado con el bit pegajoso.

De todos modos, el bit `sticky` no es demasiado frecuente hoy en día, aunque en
algunas distribuciones puede observarse en el directorio `/tmp`.

## Nota práctica: visualización de metadatos

Por supuesto, es posible visualizar todos estos metadatos de los *inodos* desde
la propia *shell* de comandos. El archivo más habitual para obtener información
de un archivo o carpeta es `ls`. Entre sus opciones avanzadas se encuentra la
opción `-l` que muestra los archivos ordenados por filas, con sus metadatos
descritos en columnas. La siguiente ejecución de `ls -l` muestra una lista muy
variada de metadatos (la salida ha sido resumida):

``` bash
$ ls -l /dev
total 0
crw-rw-rw-  1 root   root      1,   7 Aug 31 09:34 full
crw-r--r--  1 root   root      1,  11 Aug 31 09:34 kmsg
crw-rw----  1 root   kvm      10, 232 Aug 31 09:34 kvm
crw-------  1 root   root    246,   0 Aug 31 09:34 nvme0
brw-rw----  1 root   disk    259,   0 Aug 31 09:34 nvme0n1
brw-rw----  1 root   disk    259,   1 Aug 31 09:34 nvme0n1p1
brw-rw----  1 root   disk    259,   2 Aug 31 09:34 nvme0n1p2
brw-rw----  1 root   disk    259,   3 Aug 31 09:34 nvme0n1p3
crw-rw-rw-  1 root   root      1,   3 Aug 31 09:34 null
crw-rw-rw-  1 root   root      1,   8 Aug 31 09:34 random
brw-rw----  1 root   disk      8,   0 Aug 31 09:34 sda
brw-rw----  1 root   disk      8,   1 Aug 31 09:34 sda1
brw-rw----  1 root   disk      8,   2 Aug 31 09:34 sda2
brw-rw----  1 root   disk      8,   3 Aug 31 09:34 sda3
brw-rw----  1 root   disk      8,   4 Aug 31 09:34 sda4
lrwxrwxrwx  1 root   root          15 Aug 31 09:34 stderr -> /proc/self/fd/2
lrwxrwxrwx  1 root   root          15 Aug 31 09:34 stdin -> /proc/self/fd/0
lrwxrwxrwx  1 root   root          15 Aug 31 09:34 stdout -> /proc/self/fd/1
crw-rw-rw-  1 root   tty       5,   0 Aug 31 09:34 tty
crw--w----  1 root   tty       4,   0 Aug 31 09:34 tty0
crw-------  1 root   root      4,   1 Aug 31 09:34 tty1
crw-------  1 root   root      4,   2 Aug 31 09:34 tty2
crw--w----  1 Ekaitz tty       4,   8 Aug 31 09:37 tty8
crw-rw-rw-  1 root   root      1,   9 Aug 31 09:34 urandom
crw-rw-rw-  1 root   root      1,   5 Aug 31 09:34 zero
```

En el ejemplo se observan diferentes columnas, cada una con un significado:

1. Modo del archivo:
    - Tipo:
        - `-` archivo corriente
        - `l` enlace simbólico
        - `d` directorio
        - `p` tubería con nombre
        - `s` socket de dominio unix
        - `c` dispositivo de caracteres
        - `b` dispositivo de bloque
    - Permisos rwx en modo UGO
        - `s` en la posición `x` de U o G para el `setuid` o `setgid`.
        - `t` en la posición `x` de O para el bit `sticky`.
2. Número de referencias al archivo (ver apartado sobre Enlaces).
3. Usuario dueño del archivo
4. Grupo al que pertenece el archivo
5. Tamaño o la pareja *major number* - *minor number*
6. Fecha del último cambio
7. Nombre del archivo. En caso de que sea un enlace simbólico añade el símbolo
`->` seguido del nombre del archivo al que hace referencia.

Usando esta información, rápidamente puede obtenerse una idea general del
contenido del directorio `/dev` en el dispositivo de quien escribe este
documento, pero en ocasiones es necesario tener una visión más detallada de los
metadatos de un archivo. El comando `stat` aporta esta
información con un formato más detallado que incluye los valores
binarios correspondientes a cada campo recién descrito así como cierta
información adicional que `ls` no llega a mostrar. Ver `man stat`.
