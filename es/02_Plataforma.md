# La plataforma

Antes de comenzar a estudiar Unix, es necesario hacer un análisis general del
contexto de la propia plataforma.

Unix es un sistema operativo multiusuario, lo que implica de una forma u otra
que quien desee usar el sistema debe autenticarse con sus credenciales.

Además, Unix es multitarea, lo que significa que puede ejecutar varias
aplicaciones al mismo tiempo y, si es necesario, de forma coordinada.

Para comenzar a familiarizarse con estos dos puntos la mejor manera es comenzar
por la *shell*, nuestra forma de interactuar con el sistema operativo.

La forma más sencilla de seguir el documento y sus ejemplos es mediante una
distribución de GNU/Linux. Casi todos los ejemplos funcionarán en otras
distribuciones de Unix, como las basadas en BSD, pero no se asegura que sea así
y en algunos casos será necesario adaptar algún concepto.

## La *shell*

En los sistemas Unix, la *shell* de comandos es fundamental ya que todo el
sistema gravita a su alrededor. Conocer la *shell* de comandos es muy
importante por infinidad de motivos, el principal es que la terminal de
comandos aporta todas las herramientas necesarias para utilizar y configurar un
sistema basado en Unix, ya que en su momento era lo único que había disponible.
Además, aunque en el escritorio sea posible instalar una *shell* gráfica (que
hace uso de las capacidades de la *shell* de comandos subyacente), en los
entornos donde Unix brilla con más fuerza, como son el servidor o el software
embebido, normalmente no se dispone de entorno gráfico, pero sí de una interfaz
de comandos, ya sea a través de la red mediante el protocolo `ssh` o mediante
un puerto serie conectado al dispositivo.

En cualquier caso, se disponga o no de una *shell* gráfica en el sistema, en
entornos Unix, a diferencia de en otros, la *shell* de comandos sigue formando
parte fundamental del día a día de quien maneja el sistema y una vez dominada
facilita muchas tareas que suelen ser complicadas o imposibles de realizar en
el entorno gráfico.

El estándar *de facto* de los sistemas Unix es la *shell* Bourne Shell, aunque
ha sido mejorada de diversas formas, dando lugar a muchas *shell*s derivadas.
En algunos sistemas GNU/Linux la *shell* Bourne sigue estando disponible, en
otros se ha sustituido por completo por bash, una renovación de la Bourne Shell
que es la *shell* del proyecto GNU. Este hecho la hace ser la *shell* más común
en sistemas GNU/Linux, pero no es la única.

Existen muchas otras *shell*s, como ksh (KornShell), Zsh, tcsh, fish, etc. A
pesar de sus diferencias y funcionalidades específicas, todas ellas se
aproximan al estándar POSIX[^posix-shell] sobre *shell*s todo lo posible así
que todas son similares.

[^posix-shell]: Como nota histórica, el estándar POSIX se basó en la KornShell
  original, que está basada en Bourne Shell.

Los ejemplos de código utilizados durante el documento han sido probados
únicamente en bash, pero es muy probable que funcionen en otras *shell*s. En la
medida de lo posible se trata de utilizar código portable.

De momento no se prestará demasiada atención a la propia *shell* ni a su
funcionamiento, que se describirá en un apartado propio, pero es necesario
introducir algunos puntos.

### Acceso al sistema

En sistemas sin entorno gráfico, lo primero en exponerse es la terminal de
comandos. En este punto, el sistema pide la autenticación del usuario para el
dispositivo (sustituir `hostname` por el nombre del equipo):

```
hostname login:
```

Introducir el nombre de usuario seguido de la tecla `INTRO↵` hace que la shell
capture el nombre y requiera la contraseña:

```
hostname login: Ekaitz
Password:
```

Al introducir la contraseña no habrá ningún tipo de indicador visual, pero al
pulsar `INTRO↵` la contraseña se recogerá y si es la adecuada el sistema dará
acceso al usuario, normalmente mostrando un texto de bienvenida e instando al
usuario a actuar con cuidado.

En los sistemas con entorno gráfico, la interfaz será otra pero el proceso será
similar. Una vez accedido al sistema, ya que el contenido de este documento
trata sobre la *shell* de comandos, es necesario iniciar un emulador de
terminal (cualquiera es válido). El emulador de la terminal es un programa que
accede a la *shell* de comandos desde el entorno gráfico, permitiendo así
cambiar el ordenamiento de la pantalla, buscar en su salida, copiar y pegar el
texto a otras aplicaciones gráficas, etc.

Normalmente, los usuarios de un sistema con entorno gráfico usan los emuladores
de la terminal para acceder a la *shell* de comandos del sistema debido a su
comodidad de uso, aunque también es posible acceder a las terminales crudas
mostradas en los sistemas sin entorno gráfico, normalmente presionando las
teclas `Ctrl+Alt` junto a las teclas de función (`F1`, `F2`...). La tecla `F1`
normalmente muestra la *shell* en la que se arrancó el entorno gráfico, así que
suele estar ocupada. La combinación `Ctrl+Alt+F7` suele habilitarse como
combinación de retorno al sistema gráfico.[^DE]

[^DE]: Todo lo mencionado sobre entornos gráficos aquí debe tomarse con
  especial cuidado porque pueden variar mucho de una distribución a otra. Por
  ejemplo, algunas distribuciones de Linux, como Ubuntu, han deshabilitado
  estas combinaciones de teclas debido a que causaban confusión en las personas
  usuarias.

Para salir de la sesión de la *shell* puede usarse el comando interno `exit`
(ver `help exit`) o pulsar `Ctrl+D`[^ctrld].

[^ctrld]: Realmente `Ctrl+D` envía una indicación de «final del archivo», lo
  que provoca que la terminal dé la sesión por finalizada.

### El *prompt*

Una vez accedido a la *shell* del sistema, lo primero que se muestra es un
*prompt*.  El *prompt* es un pequeño indicador de texto que indica que puede
introducirse un nuevo comando.

Por defecto[^prompt], el *prompt* completo en bash suele ser algo similar a:

``` bash
usuario@hostname:carpeta-actual$
```

Así que un usuario de nombre `ekaitz` en una máquina con nombre `maquina` que
está situado en la carpeta `/home/ekaitz/` tendría el siguiente *prompt*:

``` bash
ekaitz@maquina:/home/ekaitz$
```

El símbolo del dólar, `$`, indica que el usuario actual es un usuario sin
privilegios de administrador. Si los tuviera, el símbolo cambiaría a `#` para
sugerir a quien introduce los comandos que lo haga con cuidado, ya que está
identificado en el sistema como usuario privilegiado así que puede afectar de
forma libre al sistema pudiendo eliminar archivos críticos, terminar procesos,
etc. porque  «*Unix te da suficiente cuerda para que te ahorques*».

En los ejemplos de código que se encontrarán por el documento usan una versión
simplificada del *prompt*, indicando únicamente `$` para los comandos que deben
ejecutarse como usuario no privilegiado y `#` para los que deben ejecutarse
como administrador del sistema.

[^prompt]: En la *shell* bash el *prompt* es configurable mediante la
  variable de entorno `PS1`. No es el único *prompt* que se puede configurar,
  porque hay otros tipos de *prompt* en el sistema. La documentación de bash
  tiene más información al respecto (ver `info bash` o `man bash`).

### Funcionamiento de la *shell*

La *shell* puede funcionar tanto como intérprete de comandos sueltos como
intérprete de programas, que realmente son la misma funcionalidad, pero
utilizada de dos formas diferentes. De momento, sólo es necesario comprender el
funcionamiento más mínimo.

La *shell* de comandos recibe una línea de texto y espera hasta que se pulse la
tecla `INTRO↵`. Cuando se hace, la *shell* absorbe el texto introducido y lo
divide en piezas separadas por espacios. La primera palabra se considera el
comando a ejecutar y el resto de palabras se consideran argumentos. Por
ejemplo, al ejecutar:

``` bash
$ ls folder
```

Se considera que `ls` es el comando y `folder` es su argumento. Mientras que:

``` bash
$ ls -la folder
```

Considera que `ls` es el comando y `-la` y `folder` son sus argumentos.

Visto así, puede surgir rápidamente la necesidad de querer enviar argumentos
que contengan espacios. Por supuesto, es posible hacerlo. Mediante varias
formas:

``` bash
$ ls "folder with multi-word name"

$ ls 'folder with multi-word name'

$ ls folder\ with\ multi-word\ name
```

Las dos primeras usan una cadena de caracteres como argumento y la última
escapa la evaluación de espacios como separador de argumento. Se consigue un
resultado idéntico en todas ellas, pero más adelante se verá que tienen
peculiaridades que debemos estudiar en el futuro.

### Comandos

La *shell* captura el comando introducido y trata de ejecutarlo con los
argumentos enviados. Existen dos tipos principales de comandos que la *shell*
puede utilizar: llamadas a programas o comandos internos (conocidos como
*built-in*).

#### Built-ins

Los Built-ins los ejecuta la propia shell y no requieren la ejecución de ningún
programa, mientras que las llamadas a programas requieren la ejecución de un
programa externo, lo que requiere de una llamada al sistema.

A nivel de uso no puede notarse una diferencia entre lo primero y lo segundo:

``` bash
$ cd carpeta

$ ls carpeta
```

`cd` es un comando interno y `ls` un programa independiente, pero la forma de
llamarlos es la misma.

Lo que sí que cambia la interacción con los comandos internos es la forma de
obtener su documentación. Para hacerlo, se usa el comando interno `help`. Para
conocer cómo usarlo, lo mejor es ver la documentación de `help`.

``` bash
$ help help
```

> Para comprobar si un comando es interno o un ejecutable externo el comando
> puede usarse el comando interno `type`. Por ejemplo `type cd` responderá que
> `cd` es un comando interno. Para más información: `help type`.

#### Programas

Los programas externos se ejecutan mediante una llamada al sistema operativo de
la familia `exec`. Simplemente se busca a programa con el nombre indicado en
las carpetas configuradas como lugares donde buscar programas.

Siendo así, la ejecución del comando:

``` bash
$ ls -l folder
```

Buscará un programa llamado `ls` en la carpeta correspondiente y lo ejecutará
utilizando `-l` y `folder` como argumentos. Cuando el programa termine, la
*shell* recuperará el control, permitiendo a quien la usa introducir un nuevo
comando.

#### Comandos útiles

En lugar de dedicar espacio mental a memorizar una larga lista de comandos,
éstos serán introducidos de forma paulatina durante todo el documento. Sin
embargo, hay unos mínimos que sería interesante aprender cuanto antes, casi
todos relacionados con el tratamiento de archivos ya que «*todo es un
archivo*».

- `ls`: lista los archivos y carpetas en los directorios que se le indiquen
  como argumento o, por defecto, en el directorio actual.
- `cat`: imprime por pantalla el contenido de los archivos que se le indiquen,
  concatenándolos.
- `grep`: filtra archivos en función de patrones de texto.
- `head`: imprime las primeras líneas de un archivo.
- `tail`: imprime las últimas líneas de un archivo.
- `cd`: cambia el directorio actual de la *shell*. Es un *built-in*.
- `touch`: cambia la fecha de edición de un archivo y si no existe lo crea.
- `mkdir`: crea directorios.
- `rm`: elimina archivos.
- `rmdir`: elimina directorios vacíos.
- `ps`: visualiza información sobre procesos activos.

> **NOTA**: Esta es una lista bastante reducida de comandos útiles. Muchos de
> ellos tienen funcionalidades avanzadas que deben consultarse a través de la
> documentación del sistema, como ya se ha visto. A partir de este punto se
> consideran comandos conocidos y se utilizarán sin más explicación.

Como cada sistema puede tener más o menos programas instalados, es interesante
tener una referencia de qué comandos son considerados los más básicos de un
sistema operativo. El proyecto *GNU Core Utilities* (también conocido como
*coreutils*) recoge todas las funcionalidades principales de los sistemas GNU
en un único paquete para asegurar que las distribuciones que lo incluyan tienen
lo mínimo suficiente.

Existen proyectos similares con diferente perspectiva, licencia u objetivos,
como por ejemplo el proyecto BusyBox, que trata de aportar todas estas
utilidades a modo de un sólo programa ejecutable optimizado para entornos
embebidos con poco espacio de almacenamiento de programas.

### Variables de entorno

La *shell* de comandos no deja de ser un entorno de programación, y como tal
también puede declarar variables. Normalmente, las variables sólo están
visibles en la sesión actual pero existen un tipo de variables que pueden
exportarse a todas las sesiones de la *shell* que se obtengan desde la sesión
actual.

Este tipo de variables, conocidas como variables de entorno (*environment
variables*), se utilizan generalmente como configuración.

Por ejemplo, la variable `PATH` guarda dónde deben buscarse los programas a la
hora de ejecutar comandos externos. Alterar el valor de esa variable hace que
la *shell* busque los programas en directorios distintos.

Hay varias variables interesantes que deben recordarse:

- `PATH` guarda la lista de directorios donde se buscarán los programas
  ejecutables. Como es normal en Unix, los directorios se separan por dos
  puntos, `:`. Por ejemplo, el valor `/usr/bin:/bin` hará que la *shell* busque
  los programas en `/usr/bin` y `/bin`.
- `PWD` guarda el directorio actual.
- `EDITOR` y `PAGER` son variables de entorno para configurar el editor y
  paginador por defecto que usará la *shell*.

Para declarar variables de entorno puede usarse el comando interno `export`.
Éste indica a la *shell* que la variable señalada debe exportarse al resto de
programas que se ejecuten desde la *shell*.

Lo más común es asignarle un valor a las variables en el momento de
exportarlas.  Por ejemplo:

``` bash
$ export EDITOR=vim
```

Exporta la variable `EDITOR` con valor `vim`.

La opción `-p` permite visualizar todas las variables exportadas, pero como
pueden ser muchas es más sencillo usar el comando interno `echo`, que sirve
para imprimir texto en la pantalla, para verlas.

``` bash
$ echo $EDITOR
vim
$
```

Al introducir el comando de la primera línea, la *shell* resuelve el valor de
la variable y la visualiza por la llamada a `echo` escribiendo `vim`. Al
terminar, devuelve el *prompt* para esperar por el siguiente comando.

El símbolo del dólar, `$`, por delante del nombre de la variable indica, como
se verá más adelante, que esa palabra, en este caso `EDITOR`, no debe tratarse
simplemente como texto, sino que debe evaluarse como una variable. En caso de
no introducirlo ocurre lo siguiente:

``` bash
$ echo EDITOR
EDITOR
$
```

Simplemente imprime el texto sin evaluar.

## Usuarios

Cuando Unix nació, los ordenadores se compartían por muchas personas, ya que
eran dispositivos caros y gigantescos. Es por eso que Unix desarrolló
herramientas para gestionar usuarios, asignarles permisos, carpetas propias,
etc. De este modo compartir los recursos de una máquina era mucho más sencillo.

Este sistema de usuarios es muy frecuente hoy en día también, cuando el uso de
las computadoras ha cambiado bastante. En primer lugar, sigue cumpliendo el
caso de uso para el que se diseñó, permitiendo el acceso compartido a una
máquina, pero también sirve para asignar programas a perfiles de usuario,
limitando así su alcance en el sistema y aumentar así la seguridad del mismo.

Es por eso que es importante conocer de forma al menos general cómo trabajar
con el sistema de usuarios si se quiere gestionar un sistema tipo Unix de forma
segura a día de hoy.

### Superusuarios

El usuario administrador, o superusuario, suele llamarse `root` en los sistemas
Unix. Este usuario tiene capacidades absolutas en el sistema. Funcionar en modo
administrador es tan sencillo como ingresar en el sistema como tal, si se
tienen sus credenciales, nombre de usuario (`root`) y contraseña, o ejecutar el
comando `su` (*superuser*) e introducir las credenciales.

Tener acceso de administrador da acceso total a la máquina, así que no hace
falta decir que la contraseña debe guardarse con extremo cuidado.

#### sudo

Los superusuarios pueden hacer **cualquier cosa** en la máquina y en
general no se les consultará si están seguros de hacerlo. Es bastante sencillo
equivocarse y estropear el sistema si no se anda con cuidado.

Es por eso que lo más lógico es funcionar en modo usuario no-privilegiado la
mayor parte del tiempo y sólo usar los privilegios de usuario administrador en
casos concretos. Así se evitan problemas.

En ese caso, un usuario con acceso de administrador podrá usar el comando `sudo`
(*superuser-do*) para ejecutar un comando concreto de forma privilegiada.

Para saber si un usuario tiene permiso para ejecutar `sudo` y usar así
capacidades de superusuario, se comprueba si su nombre aparece en el archivo
`/etc/sudoers`. Se puede leer más sobre este archivo usando `man
sudoers`.[^sudo-modernas]

[^sudo-modernas]: En algunas distribuciones más modernas se elimina por
  completo el usuario `root` y se utiliza `sudo` como puente para que los
  usuarios puedan ejecutar comandos con privilegios. Esto no tiene demasiadas
  implicaciones técnicas ya que cualquier usuario en `/etc/sudoers` puede
  ejecutar `sudo su` y acceder a una sesión en modo de superusuario a pesar de
  que el usuario `root` como tal no exista.

### Gestión de usuarios

Tres comandos principales sirven para añadir, eliminar y editar usuarios.
`useradd`, `userdel` y `usermod`, respectivamente[^users].

La creación de un usuario es un proceso relativamente complejo debido a las
opciones que aporta así que es necesario desgranar unos mínimos.

[^users]: Debido a la complejidad de los comandos, existen versiones más
  interactivas llamadas `adduser` y `deluser` que no siempre se distribuyen en
  las Distribuciones,

#### Carpeta personal

Los usuarios creados pueden tener una carpeta propia, diseñada para que guarden
sus archivos. Las carpetas personales se almacenan en `/home/`, normalmente
cada una con el nombre del usuario correspondiente.

El comando `useradd` permite seleccionar una carpeta personal para el usuario a
crear mediante la opción `--home-dir`, pero la carpeta debe crearse
manualmente.

Normalmente, los usuarios creados para procesos no incluyen una carpeta para
ellos. El comando `useradd` permite deshabilitar la asignar de una carpeta
para el usuario usando `--no-create-home`.

La carpeta personal suele denominarse `home` (hogar), y comúnmente así se
conoce. La *shell* además suele tener definida la variable de entorno `HOME`
con el valor de la carpeta personal del usuario actual. Además, la *shell*
expande el símbolo de la vírgula (en inglés *tilde*), `~`, a la carpeta
personal del usuario actual. De este modo:

``` bash
$ cd ~
```

Cambia de directorio actual a la carpeta personal.

#### Grupos

Los usuarios pueden pertenecer a grupos. Los grupos sirven para crear conjuntos
de usuarios con permisos concretos. De esta manera se puede dar un permiso a un
grupo y que todos los usuarios del grupo lo reciban automáticamente.

Los comandos `groupadd`, `groupdel` y `groupmod` sirven para crear, eliminar y
editar grupos. Para mostrar los grupos del sistema puede usarse el comando
`groups`.

Un ejemplo de grupo aportado habitualmente por el sistema es `dialout` que
aporta acceso a puertos serie a todos sus miembros.


#### Configuración subyacente

Todos estos comandos usan principalmente el sistema de archivos para guardar su
configuración y lo único que hacen es interactuar los archivos de
configuración añadiendo, editando o borrando entradas. Si se desea investigar
en más detalle, se recomienda leer las siguientes páginas del manual:

- `man 5 passwd`
- `man 5 shadow`
- `man 5 group`


### Otros sistemas de autenticación

Aunque el sistema de autenticación clásico de Unix estuviera basado en el uso
de un nombre de usuario y una contraseña, desde la implementación de PAM
(*Pluggable Authentication Modules*) en los años 90 es posible utilizar otros
métodos de autenticación en casi todos los sistemas operativos similares a
Unix.

PAM permite la sustitución del método de autenticación por otros, de modo que
en lugar de usar la contraseña puedan usarse lectores de huellas digitales,
sistemas centralizados de autenticación u otros (ver `man 7 pam`).  El sistema
clásico de autenticación está ahora disponible mediante un módulo de PAM (ver
`man pam_unix`).


## Procesos

Unix se define como un sistema operativo multitarea. A nivel de uso esto se
traduce en que puede generar procesos y realizar diferentes tareas en cada
proceso.

Cada vez que se ejecuta un programa en la *shell* (ya sea la gráfica o la
terminal de comandos) se crea un nuevo proceso en el sistema y se le asigna un
identificador de proceso (*process identifier*, o PID). A nivel del núcleo,
cada proceso se almacena en una tabla de procesos y el tiempo de computación se
va distribuyendo entre ellos.

Los procesos siguen una estructura de árbol que indica quién fue el proceso que
creó el proceso actual. Todos los procesos tienen un proceso padre que es el
proceso desde el que se crean los nuevos. Como se verá más adelante en detalle,
un proceso principal, normalmente conocido como `init`, es el padre de todos
los procesos y se le asigna el PID 1.

En el caso de la *shell*, cuando se ejecuta un comando que acaba llamando a un
programa, la *shell* es la que crea el nuevo proceso y ejecuta el programa en
él.

En apartados posteriores se analiza cómo funcionan estos mecanismos a nivel de
núcleo y se estudia cómo deben gestionarse los procesos en aplicaciones
programadas para sistemas operativos de tipo Unix. Por el momento, con conocer
estos puntos básicos es más que suficiente para saber gestionar el sistema.

### Gestión básica de procesos en la shell

La *shell* es la interfaz para relacionarse con el sistema y ejecutar sus
programas y, como tal, tiene capacidad para lanzar procesos, pero también para
pausarlos, enviarlos a segundo plano o terminarlos.

Lanzar un proceso es tan sencillo como ejecutar un programa. Para aprender a
controlar los procesos lo más sencillo es utilizar un programa que se ejecute
durante un periodo de tiempo largo y así poder interactuar con él. Un buen
ejemplo es `sleep`, un programa que se ejecuta durante los segundos que se le
pidan y una vez han pasado termina.

``` bash
$ sleep 1000
```

Una vez lanzado, el programa ha tomado las riendas de la terminal y no es
posible seguir introduciendo comandos. Pulsar las teclas `Ctrl+C` envían una
señal de terminación al programa, permitiendo cortar su ejecución.

``` bash
$ sleep 1000
^C
$
```

Al pulsar `Ctrl+C` se muestra la combinación `^C` en la pantalla, que indica
exactamente eso, que se han pulsado esas teclas (`^` es una convención para
visualizar de forma gráfica la tecla `Ctrl`), y se corta la ejecución del
programa, devolviendo el *prompt*.

Otras combinaciones tienen efectos distintos. Pulsar `Ctrl+Z` pausa el proceso,
recuperando el *prompt*, pero lo mantiene creado para poder continuarlo en
donde se pausó.

``` bash
$ sleep 1000
^Z
[1]+  Stopped                 sleep 1000
$ 
```

También es posible lanzar procesos en la *shell* directamente en segundo plano,
para ello se añade el símbolo et («`&`») al final del comando:

``` bash
$ sleep 3 &
[1] 6808
$ ...
[1]+  Done                    sleep 3
```

Al lanzar el comando del ejemplo, la *shell* indica que el proceso ha sido
lanzado con el PID `6808` y retorna el *prompt* permitiendo lanzar más
comandos. Una vez el comando termine, la *shell* se reserva el aviso y justo
antes de retornar el siguiente *prompt* avisa de que el comando en segundo
plano terminó.

El programa `ps` sirve para mostrar información sobre los procesos en marcha en
el sistema. Ejecutarlo sin argumentos muestra los procesos en la sesión actual,
pero tiene muchas opciones que permiten listar todos los procesos de la
máquina. Como es un comando extremadamente complejo (ver `man ps`) se
recomienda recordar usar `ps aux` para mostrar todos los procesos en activo en
la máquina. Para el caso del ejemplo anterior, es suficiente con una ejecución
simple para ver si el programa `sleep` sigue ejecutándose:

``` bash
$ ps
  PID TTY          TIME CMD
 9091 pts/1    00:00:00 bash
 9489 pts/1    00:00:00 sleep
 9505 pts/1    00:00:00 ps
```

Las *shells* normalmente disponen de un sistema avanzado de control de procesos
que va más allá de lo que se acaba de estudiar. `bash`, por ejemplo, incluye
los comandos internos `bg`, `fg` y `jobs` de los que se puede conocer más en
detalle leyendo el propio manual de bash (`man bash`) en la sección de control
de procesos (*job control*).

### Señales

Tanto `Ctrl+Z` como `Ctrl+C` se traducen en el envío de señales a procesos. Las
señales son un sistema de comunicación entre procesos usada en sistemas
operativos compatibles con POSIX que envían notificaciones de eventos entre los
diferentes procesos (informar de un error, pedir la terminación del proceso,
etc).

POSIX define varias señales, identificadas por `SIG` seguido de su
funcionalidad o por un identificador numérico. Quizás la más conocida y común
es la señal número `9`, `SIGKILL`, que sirve para terminar (*kill* significa
«matar» en inglés) un proceso.

Desde el punto de vista del programa hay señales cuya finalidad es la de
disparar acciones que el programa debe ejecutar y otras que simplemente
terminan el programa sin opción a gestionarse. La señal `SIGINT`, que es la que
se envía en la terminal al usar `Ctrl+C`, puede capturarse para preparar una
finalización correcta del programa, terminando conexiones con bases de datos,
escrituras a archivos, etc. Mientras que la señal `SIGKILL` fuerza una
interrupción drástica.

La página del manual `man 7 signal` incluye una visión general del
funcionamiento de las señales en el sistema, donde puede apreciarse que es un
mecanismo complejo muy relacionado con fallos internos de los programas, cosa
que desde el punto de vista del desarrollo de aplicaciones para sistemas Unix
es muy importante, pero quizás no sea tan relevante para quien sólo usa el
sistema.

A nivel práctico desde la *shell*, las señales pueden enviarse mediante
comandos de la familia `kill` como son el propio `kill`, y otros como
`killall` y `pkill`.  Su diferencia principal radica en el sistema de selección
del proceso, el primero necesita el PID del proceso al que enviar la señal
elegida, mientras que los otros dos, más difíciles de controlar, envían la
señal a cualquier proceso que coincida con un patrón de búsqueda.

Sea cual sea la opción elegida, requiere conocer las señales de forma al menos
superficial para conocer qué señal se desea enviar. Generalmente el interés de
un usuario humano será el de terminar procesos muertos mediante la *shell*,
para lo que sólo debe elegirse la severidad con la que hacerlo, esto es: elegir
si se desea o no usar `SIGKILL`.
