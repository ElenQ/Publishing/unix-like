# Unix

Para hablar de sistemas similares a Unix lo más lógico es hablar del propio
Unix, pero esta no es la única razón para hacerlo. Unix ha sido extremadamente
influyente en sistemas operativos posteriores y sus ideas han sentado las bases
de la informática. Es por eso que sigue en uso hoy en día después de tantos
años.

## Historia

Unix nace como un sistema operativo sin nombre en un departamento de
investigación y desarrollo conocido como Bell Labs, una simplificación de *Bell
Telephone Laboratories*, de la mano de Ken Thompson y Dennis Ritchie[^ritchie].

Este primer Unix, escrito completamente en ensamblador, funcionaba en una
máquina PDP-7, un «minicomputador» de media tonelada de peso. Era el año 1969.

En 1970, para hacer un chiste con su predecesor Multics (acrónimo de
*Multiplexed Information and Computer Services*), un sistema operativo
multitarea, a Brian Kernighan[^kernighan] se le ocurre la idea de llamar Unics
al nuevo sistema operativo, ya que este era monotarea, pero era más eficiente y
sencillo. De ese nombre surge la grafía final Unix.

Debido al precio bajo de las licencias en aquella época, pronto empiezan a
surgir distribuciones de Unix con ciertos cambios sobre las versiones
originales, siendo las más conocidas y relevantes a día de hoy BSD (*Berkeley
Software Distribution*), desarrollada por la Universidad de California,
Berkeley, y System V, desarrollada por AT&T.

En 1983, Richard Stallman comienza el proyecto GNU (acrónimo de *GNU is Not
Unix!*, «GNU no es Unix») con el objetivo de crear un sistema operativo libre,
permitiendo el estudio, la distribución y la mejora del sistema. Sus esfuerzos
consiguen replicar gran parte de la funcionalidad de Unix con programas
escritos por un conjunto de voluntarios y por él mismo, pero el núcleo en el
que trabajan, GNU Hurd, no tiene éxito.

Al no disponer de ningún núcleo completo fácil de analizar y estudiar que poder
utilizar como base para sus clases, el profesor Andrew S. Tanenbaum, de la
*Vrije Universiteit* en Amsterdam, decide desarrollar un sistema operativo
sencillo, libre y fácil de estudiar llamado Minix y lo publica acompañado de su
libro *Operating Systems: Design and Implementation*, traducido al español como
«Sistemas Operativos: Diseño e Implementación», en 1987. Este libro, que
incluye una descripción muy profunda del sistema e incluso su código fuente
completo, es un éxito y las universidades de todo el mundo comienzan a usarlo
como base para sus asignaturas sobre sistemas operativos[^tanenbaum].

> Como nota histórica, en 1976 John Lions publicó un libro (*Lions' Commentary
> on UNIX 6th Edition, with Source Code*) sobre Unix que describía cada línea
> de código de la versión 6 de Unix. En 1979 AT&T, que tenía el control sobre
> Unix, cambia completamente su filosofía y al publicar la versión 7 de Unix
> prohíbe la publicación de libros sobre este y el estudio de su código fuente
> en las universidades. Este es el detonante principal de muchos de estos
> proyectos de replicación de Unix desde una perspectiva abierta y fácil de
> estudiar.

Sin embargo, Minix era un sistema muy particular que hacía algunas concesiones
técnicas con el fin de ser simple y fácil de enseñar. Es por eso que en 1991,
un estudiante de informática de la Universidad de Helsinki llamado Linus
Torvalds anuncia su proyecto de desarrollo de un núcleo similar a Minix, pero
con unas características de diseño diferentes. Poco más tarde anuncia que ya
dispone de una versión funcional capaz de ejecutar varias utilidades del
proyecto GNU y libera el código fuente, que pronto recibe contribuciones de
miembros de la comunidad de Minix y el proyecto comienza a crecer.

El proyecto de Linus Torvalds toma el nombre Linux por un chiste con el
nombre de su autor y la palabra Unix.

En el momento de la publicación de Linux existe cierta polémica y surgen varias
discusiones técnicas entre Andrew S. Tanenbaum y Linus Torvalds debido a la
decisión de Linus de diseñar su núcleo de forma monolítica en lugar de utilizar
un micronúcleo, como Minix. Sin embargo, las discusiones terminan y Linux se
convierte en el núcleo libre más popular del momento, dejando a Minix en un
segundo plano.

Debido a su popularidad y a su compatibilidad con GNU, Linux pronto pasa a
combinarse con el resto del software de GNU para crear un sistema operativo
completo: GNU/Linux.

Minix, por otro lado, sigue desarrollándose y centrándose en la resistencia que
su sistema micronúcleo le aporta. De esta forma se convierte en una interesante
alternativa para entornos embebidos de pocos recursos que necesitan de un
sistema que sea muy difícil de romper.

GNU/Linux y Minix no son los únicos sistemas operativos libres derivados de
Unix. A finales de los años 80, debido al incremento de los precios de las
licencias de Unix los desarrolladores de BSD, la distribución de la Universidad
de California, deciden distribuir el sistema de forma libre, bajo los términos
de la licencia BSD, dando lugar a una gran rama de proyectos derivados. Algunos
de ellos han sido tomados por empresas para desarrollar ramas privativas tales
como MacOS o el software en las consolas PlayStation4 o Nintendo Switch. Otros,
como NetBSD o FreeBSD, siguen siendo libres a día de hoy.

Estos no son los únicos proyectos derivados de Unix a día de hoy. System V
también fue extremadamente influyente en la creación de sistemas privativos
como Solaris o AIX, donde también han ocurrido infinidad de innovaciones
técnicas.

[^ritchie]: También conocido por ser el autor del lenguaje de programación C.

[^kernighan]: También conocido por participar en el desarrollo del lenguaje de
  programación C y escribir, junto a Dennis Ritchie, el libro que popularizó el
  lenguaje.

[^tanenbaum]: Tal es su importancia que quien escribe este texto estudió
  sistemas operativos usando ese mismo libro, a pesar de haber cursado la
  asignatura en 2011.

### POSIX

En 1988, con la proliferación de distribuciones de Unix y la aparición de otros
sistemas operativos, el IEEE (*Institute of Electrical and Electronics
Engineers*) comienza a desarrollar un estándar de compatibilidad entre sistemas
operativos con el objetivo de que las nuevas aplicaciones puedan funcionar en
diferentes sistemas operativos sin requerir cambios en su código fuente. El
estándar se nombra POSIX, como acrónimo de *Portable Operating System
Interface* [^posix-name].

Las primeras versiones de POSIX se basan fundamentalmente en Unix, pero
necesitan encontrar una base común, ya que las diferentes versiones de Unix han
divergido.

La primera versión de POSIX únicamente describe la interfaz con el sistema
operativo (la *API*), pero a partir de ahí se estandarizan cada vez más
detalles: la shell, usando como referencia la shell de System V; los sockets,
basados en los sockets Berkeley; algunas herramientas, como `awk` y `sed`;
gestión de hilos de ejecución, ahora conocidos como `pthread` (de
POSIX-Thread); intercomunicación entre procesos; etc.

En cierto modo POSIX sólo recopila en un documento los detalles que ya se
habían convertido en estándares de-facto, pero sirve para asentar estos
conceptos y facilitar el desarrollo de nuevos sistemas que expongan interfaces
compatibles con los ya existentes.

El proceso de actualización de POSIX sigue activo a día de hoy, siendo la
última actualización del año 2018, y su relevancia sigue siendo altísima, sobre
todo en los sistemas similares a Unix. Es por eso que no debe extrañar su
aparición a lo largo del documento.

[^posix-name]: Curiosamente, este nombre fue idea de Richard Stallman, que en
  aquel momento formaba parte del comité del IEEE.

### Distribuciones de Linux

Las primeras distribuciones de Linux surgen poco después de la aparición del
núcleo. Desde su primera aparición, Linux se entrega al público como código
fuente, sin estar preparado para su uso. Es por eso que surge el interés de
prepararlo, junto con herramientas del proyecto GNU como una imagen lista para
instalar en una máquina. Estas imágenes son lo que se conoce como distribución
de Linux[^distro-naming].

[^distro-naming]: Existe cierto grado de controversia con el nombre
  *distribución de Linux*. El proyecto GNU defiende que GNU es un sistema
  operativo y Linux sólo es uno de sus núcleos, por lo que las distribuciones
  no deberían usar el nombre Linux sino GNU+Linux o GNU/Linux, para indicar que
  Linux y GNU son igual de importantes.

Las distribuciones incluyen el software que sus autores deciden incluir, así
que pronto aparecen nuevos equipos que preparan imágenes con otras
herramientas, más adecuadas para sus tareas, que incluso comienzan a incluir
software diseñado específicamente para estas distribuciones.

Poco a poco las distribuciones se van especializando, se forman equipos de
desarrollo para mantenerlas y su infraestructura crece, aportando sistemas de
paquetería con repositorios oficiales en la red desde los que poder acceder a
más software que el que se aporta por defecto.

Incluso aparecen las primeras distribuciones de Linux en el entorno
empresarial. Empresas cuyo modelo de negocio es aportar una distribución de
Linux estable y con una cuota de mantenimiento.

Este ecosistema no hace más que crecer con el tiempo, llegando a producir
cientos y cientos de distribuciones, aunque algunas se abandonen por el camino.
Hoy en día existen tantas distribuciones de Linux que incluso se han acuñado
términos como el *distro-hopping*, que denomina la costumbre que tienen algunas
personas de cambiar de una distribución a otra constantemente.

#### Características de una distribución

A pesar de la gran cantidad de distribuciones existentes, en general suelen
diferenciarse por un conjunto básico de características que debe analizarse
antes de seleccionar una distribución con la que trabajar. Estas
características pueden resumirse de la siguiente manera:

- **Objetivo**: cada distribución parte de un objetivo distinto, que suele
  condicionar el resto de características. Una distribución puede centrarse en
  ser rápida, ligera, estable, orientada a servidor, orientada a escritorio,
  fácil de usar, visualmente atractiva, etc. Cada distribución suele exponer
  claramente cuales son sus intenciones.
- **Gestión de paquetes**: al principio las distribuciones comenzaban a
  diferenciarse por su sistema de paquetería, que es una aplicación que permite
  instalar y gestionar otras aplicaciones, ya sea a través de Internet usando
  los repositorios que las propias distribuciones mantienen o a partir de
  paquetes obtenidos por otros medios. Las familias de distribuciones suelen
  compartir gestor de paquetes.
- **Sistema de actualizaciones**: algunas distribuciones eligen publicar
  versiones cada varios meses y requerir un proceso de actualización para pasar
  de una a otra. Otras distribuciones se actualizan sobre la marcha, lo que se
  conoce como *rolling-release*.
- **Software incluido por defecto**: las distribuciones más orientadas a una
  tarea incluyen por defecto software que la facilita. Por ejemplo, Kali Linux,
  una distribución orientada a la ciberseguridad, incluye muchas herramientas
  relacionadas con ese tema que normalmente no suelen incluirse en otras
  distribuciones. Lo mismo ocurre con las distribuciones escolares, o las que
  están orientadas a personas creativas que se dedican a la edición de vídeo,
  audio, etc.

#### Familias de distribuciones

Como comenzar una distribución completa es una tarea difícil, muchas
distribuciones se basan en distribuciones anteriores, creando así un árbol
genealógico gigantesco, pero que puede dividirse fácilmente en unas pocas
familias.

- **Slackware** es una de las primeras distribuciones en aparecer y es la más
  antigua que se mantiene viva a día de hoy. Su objetivo principal es ser
  estable y simple. Se considera una distribución algo difícil de usar en
  comparación con otras más modernas. Ha resultado ser una gran influencia en
  otras distribuciones como las primeras versiones de SUSE.

- **Debian** es una de las distribuciones más exitosas tanto en servidor como
  en escritorio y es la distribución madre de una gran familia, que incluye
  Ubuntu, una distribución muy popular, y a toda su gran sub-familia de
  distribuciones. Su gran popularidad en el servidor se debe principalmente a
  su orientación a la estabilidad y a su fácil instalación en este entorno,
  donde brilla siendo la distribución más común.  
  Debian usa un sistema de paquetes propio, que también es muy popular,
  conocido como `dpkg` sobre el que se construye `apt`, un programa de gestión
  de software que facilita la descarga de paquetes desde los repositorios y
  gestiona las dependencias.

- **RedHat** se mantiene por la empresa del mismo nombre, recientemente
  adquirida por IBM. Esta empresa mantiene la distribución y aporta a sus
  clientes soporte y calidad de servicio sobre el software. Además, apoya el
  desarrollo de su versión no-comercial, conocida como Fedora. Esta familia de
  distribuciones usa el sistema de paquetes `rpm`.

- **ArchLinux** es una distribución bastante popular que se publica mediante el
  sistema de *rolling-release*. Es una distribución sencilla, pragmática y
  versátil. Muy frecuente entre los usuarios avanzados. Las distribuciones de
  esta familia usan el sistema de paquetes `pacman`, que fue diseñado para esta
  distribución en particular. También destaca por su extensísima documentación
  en forma de wiki conocida como ArchWiki, útil para usuarios de cualquier
  distribución.

- **SUSE**: similar a RedHat, SUSE es una empresa alemana que mantiene una
  distribución con el mismo nombre. Esta distribución también tiene una versión
  abierta, *openSUSE*. Esta familia es muy reducida en comparación con las
  anteriores.

- **Gentoo** es una distribución basada en código fuente, lo que significa que
  los programas no se distribuyen ya compilados, sino que se distribuyen en
  código fuente y se compilan en la propia máquina en la que se instalan,
  pudiendo aportar así optimizaciones específicas para la máquina en la que se
  ejecutan. Usa el sistema de paquetes Portage, que es el que permite este
  funcionamiento. La distribución también usa un sistema de *rolling-release*.
  A pesar de que su familia no es tan extensa como en otras, ha dado lugar a
  distribuciones muy populares.


Las distribuciones mencionadas son todas de propósito general, lo que facilita
que se extiendan o se especialicen, dando lugar a familias más amplias. Existen
sin embargo distribuciones más específicas, nacidas con objetivos más
concretos, o distribuciones que rompen con los mecanismos habituales. Algunos
ejemplos interesantes son los siguientes:

- **NixOS** es una distribución basada en el sistema de paquetes Nix que
  permite describir la configuración del sistema completo de forma declarativa
  y unificada, permitiendo replicar el sistema y controlar sus actualizaciones
  de forma sencilla y completamente reversible. El sistema de paquetes es quien
  facilita gran parte de esta tarea ya que está diseñado de forma que la
  instalación de paquetes es replicable y puramente funcional (en el sentido de
  la programación funcional). Esta distribución da lugar también a la
  distribución Guix System, basada en el sistema de paquetes Guix, basado en
  Nix. Ambas distribuciones son especialmente importantes en el entorno del
  software reproducible, como por ejemplo en estudios científicos, donde es
  necesario poder reproducir el entorno donde el estudio se ejecutó para poder
  comprobar los resultados del mismo.

- **OpenWRT** es una distribución orientada a dispositivos embebidos,
  principalmente aquellos que rutan tráfico de red. Esta distribución, basada
  en `musl` y `BusyBox`, es extremadamente ligera con el fin de que pueda
  instalarse en el poco espacio de almacenamiento que estos dispositivos
  proporcionan. Además, incluye software específico para controlar en sistema
  de rutado y una interfaz web de control. Su sistema de paquetes, `opkg`,
  expone una interfaz similar a `apt` pero siendo mucho más ligero.

- **Alpine Linux** es una distribución diseñada para la seguridad, simplicidad
  y eficiencia. Al igual que OpenWRT, se basa en `musl` y `BusyBox` para ser lo
  más ligera posible aunque su objetivo no sean los dispositivos embebidos. Es
  muy frecuente como distribución en los sistemas de contenedores debido a su
  velocidad de arranque y su reducido tamaño.

Las distribuciones mencionadas no son más que una mínima muestra del vasto
universo de distribuciones disponibles, pero aporta una visión de conjunto
suficiente. Para obtener más información sobre distribuciones pueden
consultarse páginas como [DistroWatch](https://distrowatch.com/), cuyo objetivo
es registrar distribuciones y aportar noticias, información y un ranking
de las mejores.

## Filosofía

El gran impacto de Unix en la informática se debió a muchos motivos pero quizás
el más interesante de ellos es su fuerte filosofía, que impregna casi cada
ápice del sistema.

La filosofía Unix se documentó allá por el 1978 de forma bastante concreta
aunque ha sido resumida, extendida y reinterpretada. Sus puntos principales
gravitan alrededor de dos conceptos: la simplicidad de los programas y el uso
del texto como formato de almacenamiento e interfaz entre programas.

Peter H. Salus resumió la filosofía Unix en 1994 con estos simples puntos:

- «*Escribe programas que hagan una cosa y la hagan bien*»
- «*Escribe programas para trabajar juntos*»
- «*Escribe programas para manejar flujos de texto, porque esa es una interfaz
  universal*»

Más allá de la propia filosofía, el propio diseño de Unix habilita que pensar
de ese modo sea posible debido a sus fuertes decisiones de diseño. La más
importante, «*Todo es un archivo*», es una decisión fundamental que condiciona
todo el sistema, haciendo posible acceder a múltiples fuentes de datos y
funcionalidades como si de un archivo se trataran. Esta decisión será de vital
importancia más adelante.

Por otro lado, Unix no nace como un sistema operativo amigable y gran parte de
esa herencia sigue presente a día de hoy. Es fácil destruir el sistema completo
si no se tiene cuidado. Por ejemplo, ejecutar `rm -rf /` en modo superusuario
eliminará todo el contenido en el disco duro sin siquiera preguntar si es lo
que se desea hacer. Es por eso que en las listas de correo de Debian se puede
encontrar la frase: «*Unix te da suficiente cuerda como para que te ahorques*»


###  Trabajos derivados

Desde la adopción general de Unix, han surgido varios proyectos derivados de la
filosofía que este sistema tenía en su momento. Ejemplos de esto son Plan9, un
sistema operativo distribuido en el que computadoras geográficamente separadas
podían funcionar como un único sistema, o Inferno, creado con la experiencia
obtenida en Plan9, ambos sistemas operativos diseñados en Bell Labs.

Los dos parten de la filosofía Unix y se basan en similares reglas. Ambos se
basan en la abstracción «todo es un archivo» y la combinan con un protocolo
unificado de comunicación (llamado 9P o Styx) para hacer que todas las piezas
de estos sistemas operativos distribuidos encajen a la perfección.

A diferencia de Unix, estos proyectos consiguieron explotar la filosofía de
forma mucho más avanzada, sobreponiéndose a las limitaciones de implementación
que se encontraron al desarrollar Unix. Lamentablemente, ninguno de ellos
llega a tener éxito comercial así que ahora forman parte de la historia de la
informática, aunque siguen muy presentes en comunidades de aficionados.
