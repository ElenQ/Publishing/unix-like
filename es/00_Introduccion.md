# Introducción

Este documento se plantea como una introducción a sistemas operativos similares
a Unix [^Unix-like], al ser éste altamente influyente en el diseño e
implementación de sistemas operativos modernos.

Durante el documento se centra la atención principalmente en aquellos sistemas
operativos que se basan en el núcleo Linux, por ser software libre, por ser
extremadamente popular en muchísimos ámbitos muy diversos (desde
supercomputadoras hasta dispositivos embebidos) y por el impacto que ha tenido
en el mundo de la computación.

En lo que al resto de piezas del sistema operativo se refiere, este documento
se centra principalmente en software GNU, proyecto de software libre nacido
para aportar a las personas libertad y control en el uso de sus computadoras.
Al tándem formado por GNU y Linux se le conoce como el sistema operativo
GNU/Linux[^historia-gnu].

Actualmente la forma más sencilla de obtener un sistema operativo GNU/Linux es
mediante las «Distribuciones de Linux» (o «distro»), paquetes de software que
incluyen el sistema operativo GNU/Linux junto con otras piezas de software,
normalmente libre, que no tiene por qué estar relacionado con GNU.

En caso de tratar detalles específicos de una Distribución, este documento
elige tratar distribuciones basadas en Debian (como la propia Debian o la
familia Ubuntu), por ser una de las más conocidas y accesibles. De todos modos,
como todas tienen en común que están basadas en GNU/Linux, la mayor parte de lo
expresado en este documento es perfectamente extrapolable y se indicará
claramente cuando se trate de una distribución específica.

[^Unix-like]: En inglés se utiliza el adjetivo *Unix-like* («parecido-a-Unix»)
  para referirse a estos sistemas operativos cuyo funcionamiento es similar a
  Unix, sin necesidad de ajustarse al pie de la letra a ninguno de sus
  estándares.

[^historia-gnu]: En el apartado sobre la historia de Unix se profundiza más en
  la relación entre GNU y Linux.

## Algunas definiciones

La fama del núcleo Linux es tal que su nombre ha trascendido. Hoy en día, el
nombre Linux se utiliza para denominar a los sistemas operativos que lo usan
como núcleo, proyectando la idea de que Linux es un sistema operativo.

Dejando a un lado que esto sea una muestra de falta de rigurosidad técnica que
debemos corregir, es especialmente importante para los objetivos de este
documento identificar los componentes de forma correcta. Esto se debe a que los
conceptos cobran especial relevancia a la hora de tratar con estándares y
convenciones y es necesario tener claro el tema del que se trata a la hora de
buscar documentación y referencias.

No es el único tema en el que las definiciones se han difuminado con el paso
del tiempo, así que durante todo el documento se hará especial hincapié en los
casos que sean relevantes. Sin embargo, existen algunos casos que merecen una
mención especial antes de comenzar.

### ¿Qué es un Sistema Operativo?

Un sistema operativo (*Operating System* en inglés, o simplemente *OS*) es un
programa informático que aporta un conjunto de funcionalidades a la máquina en
la que se instale.

La característica principal de un sistema operativo es que aporta a la máquina
la capacidad de ejecutar aplicaciones sobre él.

Los sistemas operativos actúan como intermediario entre las personas que
utilizan el sistema y la máquina, poniendo los recursos de ésta a la
disposición de quien la usa. Algunos ejemplos de estos recursos son el acceso
al almacenamiento, la ejecución de aplicaciones, el acceso a la red, etc.

### ¿Qué es un núcleo?

El núcleo (*kernel* en inglés) es la pieza del sistema operativo que dispone de
acceso total, privilegiado, a los recursos del dispositivo. El núcleo tiene
muchas labores, todas las relacionadas con repartir los recursos físicos de la
máquina: gestionar la memoria, el tiempo de computación, crear y eliminar
procesos, etc.

Aparte del núcleo, el sistema operativo incluye otros componentes que no
disponen de acceso completo a los recursos de la máquina y deben pedírselos al
núcleo.

### ¿Qué es una *shell*?

La *shell* (*shell* significa «caparazón») es la parte del sistema operativo
que actúa como interfaz con quienes lo utilizan. La *shell* expone los
servicios que el sistema operativo aporta de forma que las personas u otros
programas puedan usarlos.

Considerando la forma en la que se interactúa con ellas, existen dos familias
principales de *shell*s: las basadas en una línea de comandos, o CLI
(*Command-Line Interface*), y las gráficas, o GUI (*Graphical User
Interface*)[^only-cli].

En ambos casos, el trabajo de la *shell* es el de recoger la interacción de las
personas o programas que la utilicen y transformar esa interacción en
peticiones al sistema operativo, las cuales, en algunos casos, acabarán
accediendo al núcleo, en busca de recursos de la máquina.

[^only-cli]: En muchas ocasiones, debido a que cuando el concepto se acuñó eran
  el único tipo que existía, se denomina *shell* únicamente a la interfaz de
  línea comandos.


Los sistemas tipo Unix exponen un *shell* de comandos muy rica y potente, que
era inicialmente el único modo de interactuar con estos sistemas. Hoy en día
existen entornos gráficos para trabajar en ellos pero es vital conocer la
*shell* de comandos lo suficiente para desenvolverse correctamente ya que en
muchos contextos (software embebido, servidores, etc.) la terminal de comandos
será la única forma disponible de acceso.

Además, quien conoce los sistemas tipo Unix sabe que incluso teniendo una
*shell* gráfica, el poder que la terminal de comandos aporta es en muchas
ocasiones superior al modo gráfico y conviene conocerla para poder explotar sus
funcionalidades. Que en muchos casos no están disponibles gráficamente.

## Dónde documentarse

Los propios sistemas tipo Unix tienen costumbre de estar documentados. Como
ocurrirá en más de una ocasión, el sistema GNU/Linux hereda la forma clásica de
Unix y añade una forma nueva para hacer las cosas.

El estilo GNU de consultar la documentación es mediante el comando `info`. Este
comando visualiza documentación escrita en el formato Texinfo y la visualiza de
modo navegable en la terminal. Texinfo es un formato muy rico y puede
utilizarse para generar documentación en otros formatos como PDF y HTML así que
es muy posible que la documentación que se desee consultar mediante `info` esté
también disponible en otros formatos en la web del proyecto a consultar. Para
saber más sobre el comando `info`, no hay mejor manera que leer su
documentación:

``` bash
$ info info
```

> **NOTA**: a la hora de replicar este comando el símbolo `$` no debe
> introducirse. Se indica como referencia de que el comando puede ejecutarse en
> modo usuario. Si se tratara de modo administrador se indicaría el símbolo
> `#`. Más adelante se profundiza en el significado de estos símbolos.

El estilo Unix usa el comando `man`, seguido del tema que se desee consultar.
Este sistema está basado en el formato `troff`, y hace uso del paginador por
defecto del sistema para visualizarse. Al igual que en el caso anterior,
`troff` también puede volcarse a otros formatos, por lo que es posible acceder
a páginas del manual a modo de páginas web, aunque es menos frecuente y útil,
debido a que éstas suelen ser más escuetas.

Las «páginas man» contienen gran variedad de información sobre el sistema
agrupada en diferentes secciones numeradas del 1 al 9.  Para conocer más sobre
su funcionamiento y el uso de las diferentes secciones, lo mejor es leer la
documentación de `man` en el propio `man`:

``` bash
$ man man
```

Casi todo el software de un sistema GNU/Linux estará documentado de una u otra
manera, siendo más frecuente la segunda. Sin embargo, algunos programas no
proporcionan documentación en ninguno de los dos formatos. En esos casos, es
interesante probar a ejecutarlos usando las opciones de ayuda que los propios
programas proporcionen. Por convención, las más frecuentes son `-h` y `--help`,
por ejemplo:

``` bash
$ programa --help
```

Normalmente los programas se entregan empaquetados con su documentación.
Algunos programas con documentaciones muy extensas sólo entregan un pequeño
resumen o la dejan relegada a otro paquete independiente que debe instalarse.
Se recomienda comprobar esos casos de forma individual.

### Documentación sobre Linux

En el caso del núcleo Linux se recomienda encarecidamente instalar la
documentación aportada por el proyecto «The Linux Programmer's Manual» si ésta
no viene instalada con el sistema. El proyecto puede descargarse desde la
siguiente página web, aunque las Distribuciones suelen incluirlo en su sistema
de paquetes:

<https://www.kernel.org/doc/man-pages/>

Esta documentación en forma de páginas `man` aporta mucha información sobre el
funcionamiento del núcleo y cómo interactuar con él.
